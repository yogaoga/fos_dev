$(document).ready(function() {		
	
	$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			"z-index":"99999",
			opacity: 0.80
	}).appendTo("body");
	$("#mini-chart-orders").sparkline([1,4,6,2,0,5,6,4], {
		type: 'bar',
		height: '30px',
		barWidth: 6,
		barSpacing: 2,
		barColor: '#f35958',
		negBarColor: '#f35958'});

	$("#mini-chart-other").sparkline([1,4,6,2,0,5,6,4], {
		type: 'bar',
		height: '30px',
		barWidth: 6,
		barSpacing: 2,
		barColor: '#0aa699',
		negBarColor: '#0aa699'});	
	
	$('#ram-usage').easyPieChart({
		lineWidth:9,
		barColor:'#f35958',
		trackColor:'#e5e9ec',
		scaleColor:false
    });
	$('#disk-usage').easyPieChart({
		lineWidth:9,
		barColor:'#7dc6ec',
		trackColor:'#e5e9ec',
		scaleColor:false
    });
	
	// Moris Charts - Line Charts
	
	Morris.Area({
	  element: 'line-example',
	  data: [
		{ y: '2015-10-01', a: 50 },
		{ y: '2015-09-30', a: 65 },
		{ y: '2015-09-29', a: 50},
		{ y: '2015-09-20', a: 75},
		{ y: '2015-09-19', a: 50},
		{ y: '2015-09-18', a: 75},
		{ y: '2015-09-17', a: 100	}
	  ],
	  xkey: 'y',
	  ykeys: ['a'],
	  labels: ['Visitor'],
	  lineColors:['#78CEC7'],
	  fillOpacity: '0.7',
	  resize: 'true'
	});

	Morris.Area({
	  element: 'line-example1',	
	  data: [
		{ y: '2015-10-01', a: 50, b: 40, c:30, d:32 },
		{ y: '2015-09-30', a: 65,  b: 55, c:23, d:65 },
		{ y: '2015-09-29', a: 50,  b: 40, c:12, d:67 },
		{ y: '2015-09-20', a: 75,  b: 65, c:32, d:41 },
		{ y: '2015-09-19', a: 50,  b: 40, c:11, d:56 },
		{ y: '2015-09-18', a: 75,  b: 65, c:53, d:86 },
		{ y: '2015-09-17', a: 100, b: 90, c:11, d:42 }
	  ],	
	  xkey: 'y',
	  ykeys: ['a', 'b','c','d'],
	  labels: ['Produk A', 'Produk B', 'Produk C', 'Produk D'],
	  lineColors:['#8297A8','#9FC2C4','#FC9D9B','#F3565D'],
	  fillOpacity: '0.7',
	  lineWidth:'0',
	  resize: 'true'
	});
	
	Morris.Area({
	  element: 'area-example',
	  data: [
		{ y: '2006', a: 100, b: 90 },
		{ y: '2007', a: 75,  b: 65 },
		{ y: '2008', a: 50,  b: 40 },
		{ y: '2009', a: 75,  b: 65 },
		{ y: '2010', a: 50,  b: 40 },
		{ y: '2011', a: 75,  b: 65 },
		{ y: '2012', a: 100, b: 90 }
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['Series A', 'Series B'],
	  lineColors:['#0090d9','#b7c1c5'],
	  lineWidth:'0',
	   grid:'false',
	  fillOpacity:'0.5'
	});
	
	Morris.Donut({
	  element: 'donut-example',
	  data: [
		{label: "Download App", value: 12},

		{label: "Mail-Inbox", value: 20}
	  ],
	  colors:['#60bfb6','#91cdec']
	});

	$('#mysparkline').sparkline([4,3,3,1,4,3,2,2,3], {
			type: 'line', 
			width: '100%',
			height:'250px',
			fillColor: 'rgba(209, 218, 222, 0.30)',
			lineColor: '#fff',
			spotRadius: 4,
			valueSpots:[4,3,3,1,4,3,2,2,3],
			minSpotColor: '#d1dade',
			maxSpotColor: '#d1dade',
			highlightSpotColor: '#d1dade',
			 highlightLineColor: '#bec6ca',
			normalRangeMin: 0
	});
	$('#mysparkline').sparkline([3,2,3,4,3,2,4,1,3], {
					type: 'line', 
					composite: true,
					width: '100%',
					fillColor: ' rgba(0, 141, 214, 0.10)',
					lineColor: '#fff',
					minSpotColor: '#167db2',
					maxSpotColor: '#167db2',
					highlightSpotColor: '#8fcded',
					 highlightLineColor: '#bec6ca',
					spotRadius: 4,
					valueSpots:[3,2,3,4,3,2,4,1,3],
					normalRangeMin: 0
	});	

	var values = [1, 2, 3,4,5,];

	// Draw a sparkline for the #sparkline element
	$('#pie-distributor').sparkline(values, {
	    type: "pie",
	    width: '100%',
		height: '100%',
		offset: 15,
		borderWidth: 0,
		borderColor: '#95CEFF',
		sliceColors: ['#999EFF','#434348','#90ED7D','#F7A35C','#95CEFF'],
	    // Map the offset in the list of values to a name to use in the tooltip
	    tooltipFormat: '{{offset:offset}} ({{percent.1}}%)',
	    tooltipValueLookups: {
	        'offset': {
	            0: 'Dono',
	            1: 'Kasino',
	            2: 'Indro',
	            3: 'Erza',
	            4: 'Natsu'
	        }
	    },
	});
	
	$("#spark-2").sparkline([4,3,3,4,5,4,3,2,4,5,6,4,3,5,2,4,6], {
		type: 'line',
		width: '100%',
		height: '200',
		lineColor: '#0aa699',
		fillColor: '#e6f6f5',
		minSpotColor: '#0c948a',
		maxSpotColor: '#78cec7',
		highlightSpotColor: '#78cec7',
		highlightLineColor: '#78cec7',
		spotRadius: 5
	});
	
	$("#sparkline-pie").sparkline([8,2,3], {
		type: 'pie',
		width: '100%',
		height: '100%',
		sliceColors: ['#F3565D','#FC9D9B','#9FC2C4'],
		offset: 10,
		borderWidth: 0,
		borderColor: '#000000 '
	});
	
	var seriesData = [ [], []];
	var random = new Rickshaw.Fixtures.RandomData(50);

	for (var i = 0; i < 50; i++) {
		random.addData(seriesData);
	}

	graph = new Rickshaw.Graph( {
		element: document.querySelector("#updatingChart"),
		height: 200,
		renderer: 'area',
		series: [
			{
				data: seriesData[0],
				color: 'rgba(0,144,217,0.51)',
				name:'DB Server'
			},{
				data: seriesData[1],
				color: '#eceff1',
				name:'Web Server'
			}
		]
	} );
	var hoverDetail = new Rickshaw.Graph.HoverDetail( {
		graph: graph
	});

	setInterval( function() {
		random.removeData(seriesData);
		random.addData(seriesData);
		graph.update();

	},1000);
	
	//Bar Chart  - Jquert flot
	
    var d1_1 = [
        [1325376000000, 120],
        [1328054400000, 70],
        [1330560000000, 100],
        [1333238400000, 60],
        [1335830400000, 35]
    ];
 
    var d1_2 = [
        [1325376000000, 80],
        [1328054400000, 60],
        [1330560000000, 30],
        [1333238400000, 35],
        [1335830400000, 30]
    ];
 
    var d1_3 = [
        [1325376000000, 80],
        [1328054400000, 40],
        [1330560000000, 30],
        [1333238400000, 20],
        [1335830400000, 10]
    ];
 
    var d1_4 = [
        [1325376000000, 15],
        [1328054400000, 10],
        [1330560000000, 15],
        [1333238400000, 20],
        [1335830400000, 15]
    ];
 
    var data1 = [
        {
            label: "Tas",
            data: d1_1,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300,
                fill: true,
                lineWidth:0,
                order: 1,
                fillColor:  "rgba(243, 89, 88, 0.7)"
            },
            color: "rgba(243, 89, 88, 0.7)"
        },
        {
            label: "Celana",
            data: d1_2,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300,
                fill: true,
                lineWidth: 0,
                order: 2,
                fillColor:  "rgba(251, 176, 94, 0.7)"
            },
            color: "rgba(251, 176, 94, 0.7)"
        },
        {
            label: "T-shirt",
            data: d1_3,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300,
                fill: true,
                lineWidth: 0,
                order: 3,
                fillColor:  "rgba(10, 166, 153, 0.7)"
            },
            color: "rgba(10, 166, 153, 0.7)"
        },
        {
            label: "Dll",
            data: d1_4,
            bars: {
                    show: true,
                barWidth: 12*24*60*60*300,
                fill: true,
                lineWidth: 0,
                order: 4,
                fillColor:  "rgba(67, 67, 72, 0.7)"
            },
            color: "rgba(67, 67, 72, 0.7)"
        },

    ];
 
    $.plot($("#placeholder-bar-chart"), data1, {
        xaxis: {
            min: (new Date(2011, 11, 15)).getTime(),
            max: (new Date(2012, 04, 18)).getTime(),
            mode: "time",
            timeformat: "%b",
            tickSize: [1, "month"],
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            tickLength: 0, // hide gridlines
            axisLabel: 'Month',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5,
        },
        yaxis: {
            axisLabel: 'Value',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 1,
			borderColor:'#f0f0f0',
			labelMargin:8,
        },
        series: {
            shadowSize: 1
        }
    });
 
 
    function getMonthName(newTimestamp) {
        var d = new Date(newTimestamp);
 
        var numericMonth = d.getMonth();
        var monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
 
        var alphaMonth = monthArray[numericMonth];
 
        return alphaMonth;
    }
 

	 // ORDERED & STACKED CHART
    var data2 = [
        {
            label: "Product 1",
            data: d1_1,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300*2,
                fill: true,
                lineWidth:0,
                order: 0,
                fillColor:  "rgba(243, 89, 88, 0.7)"
            },
            color: "rgba(243, 89, 88, 0.7)"
        },
        {
            label: "Product 2",
            data: d1_2,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300*2,
                fill: true,
                lineWidth: 0,
                order: 0,
                fillColor:  "rgba(251, 176, 94, 0.7)"
            },
            color: "rgba(251, 176, 94, 0.7)"
        },
        {
            label: "Product 3",
            data: d1_3,
            bars: {
                show: true,
                barWidth: 12*24*60*60*300*2,
                fill: true,
                lineWidth: 0,
                order: 0,
                fillColor:  "rgba(10, 166, 153, 0.7)"
            },
            color: "rgba(10, 166, 153, 0.7)"
        },
        {
            label: "Product 4",
            data: d1_4,
            bars: {
                    show: true,
                barWidth: 12*24*60*60*300*2,
                fill: true,
                lineWidth: 0,
                order: 0,
                fillColor:  "rgba(0, 144, 217, 0.7)"
            },
            color: "rgba(0, 144, 217, 0.7)"
        },

    ];
	$.plot($('#stacked-ordered-chart'), data2, {		
		 grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 1,
			borderColor:'#f0f0f0',
			labelMargin:8

        },
		        xaxis: {
            min: (new Date(2011, 11, 15)).getTime(),
            max: (new Date(2012, 04, 18)).getTime(),
            mode: "time",
            timeformat: "%b",
            tickSize: [1, "month"],
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            tickLength: 0, // hide gridlines
            axisLabel: 'Month',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
				stack: true
	});
	// DATA DEFINITION
	function getData() {
		var data = [];

		data.push({
			data: [[0, 1], [1, 4], [2, 2]]
		});

		data.push({
			data: [[0, 5], [1, 3], [2, 1]]
		});

		return data;
	}
	
	
});