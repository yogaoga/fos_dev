$(function(){

	checkUsername = function(){
		var $username = $('[name="username"]').val();
		$("#loader").show();
		$.ajax({
			type 	: 'GET',
			url 	: _base_url + '/checkuser',
			data 	: {
				username 	: $username,
			},
			cache 	: false,
			dataType : 'json',
			success : function(res){
				$('#status').html(res.data);
				$("#loader").hide();
			}
		});
	}

});