@extends('Master.Template')

@section('meta')
<script type="text/javascript" src="{{ asset('/js/personalia/karyawan.js') }}"></script>
<style type="text/css">
	.items:hover td .tbl-opsi{
		display: block !important;
	}
</style>
@endsection

@section('title')
Pesan
@endsection

@section('content')

<div class="row">
	<div class="col-sm-9">
		<div class="grid simple">
			<div class="grid-title no-border">
				<div class="row">
					<div class="col-sm-9 col-xs-9">
						<h4>{{ $items->total() }} Pesan <span class="semi-bold">ditemukan</span></h4>
					</div>
					<div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		                <a href="javascript:;" class="reload"></a> 
		              </div>
		            </div>
	            </div>
			</div>
			<div class="grid-body no-border">
				<table class="table table-striped table-flip-scroll cf">
					<thead>
						<tr>
							<th>No</th>
							<th><a href="javascript:;">Subject</a></th>
							<th><a href="javascript:;">Email</a></th>
							<th>Tgl Pesan</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="contents-items">
						<?php $no = $items->currentPage() == 1 ? 1 : ($items->perPage() + $items->currentPage()) -1 ; ?>
						@forelse($items as $item)
						<tr class="item_{{ $item->id }} items">
							<td>{{ $no }}</td>
							<td>{{ $item->email }}</td>
							<td>{{ $item->content }}</td>
							<td>{{ $item->created_at }}</td>
							<td class="text-right">
								@if(Auth::user()->permission > 2)
								<button type="button" class="close hapus" data-nama="{{ $item->subject }}" data-id="{{ $item->id }}"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								@endif
							</td>
						</tr>
						<?php $no++; ?>
						@empty

						@endforelse
					</tbody>
				</table>

				<div class="text-right pagins">
					{!! $items->render() !!}
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="grid simple">
			<div class="grid-title no-border">
				<div class="row">
					<div class="col-sm-9 col-xs-9">
		             <h4>Cari <span class="semi-bold">Pesan</span></h4>
		            </div>
		            <div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		              </div>
		            </div>
		            
		         </div>
			</div>
			<div class="grid-body no-border">
				<p>
					<div class="form-group">
						<div class="input-group transparent">
							<span class="input-group-addon ">
								<i class="fa fa-search"></i>
							</span>
							<input type="text" class="form-control" placeholder="Cari Pesan..." name="src">
						</div>
					</div>


					<div class="form-group">
						<label>Urutan</label>
						<select id="source" style="width:100%" name="orderby">
							<option value="asc">A - Z</option>
							<option value="desc">Z - A</option>
						</select>
					</div>
					<div class="form-group">
						<label>Limit / Page</label>
						<select id="source" style="width:100%" name="limit">
							<option value="10">10</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="500">500</option>
						</select>
					</div>

				</p>
				<br />
				<button class="btn btn-block btn-primary cari-barang" type="button"><i class="fa fa-search"></i> Cari</button>
			</div>
		</div>
	</div>
</div>

@endsection