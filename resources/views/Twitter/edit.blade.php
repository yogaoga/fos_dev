@extends('Master.Template')
@section('content')
	<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<div class="row">
				<div class="col-sm-9 col-xs-9">
	             	<h4>Aktifasi Widget</h4>
	            </div>
	            <div class="col-sm-3 col-xs-3 text-right">
	              <div class="tools text-left">
	                <a href="javascript:;" class="collapse"></a> 
	              </div>
	            </div>	            
	        </div>
		</div>
		<div class="grid-body no-border">
			<form action="{{ url('twitter\update') }}" method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{ $item->id }}">
				<div class="row column-seperation">
					<div class="col-md-8 col-sm-8 col-xs-8">
						<div class="form-group">
							<div class="form-label">Judul</div>
							<div class="control">
								<input type="text" required name="username" class="form-control" value="{{ $item->username }}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-label">Widget ID</div>
							<div class="control">
								<input type="text" required name="widget_id" class="form-control" value="{{ $item->widget_id }}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-label">Password</div>
							<div class="control">
								<input type="text" required name="password" class="form-control" value="{{ $item->password }}">
							</div>
						</div>
						
						<div class="form-group">
							<div class="control">
								<button type="submit" class="btn btn-primary">Ubah</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection