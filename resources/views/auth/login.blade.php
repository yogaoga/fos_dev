@extends('Master.client')

@section('content')

<section class="content-section  banner">
	<div class="parallax-cover parallax-0" style="background-image: url('/assets/img/bg-1.jpg')"></div>
	<div class="overlay-darken-5 "></div>
	<div class="container">
		<div class="pd-50">
			<div class="row">
				<div class="col-md-6 text-white text-center">
					<h1 class="text-white" style="font-size:50px">WELCOME TO <b> <span class="color"> FULL OF STARS </span> </b></h1>
					<p> 
						<h3 class="text-white" style="line-height:30px">HOUSE OF CELEBRITY'S PRODUCTS</h3>
					</p>
				</div>
				<div class="col-md-4 col-md-offset-2 col-xs-12">

					<section class="panel mt25 bordered">

						<div class="p15">  
							<form id="login-form" class="login-form" action="{{ url('/auth/login') }}" method="post">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								@if (count($errors) > 0)
								<div class="alert alert-danger col-md-10">
									<strong>Ups!</strong>  Username / Password anda tidak sesuai.<br><br>
									<ul>
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
								@endif
								<input name="username" value="{{ old('username') }}" id="login_username" type="text" autofocus  class="form-control input-sm mb5" placeholder="Username">
								<input name="password" id="login_pass" type="password"  class="form-control" placeholder="Password">
								<div class="form-group">

									<div class="col-sm-6 col-xs-12 ">
										<div class="checkbox pull-left">
											<label class="color">
												<input type="checkbox" id="checkbox1" name="remember" value="1"><small>Ingat saya </small>
											</label>
										</div>
									</div>

									<div class="col-sm-6 col-xs-12 text-right mt10">
										<a href="{{ url('/reset') }}"><small> Lupa kata sandi?</small></a>
									</div>

								</div>

								<button class="btn btn-color btn-lg btn-block" type="submit">Masuk</button>
								<div class="row">
									<div class="col-sm-12 text-center">
										<h6 class="small"> Tidak punya acoount ? <a href="{{ url('/register') }}"><b>Daftar</b></a></h6>
									</div>
								</div>
							</form>
						</div>
					</section>

				</div>
			</div>
		</div>
	</div>
</section>


@endsection