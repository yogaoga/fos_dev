@extends('Master.client')

@section('content')
<section class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 mb25">

				<div class="row">

					<div class="col-xs-12 mb25">

						<div class="box-tab">

							<ul class="nav nav-tabs">
								<li class="active"><a href="#home1" data-toggle="tab">Press</a>
								</li>
								<li class=""><a href="#profile1" data-toggle="tab">Press Kit</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade active in" id="home1">
									
									<div class="row">                

										<!-- Blog posts -->
										<div class="col-md-12">

											<!--  blog post -->
											<div class="post">
												<div class="post-title">
													<h4 class="mt25">
														<a class="transition" href="post.html">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</a>
													</h4>
													<div class="post-meta heading-font">
														<small>by <a href="javascript:;">merahputih.com</a><span class="spacer"></span><a href="javascript:;">12 Desember 2015</a></small>
														
													</div>
													<div class="post-date heading-font">
														<div class="post-media">
															<img src="{{ asset('/assets/img/merahputih.png') }}" width="100px;" class="img-responsive" alt="">
														</div>
													</div>
													
												</div>
												
											</div>
											<!-- /blog post -->

											<!--  blog post -->
											<div class="post">
												<div class="post-title">
													<h4 class="mt25">
														<a class="transition" href="post.html">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</a>
													</h4>
													<div class="post-meta heading-font">
														<small>by <a href="javascript:;">kompas.com</a><span class="spacer"></span><a href="javascript:;">13 Desember 2015</a></small>
														
													</div>
													<div class="post-date heading-font">
														<div class="post-media">
															<img src="{{ asset('/assets/img/logokompascom.jpg') }}" width="100px;" class="img-responsive" alt="">
														</div>
													</div>
													
												</div>
												
											</div>
											<!-- /blog post -->

											<!--  blog post -->
											<div class="post">
												<div class="post-title">
													<h4 class="mt25">
														<a class="transition" href="post.html">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</a>
													</h4>
													<div class="post-meta heading-font">
														<small>by <a href="javascript:;">marketer.com</a><span class="spacer"></span><a href="javascript:;">14 Desember 2015</a></small>
														
													</div>
													<div class="post-date heading-font">
														<div class="post-media">
															<img src="{{ asset('/assets/img/marketeers.jpg') }}" width="100px;" class="img-responsive" alt="">
														</div>
													</div>
													
												</div>
												
											</div>
											<!-- /blog post -->
											

										</div>
										<!-- /Blog posts -->


									</div>

								</div>
								<div class="tab-pane fade" id="profile1">
									<div class="row">
										<div class="col-sm-12 text-center">

											<h2>Full Of Stars Press Kit</h2>
											<h2 class="small mb20">To download Full Of Stars logos, click the thumbnails below for actual size.</h2>
											<div class="row">
												<div class="col-sm-6">
													<a href="javascript:;">
														<img src="{{ asset('/assets/img/fos-ori.png') }}"  alt="img-press">
													</a>
													<h2 class="small mb20"> Primary logo (white background)</h2>
												</div>

												<div class="col-sm-6">
													<a href="javascript:;">
														<img src="{{ asset('/assets/img/fos-white.png') }}" style="background:#EFEFEF">
														<h2 class="small">Secondary logo (for dark background)</h2>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</section>
@endsection