@extends('Master.client')

@section('content')

<section class="content-promo">
	<div class="container">
		<h2 class="text-muted">Pencarian</h2>
		<hr>
		<div class="row">
			<div class="col-md-12 bordered">
				 @foreach($datas as $data)
				<div class="row bb p5"> 
					<div class="col-md-2 col-sm-3 col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<div class="img-cont">
									<img src="{{ asset('/assets/img/promo/'.$data->gambar) }}" class="img-rensponsive">
								</div>
							</div>
						</div>
					</div> 
					<div class="col-md-8 col-sm-8  col-xs-12">
						<div class="row">
							<div class="col-md-12 no-lh">
								
								<div class="mt10"> 
									<h5 class="text-muted" ><a href="{{ url('/detail-produk/'.$data->id) }}">{{ $data->nm_barang }}</a></h5> 
								</div>
								<div class="mb5"> 
									<i class="ti ti-tag mr5"></i>
									<em class="small">
										{{ $data->nama_type }}
									</em> 
								</div>
								<div class="row">
								 	<div class="col-sm-12">
								 		<i class="ti ti-location-pin mr5"></i>
										<em class="small"> Agent Location: {{ $data->alamat }} </em>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach


			</div>
		</div>
	</div>
</section>
@endsection