@extends('Master.client')

@section('content')


<section class="content-promo">
    <div class="container">

        <div class="row">
            @foreach($artist as $data)
            <div class="col-md-4">

                <section class="widget bg-white bordered">

                    <div class="widget-body">
                      <div class="row">
                        <div class="col-sm-4 col-xs-3 pt10">
                           @if($data->foto == null || $data->foto == "")
                           <img src="{{ asset('/assets/img/user.png') }}" class="img-rensponsive" width="200px">    
                           @else
                           <img src="{{ asset('/assets/img/client/'.$data->foto) }}" class="img-rensponsive" width="200px">
                           @endif
                       </div>
                       <div class="col-sm-8 col-xs-9 clearfix">
                          <div class="product-title">
                          <h4>
                            
                            <a href="{{ url('/profil-page/'.$data->username) }}"><b>{{ $data->nm_depan }} {{ $data->nm_belakang }}</b></a>
                          </h4>
                          </div>

                        <p class="text-muted mb15">“<?php 
                           echo \Format::potongkata($data->deskripsi, 60);

                            ?>”</p>

                        </div> 
                    </div>
                </div>
            </section>
        </div>
        @endforeach
    </div>
</div>
</section>

@endsection
