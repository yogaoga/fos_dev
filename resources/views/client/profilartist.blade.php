@extends('Master.client')

@section('content')

    
    <section class="content-section">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                	
                    <div class="row">

		                <div class="col-sm-3 mt25 ">
		                	<div class="row"> 
				    			<div class="col-sm-12 text-center" >
						    		@if($artist->foto == null || $artist->foto == "")
						            <div class="img-user-u">
						                <img src="{{ asset('/assets/img/faceless.jpg') }}" class="img-circle border-img" width="120px">
						            </div>
						            @else

						             <div class="img-user-u">
						                <img src="{{ asset('/assets/img/client/'.$artist->foto) }}" class="img-circle border-img" width="120px">
						            </div>
									@endif
									<h4 class="color"><b>{{ $artist->nm_depan }} {{ $artist->nm_belakang }}</b></h4>
								</div>

							</div>
					
		                </div>

                    	<div class="col-sm-9">
                    		<h3 class="text-muted mb15">Overview</h3> 
                    		<p>{{ $artist->deskripsi }}</p>
                    	</div>

	                </div>

                    <hr>
                    


                </div>
            </div>
        </div>

    </section>


@stop