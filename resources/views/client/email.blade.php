<!DOCTYPE html>
<html>
<head>
	<title>Full Of Star</title>
</head>
<body>
<table>
	<tr>
		<td>Costumer</td>
		<td>:</td>
		<td>{{ $user->nm_depan }} {{ $user->nm_belakang }}</td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td>{{ $user->email }}</td>
	</tr>
	<tr>
		<td>No HP</td>
		<td>:</td>
		<td>{{ $user->hp }}</td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td>{{ $user->alamat }}</td>
	</tr>

</table>
	<table border="1">
	<thead>
		<tr>
			<th>Nama Barang</th>
			<th>Jumlah</th>
		</tr>
	</thead>
		<tbody>
			@foreach($items['kd_barang'] as $i => $kd_barang)

			<tr>
				<td>{{ $items['kd_barang'][$i] }}</td>
				<td align="right">{{ $items['qty'][$i] }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>