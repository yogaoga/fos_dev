@extends('Master.client')

@section('content')
	<section class="content-section mb25">
        <div class="container">
            <div class="row mt25">
                <div class="col-md-8 col-sm-offset-2 text-center">
                    <div class="section-title">
                        
                        <h4 class="sub-heading small no-lh">About</h4>
                        <h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row mt20">
                <div class="col-md-6 text-left">

                    <img src="{{ asset('/assets/img/fos-large.png') }}" class="img-responsive mt25">
                    <div class="row text-center">
                        <div class="col-md-10 col-md-offset-1">
                        <h4 class="lh-1">“We encourage celebrities to “start” a business /creating products before it’s too late..”<br><p class="text-right mt10"><b><em>~ Full Of Stars</em></b></p></h4>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-6 text-left">
                <h4 class="lh-1">
                    <p><a href="{{ url('/') }}">
                    <span class="color"><b>
                    <ins>Fullofstars.co.id </ins></b> </span></a>adalah situs interaktif non ritel yang berfokus pada pengembangan offline ritel saluran distribusi untuk produk selebriti di Indonesia. Full Of Stars memiliki kerjasama eksklusif dengan jajaran Artis Nasional papan atas</p>
                    <p>
                        Full Of Stars menyediakan produk unik dan berkualitas untuk pria dan wanita seperti : pakaian, aksesoris, sepatu, tas, serta produk lainnya. Full Of Stars akan memberikan sensasi berbelanja unik dengan adanya fasilitas Geo Tagging, sehingga calon pembeli diberi kemudahan dalam bertemu dan bertransaksi langsung antara pembeli dengan penjual (reseller).
                    </p>
                    <p>
                        Full Of Stars membuka kesempatan bagi Anda yang tertarik menjalin bisnis dengan Kami sebagai Mitra/Distributor utama untuk memegang 1 (satu) wilayah secara eksklusif
                    </p></h4>

                </div>
            </div>
        </div>  
    </section>
@endsection