@extends('Master.client')

@section('content')
	<section class="content-section mb25">
        <div class="container">
            <div class="row mt25">
                <div class="col-md-8 col-sm-offset-2 text-center">
                    <div class="section-title">
                        
                        <h4 class="sub-heading small no-lh">Reseller</h4>
                        <h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row mt25">
                
                <div class="col-md-12    text-center">
                    <h2 class="lh-1"><p>Untuk informasi lebih lengkap, silahkan hubungi <a href="{{ url('index') }}"><span class="color"><b><ins> Care Center kami</ins></b></span></a>    
                    </p></h2>
                </div>

            </div>
        </div>  
    </section>
@endsection