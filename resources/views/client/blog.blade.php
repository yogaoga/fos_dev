@extends('Master.client')

@section('content')
<!-- Blog contents -->
<section class="content-section blog-posts">
    <div class="container">
        <div class="row">
            <!-- Blog posts -->
            <div class="col-sm-9 ">

                <!--  blog post -->
                
                @foreach($items as $item)
                <div class="post">
                    <div class="post-title">
                        <h1 class="mt25">
                            <a class="transition" href="{{ url('/blogreview/'.$item->id) }}">{{ $item->judul }}</a>
                        </h1>
                        <div class="post-meta heading-font">
                            <small>Posted by <a href="javascript:;">{{ $item->name }}</a><span class="spacer"></span><a href="javascript:;">8 Comments</a></small>
                            <div class="post-date heading-font">
                                <span class="date">{{ date('d',strtotime($item->created_at)) }}</span> {{ date('M Y',strtotime($item->created_at)) }}
                            </div>
                        </div>
                    </div>

                    <div class="post-media">
                        <img src="{{ asset('/assets/img/blog/'.$item->gambar) }}" class="img-rounded img-responsive" alt="">
                    </div>
                  
                    <div class="post-content">
                        
                             <?php 
                            $content = htmlspecialchars_decode(stripslashes($item->content)); 
                            
                            $tampil = substr($content, 0, 300);
                           
                            echo $tampil . " ";

                            ?> 
                        </p>
                        <a class="read-more" href="{{ url('/blogreview/'.$item->id) }}">Read More</a>
                    </div> 


                    
                </div>
                @endforeach
                <!-- /blog post -->
                    <!-- Blog pagination -->
                    <ul class="pager">
                    
                        <li>
                            <a href="{{ $items->previousPageUrl() }}" class="prev">
                                <i class="ti-arrow-left mr5"></i>Previous</a>
                        </li>
                        <li>

                            <a href="{{ $items->nextPageUrl() }}" class="next">
                                Next <i class="ti-arrow-right ml5"></i></a>
                        </li>
                    </ul>
                    <!-- /Blog pagination -->
            </div>
            <!-- /Blog posts -->
              
            <!-- Blog sidebar -->
            <div class="col-sm-3 sidebar">

                <!-- Search form -->
                <div class="widget search">
                    <form class="form-inline" role="form">
                        <div class="search-form">
                            <button class="search-button" type="submit" title="Search"><i class="ti-search"></i>
                            </button>
                            <input type="text" class="form-control" placeholder="Search blog">
                        </div>
                    </form>
                </div>
                <!-- /Search form -->

                <!-- Categories list -->
                <div class="widget categories">
                    <h6>CATEGORIES</h6>
                    <ul>
                      @foreach($kategoris as $kategori)
                        <li>
                             <i class="ti-arrow-right color mr5"></i><a href="javascript:;">{{ $kategori->nm_kategori }}</a>
                        </li>
                      @endforeach
                    </ul>
                </div>
                <!-- /Categories list -->

                <!-- Recent posts list -->
                <div class="widget recent-activity">
                    <h6>RECENT POST</h6>
                    <ul>
                    @foreach($recents as $recent)
                        <li class="clearfix mb5">
                           <i class="ti-arrow-right color mr5"></i> <a href="{{ url('/blogreview/'.$recent->id) }}">{{ $recent->judul }}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>


            </div>
            <!-- Blog sidebar -->     
        </div>
    </div>
</section>
<!-- Blog contents -->


@endsection