@extends('Master.client')

@section('content')

<!-- hero -->


<section class="content-section hero vertical-center hero-slider">
	<ul class="bxslider">
	@foreach($banners as $banner)	
		<li style="background-image: url({{ asset("assets/img/banner/". $banner->gambar ) }});">
			<div class="overlay-darken-2"></div>

			<div class="container">
				<div class="hero-container">
					<div class="slider-center">
						
					</div>
				</div>
			</div>
		</li>
	@endforeach
	</ul>

</section>

<!-- /hero -->
<section class="mto">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<section class="panel panel-info  no-b">
					<header class="panel-heading">
						<div class="h5 text-white text-center">
							<i class="fa fa-search"></i> CARI PRODUK ARTIST ANDA DISINI
						</div>
					</header>
					<footer class="panel-footer text-center">

						<div class="row">
							<div class="col-md-12 col-xs-12">
								<form class="form-inline" role="form" action="{{ url('/search') }}" method="get">
									
									<div class="row">
										<div class="col-md-10">
											<!-- Search form -->
											<div class="widget search">

												<div class="search-form has-warning">
													<input type="text" class="form-control" placeholder="Search  product, artist" name="cari">

												</div>
											</div>
											<!-- /Search form -->
										</div>

										<div class="col-md-2 col-xs-12 text-center">
											<button type="submit" class="btn btn-md btn-primary btn-block mb5">Cari</button>
										</div>
									</div>
								</form>
							</div>

						</div>

					</footer>
				</section>
			</div>
		</div>
	</div>
</section>

<section class="content-section min-mt">
	<div class="container">

		<div class="row">
			<div class="col-md-6 col-xs-6">
				<div class="section-title bl pl15">

					<h4 class="sub-heading"><span class="color"><b> Promo</b></span> Pilihan</h4>
					<h6 class="heading mb25"><em>Lihat Promo Produk Terbaru Dari Artis Pilihan</em></h6>

				</div>
			</div>
			<div class="col-md-6 col-xs-6 text-right">
				<a href="{{ url('/product') }}"><button type="button" class="btn btn-md btn-primary btn-outline mt25"> Produk lainnya <i class="ti ti-arrow-right"></i></button></a>
			</div>
		</div>

		<div class="row text-left">

			<div id="user" class="owl-carousel owl-theme">
				@foreach($barangs as $barang)
				<div class="item">
					<div class="border m5">
						<!--
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-sm-4 col-md-4 col-xs-3">
										<div class="pull-left">
											@if($barang->foto == null || $barang->foto == "")
											<img src="{{ asset('/assets/img/user.png') }}" class="avatar avatar-md" alt="user-thumb">    
											@else
											<img src="{{ asset('/assets/img/client/'.$barang->foto) }}" class="avatar avatar-md" alt="user-thumb">
											@endif
										</div>
									</div>
									<div class="col-sm-8 col-md-8 col-xs-8">
										<div class="feature-description">
											<h5>
												<div class="row">
													<div class="col-sm-12">
														
													</div>
													<div class="col-sm-12 mt5">
														<small class="show text-muted">{{ $barang->nama_type }}</small>
													</div>
												</div>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div> -->

						@if($barang->discount > 0)
						<div class="diskon-l">
							<div class="diskon-lr">
								{{ $barang->discount }}%
							</div>
						</div>
						@else

						@endif
						<div class="row">
							<div class="col-md-12 text-center"> 
								<div class="item-image">
									<a href="{{ url('/detail-produk/'.$barang->id) }}">
										<img src="{{ asset("/assets/img/promo/".$barang->gambar) }}" class="img-responsive">
									</a>
								</div>
							</div>
						</div>
						<div class="item-content no-lh mt15">
							<div class="row">
								<div class="col-md-12">
									<div class="product-title"> 
										<i class="ti ti-user mr5"></i>
										<em class="small">
											<a href="{{ url('/profil-page/'.$barang->username) }}" class="text-muted">
												{{ $barang->nm_depan }} {{ $barang->nm_belakang }}
											</a>
										</em> 
									</div>
									<div class="product-title"> 
										<h5 class="text-muted" ><a href="{{ url('/detail-produk/'.$barang->id) }}">{{ $barang->nm_barang }}</a></h5> 
									</div>
								</div>
							</div>
							<div class="row mb10">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-6 col-xs-6">
											<small class="text-muted"> Rp. {{ number_format($barang->harga) }}</small>

										</div>
										<div class="col-sm-6 col-xs-6 text-right">
											@if($barang->discount > 0)	
											<small class="text-muted"> <del>Rp. {{ ($barang->harga * $barang->discount / 100) + $barang->harga }}</del></small>
											@else

											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>

<section class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 mb15">
				<div class="section-title bl pl15">
					<h4 class="sub-heading" data-appear-top-offset="-190">Daftar <span class="color"><b>Populer Produk</b></span></h4>
					<h6 class="heading mb25"  data-appear-top-offset="-200"><em>Daftar Produk yang sedang populer di Full Of Stars </em></h6>
				</div>
			</div>
		</div>

		<div class="row">
			@foreach($barangs as $barang)
			<div class="col-md-3 col-xs-6">
				<div class="border mb10">
					@if($barang->discount > 0)
					<div class="diskon-l">
						<div class="diskon-lr">
							{{ $barang->discount }}%
						</div>
					</div>
					@else

					@endif
					<div class="row">
						<div class="col-md-12 text-center"> 
							<div class="item-image">
								<a href="{{ url('/detail-produk/'.$barang->id) }}">
									<img src="{{ asset("/assets/img/promo/".$barang->gambar) }}" class="img-responsive">
								</a>
							</div>
						</div>
					</div>
					<div class="item-content no-lh mt15">
						<div class="row">
							<div class="col-md-12">
								<div class="product-title"> 
									<i class="ti ti-user mr5"></i>
									<em class="small">
										<a href="{{ url('/profil-page/'.$barang->username) }}" class="text-muted">
											{{ $barang->nm_depan }} {{ $barang->nm_belakang }}
										</a>
									</em> 
								</div>
								<div class="product-title"> 
									<h5 class="text-muted"><a href="{{ url('/detail-produk/'.$barang->id) }}">{{ $barang->nm_barang }}</a></h5> 
								</div>
							</div>
						</div>
						<div class="row mb10">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<small class="text-muted"> Rp. {{ number_format($barang->harga) }}</small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>

	</div>
</section>

<section class="content-section" >

	<div class="container">

		<div class="row">
			<div class="col-md-6 col-xs-6 mb15">
				<div class="section-title bl pl15">

					<h4 class="sub-heading" data-appear-top-offset="-190"> Full Of  <span class="color"><b>STARS</b> Artist</span></h4>
					<h6 class="heading mb25"  data-appear-top-offset="-200"><em> Ini Dia Sang Creator Produk Full Of Stars </em></h6>
				</div>
			</div>

			<div class="col-sm-6 col-xs-6 text-right">
				<a href="{{ url('/artist') }}"><button type="button" class="btn btn-md btn-primary btn-outline mt25"> Semua Artist <i class="ti ti-arrow-right"></i></button></a>
			</div>
		</div>

		<div class="row text-left">


			<div id="promo" class="owl-carousel owl-theme">

				@foreach($artists as $artist)
				<div class="item mr10">
					<a href="{{url('/profil-page/'.$artist->username)}}">

						<div class="row">

							<div class="col-sm-12 text-center">
								@if($artist->foto == null || $artist->foto == "")
								<img src="{{ asset('/assets/img/user.png') }}" class="img-circle" width="200px">    
								@else
								<img src="{{ asset('/assets/img/client/'.$artist->foto) }}" class="img-circle" width="200px">
								@endif
							</div>

							<div class="col-md-12  text-center">       
								<a href="{{url('/profil-page/'.$artist->username)}}" class="mb10"><h4><b>{{ $artist->nm_depan }} {{ $artist->nm_belakang }}</b></h4></a>

								<div class="row mb10 ">
									<div class="col-md-12">
										<a class="btn btn-primary btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ $artist->facebook }}" target="_blank">
											<i class="fa fa-facebook"></i>
										</a>

										<a class="btn btn-primary btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ $artist->twitter }}" target="_blank">
											<i class="fa fa-twitter"></i>
										</a>

										<a class="btn btn-primary btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ $artist->instagram }}" target="_blank">
											<i class="fa fa-instagram"></i>
										</a>
									</div>

								</div>

							</div>

						</div>
					</a>

				</div>
				@endforeach

			</div>
		</div>
		<hr>
	</div>
</section>



<section class="content-section">
	<div class="container">

		<div class="row">
			<div class="col-md-12 col-xs-12 mb15">
				<div class="section-title bl pl15">
					<h4 class="sub-heading" data-appear-top-offset="-190"> Full Of Stars  <span class="color"><b> News</b></span> </h4>
					<h6 class="heading mb25"  data-appear-top-offset="-200"><em>Dapatkan Berita Terupdate Full Of Stars Disini </em></h6>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 mb25">
				<h4>Twitter</h4>
				<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/fullofstars_id" data-widget-id="654631752191770624" data-chrome=" noheader noscrollbar">Tweet oleh @fullofstars_id</a>
			</div>

			<div class="col-md-6 ">
				

				
				<div class="row">
					<div class="col-sm-6 text-left">
						<h4>News Blog</h4>
					</div>
					<div class="col-sm-6 text-right">
						<a href="{{ url('/blog') }}"><h4 class="color">More <i class="ti ti-arrow-right "></i></h4></a>
					</div> 
				</div>

				<ul class="list-group">
					@foreach($blogs as $blog)
					<li class="list-group-item no-p">
						<div class="row p5">
							<div class="col-sm-3 col-xs-3">
								<img src="{{ asset('/assets/img/blog/'.$blog->gambar) }}" alt="image news">
							</div>
							<div class="col-sm-9 col-xs-9">
								<div class="product-title"> 
									<a href="{{ url('/blogreview/'.$blog->id) }}" class="h5">{{ $blog->judul }}</a>
								</div>
								<ul class="list-inline">
									<li><i class="fa fa-clock-o text-muted"> {{ date('d/m/Y',strtotime($blog->created_at)) }}</i></li>
								</ul>
								
							</div>
						</div>
					</li>
					@endforeach
				</ul>
				
				
			</div>
		</div>

	</div>
</section>

<!--
<section class="content-section light">
	<div class="container no-p">
		<div class="row mt25">
			<div class="col-sm-5">
				<img src="{{ asset('/assets/img/hape1.png') }}" class="img-rensponsive" alt="get-app">
			</div>
			<div class="col-sm-6 col-sm-offset-1">
				<h2><b>Get the <span class="color"> App </span></b> </h2>
				<h4><p>Lebih gampang belanja dengan smartphone anda di Full Of Stars, dimana pun dan kapan pun.</p></h4>
				<div class="row mt25">
					<div class="col-sm-4 mb5 col-xs-5 mr10">
						<a href="#"><img src="{{ asset('/assets/img/googleplay.png') }}"></a>
					</div>
					<div class="col-sm-4 mb5 col-xs-5">
						<a href="#"><img src="{{asset('/assets/img/appstore.png') }}"></a>
					</div>
				</div>

			</div>
			

			
		</div>
	</div>
</section>

<section class="content-section banner pt20 pb20">
	<div class="container">

		<div class="row">

			<div class="col-sm-5">
				<h2><b>Behind The Scene <span class="color"> Full Of Stars </span></b> </h2>
				<h4 style="line-height:30px;"><p style="text-align:justify">"Para artis kebanggaan Indonesia telah menorehkan karya-karya produknya di Full Of Star. kita intip yuk, sesi foto persiapan productnya mereka dan jangan lupa subscribe, biar bisa pantengin terus karya-karya product artis yang lainnya...!"  </p></h4>
				<a href="https://www.youtube.com/channel/UCGnmMrQHRit8vjYw9wGsztQ" type="button" class="btn btn-color  btn-outline btn-lg mt25 mb25"> <i class="fa fa-youtube-play mr15"> Watch channel </i> </a>
			</div>

			<div class="col-sm-6 col-sm-offset-1">
				<div class="embed-responsive embed-responsive-16by9">
				  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZGllDHHvY_8" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

</section> 

-->

<section class="content-section text-center pt20  bg-color-s">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 ">
				<h3 data-animation="fadeInUp" data-appear-top-offset="-190" class="animated fadeInUp done pb20 text-white"> 
				Kita intip yuk sesi foto dan persiapan mereka seperti apa, jangan lupa <em>subscribe </em> video di channel kami</h3>

				<div class="call-to-action heading-font mt0 mb20">
	                
	                <a href="https://www.youtube.com/watch?v=ZGllDHHvY_8" class="btn btn-white btn-outline btn-lg popup-video">
	                    <i class="fa fa-play mr10"></i>
	                    <span>Watch the video</span>
	                </a>
	            </div>

			</div>
		</div>
	</div>
</section>

</div>

@stop