@extends('Master.client')

@section('content')
<!-- Blog contents -->
<section class="content-section blog-posts">
    <div class="container">
        <div class="row">

            <!-- Blog posts -->
            <div class="col-sm-9 ">

                <!--  blog post -->
                <div class="post">
                    <div class="post-title">
                        <h1 class="mt25">
                            <a class="transition" href="{{ url('/blogreview/'.$data->id) }}">{{ $data->judul }}</a>
                        </h1>
                        <div class="post-meta heading-font">
                            <small>Posted by <a href="javascript:;">{{ $data->nm_depan }} {{ $data->nm_belakang }}</a><span class="spacer"></span><a href="javascript:;">8 Comments</a></small>
                            <div class="post-date heading-font">
                                <span class="date">{{ date('d',strtotime($data->created_at)) }}</span> {{ date('M Y',strtotime($data->created_at)) }}
                            </div>
                        </div>
                    </div>

                    <div class="post-media">
                        <img src="{{ asset('/assets/img/blog/'.$data->gambar) }}" class="img-rounded img-responsive" alt="">
                    </div>
                    <div class="post-content">
                            <?php 
                            echo htmlspecialchars_decode(stripslashes($data->content)); 
                            ?> 

                    </div>
                </div>
                <!-- /blog post -->
                    </div>
                    <!-- /Blog posts -->

                    <!-- Blog sidebar -->
                    <div class="col-sm-3 sidebar">

                        <!-- Search form -->
                        <div class="widget search">
                            <form class="form-inline" role="form">
                                <div class="search-form">
                                    <button class="search-button" type="submit" title="Search"><i class="ti-search"></i>
                                    </button>
                                    <input type="text" class="form-control" placeholder="Search blog">
                                </div>
                            </form>
                        </div>
                        <!-- /Search form -->

                        <!-- Categories list -->
                        <div class="widget categories">
                            <h6>CATEGORIES</h6>
                            <ul>
                              @foreach($kategoris as $kategori)
                                <li>
                                    <a href="javascript:;">{{ $kategori->nm_kategori }}</a>
                                </li>
                              @endforeach
                            </ul>
                        </div>
                        <!-- /Categories list -->

                        <!-- Recent posts list -->
                        <div class="widget recent-activity">
                            <h6>RECENT POST</h6>
                            <ul>
                            @foreach($recents as $recent)
                                <li class="clearfix mb5">
                                    <a href="{{ url('/blogreview/'.$recent->id) }}">{{ $recent->judul }}</a>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                        <!-- /Recent posts list -->

                    </div>
                    <!-- Blog sidebar -->
                </div>
            </div>
        </section>
        <!-- Blog contents -->


        @endsection