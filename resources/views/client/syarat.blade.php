@extends('Master.client')

@section('content')
	 <section class="content-section mb25">
        <div class="container">
            <div class="row mt25">
                <div class="col-md-8 col-sm-offset-2 text-center">
                    <div class="section-title">
                        
                        <h4 class="sub-heading small no-lh">Syarat & ketentuan</h4>
                        <h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row mt20">
                
                <div class="col-md-8 col-sm-offset-2 text-left">
                    <h3><b>KETERSEDIAAN PRODUK</b></h3>
                    <h4 class="lh-1"><p>
                        <ol>
                            <li>
                                Fullofstars.co.id adalah situs interaktif non ritel yang menyediakan berbagai macam produk unik dan berkualitas untuk pria dan wanita, seperti pakaian, aksesoris, sepatu, tas, serta produk lainnya. Produk - produk di FullOfStars adalah produk artis nasional ternama dan berkualitas dengan harga terjangkau. Semua transaksi pembelian hanya dilakukan secara offline dengan mendatangi tiap agent yang ada di kota anda.
                            </li>
                            <li>
                                Produk di Fullofstars.co.id adalah ready stock. Jika produk yang diinginkan tertera “sold out”, Andadapat menghubungi Care Center dan melakukan pemesanan.  
                            </li>
                            <li>
                                Selalu lakukan refresh (F5) untuk mendapatkan update informasi terbaru.
                            </li>
                            <li>
                               <em> Request products are available! </em> Fullofstars.co.id menyediakan produk lain yang mungkin belum di-upload di online store. Silahkan hubungi Care Center kami.
                            </li>
    
                        </ol>
                    </p></h4>
                    <hr>

                    <h3><b>PENUKARAN PRODUK (RETUR)</b></h3>
                    <h4 class="lh-1">
                        <p>
                            <ol>
                                <li>
                                   Seluruh produk yang dibeli oleh Anda telah melalui proses quality control untuk menjaga dari kerusakan / kecacatan produk.
                                </li>
                                <li>
                                    Jika produk yang diterima dalam keadaan rusak / cacat , Anda dapat menukarkannya dengan produk yang sama, dengan ketentuan sebagai berikut :
                                    <ol>
                                        <li>
                                            Segera hubungi <a href="index.html"> <span class="color"> Care Center</span></a> atau agent jika produk yang dibeli rusak / cacat saat Anda membelinya.
                                        </li>
                                        <li>
                                            Full Of Stars akan segera merespon mengenai keluhan Anda apabila produk rusak / cacat dikarenakan kesalahan pada sistem produksi dan pengembalian produk kepada Full Of Stars adalah BEBAS biaya.
                                        </li>
                                        <li>
                                            Produk yang ditukarkan (retur)  harus dalam <b>kondisi belum dipakai, lengkap dengan <em> tag dan packing </em></b>.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </p>
                    </h4>
                </div>

            </div>
        </div>  
    </section>
@endsection