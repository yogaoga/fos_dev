@extends('Master.client')

@section('content')

<section class="content-section content-center banner">

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">

					<div class="p25">  
						<div class="row">
							<div class="col-sm-12 text-center">
								<h4>Tinggal kan pesan ke Distributor / Reseller</h4>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<form action="{{ url('/message') }}" method="post">

									<div class="form-group">
										<div class="row">
											<div class="col-sm-12">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="id" value="{{ $id }}">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Email</div>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" required name="email" class="form-control">
													</div>
												</div>
											</div>
										</div>
										</div>


											<div class="form-group">
												<div class="row">
													
													<div class="col-sm-12">
														
														<div class="row">
															<div class="col-sm-12">
																<div class="form-label">Pesan</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-12">
																<textarea name="content" class="form-control"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group">
														<div class="control">
															<button class="btn btn-primary" type="submit">Kirim</button>	
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>

		@endsection