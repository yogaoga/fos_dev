@extends('Master.client')

@section('content')

<section class="content-section banner">
	<div class="container">
		<form action="{{ url('/distributor') }}" method="post">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">

					<h2><span class="color"><i class="fa fa-shopping-cart mr15"></i>Daftar Belanja</span></h2>

					<hr>
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">

								<table class="table table-hover">
									<thead> 
										<tr> 
											<th>Nama Barang</th> 
											<th>Qty</th>
											<th>Harga</th> 
											<th>Diskon</th>
											<th></th>
										</tr> 
									</thead> 
									<tbody> 
									@if(count($barangs) > 0)
										@foreach($barangs as $barang)
										<tr> 
											<td>{{ $barang['nm_barang'] }}</td>
											<td>
												<div class="">
													<input type="hidden" name="kd_barang[]" value="{{ $barang['kd_barang'] }}">
													<input type="text" class="form-control" name="qty[]" style="width:60px" placeholder="Qty" value="1">
												</div>
											</td> 
											<td>RP. {{ $barang['harga'] }}</td> 
											<td>Rp. @if(empty($barang['diskon'])) 0 @else {{$barang['diskon']}} @endif </td>
											<td><a href="{{ url('/hapuskeranjang/'.$barang['nm_barang']) }}">x</a></td>
										</tr> 
										@endforeach
									@else
										<tr>
											<td colspan="5">Barang Tidak ada yang anda pilih silakan klik <a href="{{url('/product')}}"> Produk </a> untuk membeli barang</td>
										</tr>
									@endif
									</tbody> 
								</table>
							</div>
						</div>
					</div> 

					<div class="row">
						<div class="col-sm-5 pull-right"> 
							<div class="" role="group" aria-label="...">
								<a href="{{ url('/product') }}" type="button" role="button" class="btn btn-success btn-md no-radius"><i class="fa fa-plus mr5"></i>Tambah Barang</a>
								<button type="submit" class="btn btn-primary btn-md no-radius"><i class="fa fa-check mr5"></i>Selesai Belanja</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</form>



	</div>
</section>

@endsection