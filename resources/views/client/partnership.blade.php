@extends('Master.client')

@section('content')
  <section class="content-section mb25">
        <div class="container">
            <div class="row mt25">
                <div class="col-md-8 col-sm-offset-2 text-center">
                    <div class="section-title">
                        
                        <h4 class="sub-heading small no-lh">Mitra</h4>
                        <h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row mt20">
                
                <div class="col-md-6 text-left">
                    <h4 class="lh-1"><p>Ingin menjadi mitra bisnis Kami ? Bersama Full Of Stars, Anda bisa mendapatkan berbagai macam keuntungan sekaligus mendapatkan pelayanan yang terbaik dari Kami, di antaranya:    
                    <ol>
                        <li>Pemegang lisensi Full Of Stars bisa memlilih 1 (satu) wilayah / kota untuk menjadi Distributor utama di wilayah / kota tersebut.</li>
                        <li>Harga lisensi yang terjangkau dan memiliki nilai investasi yang sangat baik.</li>
                        <li>Bermitra langsung dengan puluhan Artis Nasional.</li>
                        <li>Akan mendapatkan booth yang sangat menarik beserta produk dan kelengkapanya.</li>
                        <li>Akan mendapatkan promosi langsung dari Artis dan Full Of Stars team.</li>
                        <li><a href="{{ url('index') }}"><span class="color"><b><ins> Fullofstars.co.id </ins></b></span></a> merupakan situs profesional dan akan selalu dikembangkan.memiliki komunitas aktif mencapai ratusan ribu secara online maupun offline.</li>
                        <li><a href="{{ url('index') }}"><span class="color"><b><ins> Fullofstars.co.id </ins></b></span></a>merupakan situs profesional dan akan selalu dikembangkan.</li>
                        <li>Sistem pendukung E-commerce yang baik akan memberikan transaksi yang aman karena calon mitra dan reseller yang berinteraksi langsung dengan calon pembeli.</li>
                        <li>Penyimpanan dan pengiriman barang selalu dimanage secara profesional dan di cek secara mendetail</li>
                    </ol>
                    Hubungi Care Center kami jika Anda tertarik menjadi rekan bisnis <a href="{{ url('index') }}"><span class="color"><b><ins> Fullofstars.co.id </ins></b></span></a> .
                    </p></h4>
                </div>

                <div class="col-md-4 col-sm-offset-1 mt25  text-left">

                    <img src="{{ asset('/assets/img/2-01.png') }}" class="img-responsive mt25">
                    
                </div>

            </div>
        </div>  
    </section>
@endsection