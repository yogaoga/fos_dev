@extends('Master.client')

@section('content')
<section class="content-section  pb25 light">
	<div class="container">
		<div class="row mb10">
			<div class="col-sm-12">
				<ul class="list-inline mb0">
					<li>
						<div class="fb-like" data-href="http://fullofstars.co.id/" data-layout="button_count" data-action="like" data-width="20px" data-show-faces="true" data-share="false"></div>
					</li>
					
					<li>
						<div class="fb-share-button" data-href="http://fullofstars.co.id/" data-width="20px" data-layout="button_count"></div>
					</li>
					
				</ul>
			</div>
		</div>
		<div class="row ">

			<div class="col-sm-5 col-xs-12">
				<div class="diskon-l mt5 ml5">
					<div class="diskon-lr">
						@if($barang->discount > 0)
						{{ $barang->dicount }}
						@else

						@endif
					</div>
				</div>
				<div id="sync1" class="owl-carousel">
					
					<div class="item"><img src="{{ asset('/assets/img/promo/'.$barang->gambar) }}" width="700px" class="img-responsive"></div>
					
				</div>

				<div id="sync2" class="owl-carousel hidden-xs">
					<div class="item"><img src="{{ asset('/assets/img/promo/'.$barang->gambar) }}" class="img-responsive"></div>
				</div>
			</div>

			<div class="col-sm-6 col-sm-offset-1 col-xs-12">
				<div class="row">
					<div class="col-sm-9 col-md-8">
						<div class="heading-detail">
							<h1>{{ $barang->nm_barang }}</h1>
						</div>
						<h4 class=" color "><b>Rp. {{number_format($barang->harga)}}</b></h4> 
						@if($barang->discount > 0)
						
						<h4 class="small text-muted">  <del>Rp. {{ $barang->harga * $barang->discount / 100 }}</del> </h4>
						@else

						@endif

						
						<ul class="list-inline mb0">
							<li>
								<h6 class="text-muted"><i class="ti ti-user	"></i><a href="{{ url('/profil-page/'.$artist->username) }}"><strong>  {{ $artist->nm_depan }} {{ $artist->nm_belakang }}</strong></a></h6>
							</li>
							<li>
								<h3 class="small text-muted"><i class="ti ti-tag"></i><em> {{  $barang->nama_type }}</em></h3>
							</li>
						</ul>
					</div>
					<div class="col-sm-3 col-md-3 col-md-offset-1">
						<div class="pull-right hidden-xs mt25">
							@if($artist->foto == null || $artist->foto == "")
							<img src="{{ asset('/assets/img/user.png') }}" class="img-circle" width="100px">    
							@else
							<img src="{{ asset('/assets/img/client/'.$artist->foto) }}" class="img-circle" width="100px">
							@endif
						</div>
					</div>
				</div>
				<hr>

				<div class="row mt25">
					<div class="col-sm-12">

						<dl class="accordion">
							<dt>
								<a href=""><b>Deskripsi</b></a>
							</dt>
							<dd>
							<p>
								{{ $barang->deskripsi }}
							</p>
							</dd>
						</dl>
						
						</div>
					</div>
					<hr> 
					
					<form action="{{ url('/keranjang') }}" method="get"> 
						<div class="row"> 
							<div class="col-sm-4">
								<div class="form-group">
									<label class="control-label">Pilih Warna:</label>
									<select class="form-control" name="warna">
										<option>- Pilih Warna -</option>
										@foreach($warnas as $warna)
										<option value="{{ $warna->id }}">{{ $warna->nm_warna }}</option>
										@endforeach
									</select>
									
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label class="control-label">Pilih Size:</label>
									<select class="form-control" name="size">
										<option value="1">S</option>
										<option value="2">M</option>
									</select>
									
								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label class="control-label">Qty</label>
									<input type="form" class="form-control" id="qty" placeholder="Qty" value="1">
									
								</div>
							</div>

						</div>
						<div class="row mt25">
							
							<div class="col-sm-12 col-xs-12 text-white">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id_barang" value="{{ base64_encode($barang->id) }}">

								<button type="submit" class="btn btn-primary btn-block btn-lg mt20 pull-right" role="button"><i class="fa fa-arrow-right mr10"></i> Lanjutkan </button>
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

	</section>

	<section class="content-promo light">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 mb15">
					<div class="section-title pl15">
						<h4 class="sub-heading" data-appear-top-offset="-190">Produk <span class="color"><b>Lainnya</b></span></h4>
					</div>
					<hr>
				</div>
			</div>

			<div class="row">
				@foreach($barangs as $data)    
				<div class="col-md-3 col-xs-6">
					<div class="border bg-white m5">
						@if($data->discoint > 0)
						<div class="diskon-l">
							<div class="diskon-lr">
								10%
							</div>
						</div>
						@else

						@endif

						<div class="row">
							<div class="col-md-12 text-center"> 
								<div class="item-image">
									<a href="#">
										<img src="{{ asset('/assets/img/promo/'.$data->gambar) }}" class="img-responsive">
									</a>
								</div>
							</div>
						</div>
						<div class="item-content no-lh mt15">
							<div class="row">
								<div class="col-md-12">
									<div class="product-title"> 
										<i class="ti ti-user mr5"></i>
										<em class="small">
											<a href="{{ url('/profil-page/'.$data->username) }}" class="text-muted">
												{{ $data->name }}
											</a>
										</em> 
									</div>
									<div class="product-title"> 
										<h5 class="text-muted" ><a href="#">{{ $data->nama_type }}</a></h5> 
									</div>
								</div>
							</div>
							<div class="row mb10">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-12 col-xs-12">
											<small class="text-muted"> Rp. {{ number_format($data->harga) }}</small>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div> 
		</div>
	</section>>
	@endsection