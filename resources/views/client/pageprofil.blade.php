@extends('Master.client')

@section('content')


<section class="content-promo">
	<div class="container no-p">

		<div class="row">
			<div class="col-sm-12">
				<div class="banner-user" style="background-image: url({{ asset('/assets/img/cover/'.$artist->cover) }});">

					<div class="row"> 
						<div class="col-sm-12" >
							@if($artist->foto == null || $artist->foto == "")
							<div class="img-user-u">
								<img src="{{ asset('/assets/img/faceless.jpg') }}" class="img-circle border-img" width="120px">
							</div>
							@else

							<div class="img-user-u">
								<img src="{{ asset('/assets/img/client/'.$artist->foto) }}" class="img-circle border-img" width="120px">
							</div>
							@endif
							<h4 class="user-name"><b>{{ $artist->nm_depan }} {{ $artist->nm_belakang }}</b></h4>
						</div>

					</div>
					<div class="overlay-darken-1"></div>
				</div>
			</div>
		</div>




	</div>
</section>


<section class="content-section">

	<div class="container no-p">

		<div class="row">
			<div class="col-sm-9 mt10 ">

				<div class="row ">
					@if(count($barangs) > 0)
					@foreach($barangs as $data)
					<div class="col-md-3 col-xs-6">
						<div class="border bg-white">
							@if($data->discount > 0)
							<div class="diskon-l">
								<div class="diskon-lr">
									{{ $data->discount }}%
								</div>
							</div>
							@else

							@endif
							<div class="row">
								<div class="col-md-12 text-center"> 
									<div class="item-image">
										<a href="{{ url('/detail-produk/'.$data->id) }}">
											<img src="{{ asset("/assets/img/promo/".$data->gambar) }}" class="img-responsive">
										</a>
									</div>
								</div>
							</div>
							<div class="item-content no-lh mt15">
								<div class="row">
									<div class="col-md-12">
										<div class="product-title"> 
											<h5 class="text-muted" ><a href="{{ url('/detail-produk/'.$data->id) }}">{{ $data->nm_barang }}</a></h5> 
										</div>
									</div>
								</div>
								<div class="row mb10">
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-12 col-xs-12">
												<small class="text-muted"> Rp. {{ number_format($data->harga) }}</small>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@else
					@foreach($items as $item)
					<div class="col-md-3 col-xs-6">
						<div class="border bg-white">
							@if($item->discount > 0)
							<div class="diskon-l">
								<div class="diskon-lr">
									{{ $item->discount }}%
								</div>
							</div>
							@else

							@endif
							<div class="row">
								<div class="col-md-12 text-center"> 
									<div class="item-image">
										<a href="{{ url('/detail-produk/'.$item->id) }}">
											<img src="{{ asset("/assets/img/promo/".$item->gambar) }}" class="img-responsive">
										</a>
									</div>
								</div>
							</div>
							<div class="item-content no-lh mt15">
								<div class="row">
									<div class="col-md-12">
										<div class="product-title"> 
											<h5 class="text-muted" ><a href="{{ url('/detail-produk/'.$item->id) }}">{{ $item->nm_barang }}</a></h5> 
										</div>
									</div>
								</div>
								<div class="row mb10">
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-12 col-xs-12">
												<small class="text-muted"> Rp. {{ number_format($item->harga) }}</small>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif
				</div>


				<div class="row">
					<div class="col-sm-12 text-center mt25 mb25">
						
						<div class="text-center">
							<ul class="pagination pagination-lg">
							@if(count($barangs) > 0)
							{!! $barangs->render() !!}
							@else
							{!! $items->render() !!}
							@endif
							</ul>
						</div>
						
					</div> 
				</div>
			</div>
			<div class="col-sm-3">
				<div class="row  mt10 ">
					<div class="col-sm-12 bg-white pt10 ">
						<h4>Terhubung Di: 
						</h4>
						<hr>
						<div class="row ">

							<div class="col-sm-12 col-xs-12 mt5 mb5">
								<div class="row mb10">
									<div class="col-sm-2 col-xs-2">
										<a class="btn btn-sos btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ url('/profilartist/'.$artist->username)}}">
											<i class="fa fa-newspaper-o"></i>
										</a>
									</div>
									<div class="col-sm-8 col-xs-8">
										<span class="text-muted">Tentang</span>
									</div>
								</div>

								<div class="row mb10">
									<div class="col-sm-2 col-xs-2 ">
										<a class="btn btn-sos btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ $artist->facebook }}" target="_blank">
											<i class="fa fa-facebook"></i>
										</a>
									</div>
									<div class="col-sm-8 col-xs-8">
										<span class="text-muted">Facebook</span>
									</div>
								</div>

								<div class="row mb10">
									<div class="col-sm-2 col-xs-2">
										<a class="btn btn-sos btn-social-icon btn-rounded btn-outline btn-sm ml5 mr5" href="{{ $artist->twitter }}" target="_blank">
											<i class="fa fa-twitter"></i>
										</a>
									</div>
									<div class="col-sm-8 col-xs-8">
										<span class="text-muted">Twitter</span>
									</div>
								</div>

								<div class="row mb10">
									<div class="col-sm-2 col-xs-2">
										<a class="btn btn-sos btn-social-icon btn-rounded btn-outline btn-sm ml5 " href="{{ $artist->instagram }}" target="_blank">
											<i class="fa fa-instagram"></i>
										</a>
									</div>
									<div class="col-sm-8 col-xs-8">
										<span class="text-muted">Instagram</span>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="row bg-white mt10 ">
				@if(count($widget) > 0)
					<div class="col-sm-12">
						<h4>Timeline Twitter</h4>
						<hr>
						<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/{{ $widget->username }}" data-widget-id="{{ $widget->widget_id }}" data-chrome=" noheader noscrollbar">Tweet oleh @{{ $widget->username }}</a>
					</div>
				@else
					<div class="col-sm-12">
						<h4>Timeline Twitter</h4>
						<hr>
						<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/fullofstars_id" data-widget-id="654631752191770624" data-chrome=" noheader noscrollbar">Tweet oleh @fullofstars_id</a>
					</div>
				@endif
				</div>
			</div>
		</div>	
	</div>

</section>

@endsection