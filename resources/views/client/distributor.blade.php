@extends('Master.client')


@section('content')
<section class="content-section">
	<div class="container-fluid">
		<div class="row">

			<div class="col-md-8 col-xs-12 col-sm-6 mt25">
				<div class=" map-content">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<input id="origin-input" class="controls" type="text" placeholder="Enter an origin location">
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<input id="destination-input" class="controls" type="text" placeholder="Enter a destination location">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<div id="mode-selector" class="controls">
								<input type="radio" name="type" id="changemode-walking" checked="checked">
								<label for="changemode-walking">Walking</label>

								<input type="radio" name="type" id="changemode-transit">
								<label for="changemode-transit">Transit</label>

								<input type="radio" name="type" id="changemode-driving" >
								<label for="changemode-driving">Driving</label>
							</div>
						</div>
					</div>

					<div id="map"></div>
				</div>
			</div>

			<div class="col-md-4 col-xs-12 col-sm-6">

				<section class="widget ">
					<div class="widget-body ">
						<h4>Cari Agent terdekat</h4>
						<hr>
						<div class="row">

							<div class="col-sm-12 col-xs-12">
								<!-- Search form -->
								<div class="widget search">
									<form class="form-inline" role="form" method="GET" action="{{ url('/distributorList') }}">
										<div class="search-form">
											<button class="search-button" type="submit" title="Search"><i class="ti-search"></i>
											</button>
											<input type="text" class="form-control" placeholder="Cari" name="cari">
										</div>
									</form>
								</div>
								<!-- /Search form -->
							</div>
						</div>
					</div>
				</section>

				@foreach($items as $data)
				<section class="widget bg-white bb">
					<div class="widget-body">

						<div class="overflow-hidden ">
							<div class="row mb5">
								<div class="col-sm-6 col-md-7 col-xs-6">
									<div class="h4">
										<a href="#"><b>{{ $data->nm_depan }} {{ $data->nm_belakang }}</b></a>
									</div>
									<small class="show"><em>{{ $data->alamat }}</em></small>

								</div>
								<div class="col-md-5 col-sm-6 col-xs-6 mt25 text-center">
									<span class="label label-success"><i class="fa fa-check mr10"></i>available stock</span> 
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-md-6 col-sm-12 col-xs-6">
									<div class="text-left text-white ">
										<a class="btn btn-info btn-social-icon btn-rounded  btn-sm mr5" data-toggle="modal" data-target="#myModal">
											<i class="fa fa-phone"></i>
										</a>
										<a class="btn btn-info btn-social-icon btn-rounded btn-sm mr5" href="{{ url('/message/'.$data->id_karyawan) }}">
											<i class="fa fa-envelope-o"></i>
										</a>
										<a class="btn btn-info btn-social-icon btn-rounded btn-sm " href="javascript:;">
											<i class="fa fa-map-marker"></i>
										</a>

									</div>
								</div>
								
							</div>
						</div>

					</div>
				</section>

				@endforeach

			</div>

		</div> 

	</div>

</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Phone</h4>
      </div>
      <div class="modal-body">
        +62 82219230981
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection


<script>

	var initialize = function() {
		var origin_place_id = null;
		var destination_place_id = null;
		var travel_mode = google.maps.TravelMode.WALKING;

		var map = new google.maps.Map(document.getElementById('map'), {
			mapTypeControl: false,
			center: {lat: -33.8688, lng: 151.2195},
			zoom: 13
		});
		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer;
		directionsDisplay.setMap(map);

		var origin_input = document.getElementById('origin-input');
		var destination_input = document.getElementById('destination-input');
		var modes = document.getElementById('mode-selector');

		map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

		var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
		origin_autocomplete.bindTo('bounds', map);
		var destination_autocomplete =
		new google.maps.places.Autocomplete(destination_input);
		destination_autocomplete.bindTo('bounds', map);

		//GeoLocation
		var infoWindow = new google.maps.InfoWindow({map: map});
		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				console.log(pos.lat);
				infoWindow.setPosition(pos);
				infoWindow.setContent('Lokasi Anda Sekarang.');
				map.setCenter(pos);
			}, function() {
				handleLocationError(true, infoWindow, map.getCenter());
			});
		} else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
}

		// Sets a listener on a radio button to change the filter type on Places
  		// Autocomplete.
  		function setupClickListener(id, mode) {
  			var radioButton = document.getElementById(id);
  			radioButton.addEventListener('click', function() {
  				travel_mode = mode;
  			});
  		}
  		setupClickListener('changemode-walking', google.maps.TravelMode.WALKING);
  		setupClickListener('changemode-transit', google.maps.TravelMode.TRANSIT);
  		setupClickListener('changemode-driving', google.maps.TravelMode.DRIVING);

  		function expandViewportToFitPlace(map, place) {
  			if (place.geometry.viewport) {
  				map.fitBounds(place.geometry.viewport);
  			} else {
  				map.setCenter(place.geometry.location);
  				map.setZoom(17);
  			}
  		}

  		origin_autocomplete.addListener('place_changed', function() {
  			var place = origin_autocomplete.getPlace();
  			if (!place.geometry) {
  				window.alert("Autocomplete's returned place contains no geometry");
  				return;
  			}
  			expandViewportToFitPlace(map, place);

  			// If the place has a geometry, store its place ID and route if we have
    		// the other place ID
    		origin_place_id = place.place_id;
    		route(origin_place_id, destination_place_id, travel_mode,
    			directionsService, directionsDisplay);
    	});

  		destination_autocomplete.addListener('place_changed', function() {
  			var place = destination_autocomplete.getPlace();
  			if (!place.geometry) {
  				window.alert("Autocomplete's returned place contains no geometry");
  				return;
  			}
  			expandViewportToFitPlace(map, place);

  			// If the place has a geometry, store its place ID and route if we have
    		// the other place ID
    		destination_place_id = place.place_id;
    		route(origin_place_id, destination_place_id, travel_mode,
    			directionsService, directionsDisplay);
    	});

  		function route(origin_place_id, destination_place_id, travel_mode,
  			directionsService, directionsDisplay) {
  			if (!origin_place_id || !destination_place_id) {
  				return;
  			}
  			directionsService.route({
  				origin: {'placeId': origin_place_id},
  				destination: {'placeId': destination_place_id},
  				travelMode: travel_mode
  			}, function(response, status) {
  				if (status === google.maps.DirectionsStatus.OK) {
  					directionsDisplay.setDirections(response);
  				} else {
  					window.alert('Directions request failed due to ' + status);
  				}
  			});
  		}

  		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  			infoWindow.setPosition(pos);
  			infoWindow.setContent(browserHasGeolocation ?
  				'Error: The Geolocation service failed.' :
  				'Error: Your browser doesn\'t support geolocation.');
  		}
  	};
  	function initMap() {

  		setTimeout(function () {
  			initialize();
  		}, 2000);

  	}
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"
  async defer></script>
