<!doctype html>
<html class="404 no-js" lang="">

<head>
	<!-- meta -->
	<meta charset="utf-8">
	<meta name="description" content="fullofstars.co.id,Full Of Stars, E-Commers, Jual, Baju, Celana, Sepatu, Aksesoris">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- /meta -->

	<title>Full Of Stars</title>

	<!-- page level plugin styles -->
	<!-- /page level plugin styles -->

	<!-- core styles -->
	<link rel="stylesheet" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/assets/themify-icons.css') }}">
	<link href="{{ asset('/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
	<!-- /core styles -->

	<!-- template styles -->
	<link rel="stylesheet" href="{{ asset('/assets/skin/blue.css') }}" id="skin">
	<link rel="stylesheet" href="{{ asset('/assets/main.css') }}">
	<link rel="stylesheet" href="{{ asset('/assets/app.css') }}">
	<link rel="stylesheet" href="{{ asset('/assets/fonts/opensans.css') }}" id="font">
	<script src="{{ asset('/assets/plugins/jquery-1.11.1.min.js') }}"></script>

	<!-- template styles -->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- load modernizer -->
        <script src="{{ asset('/assets/plugins/modernizr.js') }}"></script>
    </head>

    <!-- body -->

    <body>

    	<!-- page loading spinner -->
    	<div class="pageload">
    		<div class="loader"></div>
    	</div>
    	<!-- /page loading spinner -->



    	<!-- hero -->
    	<section id="top" class="content-section hero vertical-center">
    		<div class="parallax-cover parallax-0" style="background-image: url({{ asset('/assets/img/banner-3.jpg') }});"></div>
    		<div class="hero-parallax parallax"></div>

    		<div class="overlay-darken-7"></div>

    		<div class="container">
    			<div class="hero-container">

    				<div class="row">
    					<div class="col-sm-12">
    						<h2 class="animated bounceIn error-number">COMING SOON</h2>
    					</div>
    					<div class="col-sm-12">
    						<img src="{{ asset('/assets/img/fos-white.png') }}" width="40%" style="opacity:0.6">
    					</div>
    					<div class="hero-sub-title" data-animation="fadeInRight">
    						<p><em>sorry, we will be right back</em>
    						</p>
    					</div>
    				</div>
    			</div>
    		</div>

    	</section>
    	<!-- /hero -->

    	<!-- core scripts -->
    	<script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script> 
    	<script src="{{ asset('/assets/plugins/appear/jquery.appear.js') }}"></script>
    	<script src="{{ asset('/assets/plugins/nav/jquery.nav.js') }}"></script>
    	<script src="{{ asset('/assets/plugins/jquery.easing.min.js') }}"></script>
    	<!-- /core scripts -->

    	<!-- page level scripts -->
    	<script src="{{ asset('/assets/plugins/jquery.parallax.js') }}"></script>
    	<!-- /page level scripts -->

    	<!-- template scripts -->
    	<script src="{{ asset('/assets/js/main.js') }}"></script>
    	<!-- /template scripts -->

    	<!-- page script -->
    	<!-- /page script -->

    </body>

    </html>
