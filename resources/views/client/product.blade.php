@extends('Master.client')

@section('content')

<section class="content-promo">
	<div class="container">
		<h2 class="text-muted">Semua Produk</h2>
		<hr>
		<div class="row">
			@foreach($barangs as $barang)
			<div class="col-md-3 col-xs-6">
				<div class="border m5">
				@if($barang->discount > 0)
					<div class="diskon-l">
						<div class="diskon-lr">
							{{ $barang->discount }}%
						</div>
					</div>
				@else

				@endif
					<div class="row">
						<div class="col-md-12 text-center"> 
							<div class="item-image">
								<a href="{{ url('/detail-produk/'.$barang->id) }}">
								<img src="{{ asset("/assets/img/promo/".$barang->gambar) }}" class="img-responsive">
								</a>
							</div>
						</div>
					</div>
					<div class="item-content no-lh mt15">
						<div class="row">
							<div class="col-md-12">
								<div class="product-title"> 
									<i class="ti ti-user mr5"></i>
									<em class="small">
										<a href="{{ url('/profil-page/'.$barang->username) }}" class="text-muted">
											{{ $barang->nm_depan }} {{ $barang->nm_belakang }}
										</a>
									</em> 
								</div>
								<div class="product-title"> 
								<h5 class="text-muted" ><a href="{{ url('/detail-produk/'.$barang->id) }}">{{ $barang->nm_barang }}</a></h5> 
								</div>
							</div>
						</div>
						<div class="row mb10">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<small class="text-muted"> Rp. {{ number_format($barang->harga) }}</small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@endsection