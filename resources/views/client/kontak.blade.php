@extends('Master.client')

@section('content')
	
	 <section class="content-section mb25">
        <div class="container">
            <div class="row mt25">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="section-title">
                        
                        <h4 class="sub-heading small no-lh">Hubungi kami</h4>
                        <h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row mt20">

                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-12 mb15" data-animation="fadeInLeft" data-delay="200" data-appear-top-offset="-200">
                                <div class="feature-icon left">
                                    <i class="ti-mobile color"></i>
                                </div>
                                <div class="feature-description">
                                    <h5  class="text-uppercase"><b>Customer Service</b></h5>
                                    Phone : (022) 20561471 <br/>
                                    Email : cs@fullofstars.co.id
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-12 mb15" data-animation="fadeInRight" data-delay="200" data-appear-top-offset="-200">
                                <div class="feature-icon left">
                                    <i class="ti-camera color"></i>
                                </div>
                                <div class="feature-description">
                                    <h5  class="text-uppercase"><b>press & media</b></h5>
                                    Phone : (022) 20561471 <br/>
                                    Contact Person : Irwan Alkafrawi <br/>
                                    Email : press@fullofstars.co.id
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-12 mb15" data-animation="fadeInLeft" data-delay="200" data-appear-top-offset="-200">
                                <div class="feature-icon left">
                                    <i class="ti-link color"></i>
                                </div>
                                <div class="feature-description">
                                    <h5  class="text-uppercase"><b>Terhubung dengan kami</b></h5>
                                    <p>Terhubung dengan kami di sosial media:</p>
                                    <div class="row">

                                        <div class="col-sm-12">

                                            <a class="btn btn-social-icon btn-color btn-rounded  btn-sm ml5 mr5" href="www.facebook.com/fullofstars.co.id" style="font-size:12px">
                                                <i class="fa fa-facebook"></i>
                                            </a>

                                            <a class="btn btn-social-icon btn-color btn-rounded  btn-sm ml5 mr5" href=" www.twitter.com/fullofstars_ID" style="font-size:12px" >
                                                <i class="fa fa-twitter"></i>
                                            </a>

                                            <a class="btn btn-social-icon btn-color btn-rounded  btn-sm ml5 mr5" href="www.instagram.com/fullofstars_id" style="font-size:12px">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-12 mb15" data-animation="fadeInRight" data-delay="200" data-appear-top-offset="-200">
                                <div class="feature-icon left">
                                    <i class="ti-envelope color"></i>
                                </div>
                                <div class="feature-description">
                                    <h5  class="text-uppercase"><b>Jobs & Career</b></h5>
                                    career@fullofstars.co.id

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </section>

@stop