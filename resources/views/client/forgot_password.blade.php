@extends('Master.client')

@section('content')

<section class="content-section content-center  banner">

	<div class="container">

		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

			<section class="panel bordered">
				<div class="p15">
					<p class="text-center mb25">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>
					<form role="form" action="signin.html">
						<input type="email" class="form-control input-lg mb25" placeholder="Email address" autofocus>

						<button class="btn btn-warning btn-lg btn-block" type="submit">Send Password Reset</button>
						<div class="row">
							<div class="col-sm-12 text-center">
								<h6 class="small"> Ingat memiliki password <a href="#"><b>Masuk</b></a></h6>
							</div>
						</div>
					</form>
				</div>
			</section>
			
		</div>

	</div>
</section>

@stop