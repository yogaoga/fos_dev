@extends('Master.client')

@section('content')
<section class="content-section mb25">
	<div class="container">
		<div class="row mt25">
			<div class="col-md-8 col-sm-offset-2 text-center">
				<div class="section-title">
					
					<h4 class="sub-heading small no-lh">Kebijakan Privasi</h4>
					<h3 class="sub-heading no-lh pt5 pb10">Full Of <span class="color"> <b>Stars</b></span></h3>
					<hr>
				</div>
			</div>
		</div>

		<div class="row mt20">

			<div class="col-md-8 col-sm-offset-2 text-left">
				<h3><b>SELAMAT DATANG DI <a href="{{url('index')}}"><span class="color"><ins>FullOfStars.co.id </ins></span></a></b></h3>
				<h4 class="lh-1"><p>
					Terima kasih telah mengunjungi situs <a href="{{url('index')}}"><span class="color"><ins> <b>FullOfStars.co.id</b></ins></span></a>, Kebijakan Privasi ini memberitahukan Anda bagaimana Full Of Stars menggunakan informasi pribadi yang dikumpulkan di situs ini. Silakan baca Kebijakan Privasi ini sebelum menggunakan situs atau mengirimkan informasi pribadi apapun. Mengakses dan menggunakan situs ini menandakan Andaakan menerima semua kegiatan yang dijelaskan dalam Kebijakan Privasi. Kebijakan ini dapat berubah, dan perubahan akan diumumkan serta perubahan hanya akan berlaku untuk kegiatan dan informasi yang akan datang, bukan yang sudah terjadi.  
				</p></h4>
				<hr>

				<h3><b>PENGENALAN</b></h3>
				<h4 class="lh-1">
					<p>
						Di <a href="{{url('index')}}"><span class="color"><ins> <b> FullOfStars.co.id </b></ins></span></a> Anda akan selalu jadi yang utama. Full Of Stars ingin Anda merasa aman dan nyaman, mengetahui bahwa Full Of Stars tidak pernah memberikan ataupun menjual informasi pribadi kepada pihak manapun tanpa persetujuan dari Anda sendiri. Full Of Stars akan menjaga hak Anda untuk mendapatkan kerahasiaan ini. Bagi Anda yang belum terbiasa menggunakan situs online, mungkin hal ini akan membuat khawatir. Tetapi Full Of Stars ingin memberikan sebuah pengalaman belanja yang berbeda di <a href="{{url('index')}}"><span class="color"><ins> <b> FullOfStars.co.id </b></ins></span></a><p>Jadi, jangan khawatir, selamat berbelanja!</p>
					</p>
				</h4>
				<hr>

				<h3><b>KOLEKSI INFORMASI</b></h3>
				<h4 class="lh-1">
					<p>
						Full Of Stars mengumpulkan informasi pribadi, seperti nama, alamat untuk bersurat, alamat email, dan keterangan lainnya bagi mitra maupun reseller. Informasi ini hanya digunakan untuk keperluan database kecuali Anda memberikan izin yang akan digunakan untuk kepentingan lain, seperti mailing list Full Of Stars.
						
					</p>
				</h4>
				<hr>

				<h3><b>PELACAKAN<em> (Cookie)</em> TEKNOLOGI</b></h3>
				<h4 class="lh-1">
					<p>
						<a href="{{url('index')}}"><span class="color"><ins> <b>FullOfStars .co.id </b></ins></span></a> menggunakan cookie dan teknologi pelacakan tergantung pada fitur yang ditawarkan. Teknologi pelacakan dan cookie bermanfaat untuk mengumpulkan informasi seperti jenis browser dan sistem operasional, mendata jumlah pengunjung situs dan memahami cara pengunjung menggunakan situs.Cookie juga dapat membantu menyesuaikan situs untuk pengunjung. Informasi pribadi tidak dapat dikumpulkan melalui cookie dan teknologi pelacak lainnya. Namun, jika Anda sebelumnya memberikan informasi identitas pribadi,cookie dapat terhubung ke informasi tersebut
						
					</p>
				</h4>
				<hr>

				<h3><b>DISTRIBUSI INFORMASI</h3>
				<h4 class="lh-1">
					<p>
						Full Of Stars dapat berbagi informasi dengan instansi pemerintah atau perusahaan lainnya yang membantu Full Of Stars dalam hal mencegah penipuan atau investigasi. Full Of Stars dapat melakukannya ketika:

						<ol>
							<li>
								Diizinkan atau diharuskan oleh hukum, atau,
							</li>
							<li>
								Mencoba untuk melindungi atau mencegah penipuan atau hal yang berpotensi untuk berujung pada penipuan atau transaksi yang tidak sah, atau,
							</li>
							<li>
								Menyelidiki penipuan yang telah terjadi. Informasi ini tidak diberikan kepada perusahaan-perusahaan lain untuk kepentingan pemasaran.
							</li>
						</ol>
						
					</p>
				</h4>
				<hr>

				<h3><b>TAUTAN KE SITUS WEB LAIN</h3>
				<h4 class="lh-1">
					<p>
						Situs kami dapat berisi tautan ke situs-situs lain yang menarik. Namun, setelah Anda menggunakan tautan tersebut untuk meninggalkan situs kami, Anda harus mengetahui bahwa kami tidak memiliki kendali atas situs web lain tersebut. Harap diketahui bahwa kami tidak bertanggung jawab atas praktik privasi situs web lain tersebut dan menyarankan Anda untuk membaca pernyataan privasi dari masing-masing situs web yang Anda kunjungi yang mengumpulkan informasi pribadi.
						
					</p>
				</h4>
				<hr>

				<h3><b>KOMITMEN UNTUK KEAMANAN DATA</h3>
				<h4 class="lh-1">
					<p>
						Informasi pribadi Anda disimpan secara aman. Hanya Full Of Stars yang dapat memiliki akses ke informasi ini. Semua email dan newsletter dari situs ini memberikan pilihan kepada Anda untuk tidak disertakan ke dalam daftar mailing list berikutnya.
					</p>
					
				</h4>
				<hr>

				<h3><b>PENGALIH BISNIS</h3>
				<h4 class="lh-1">
					<p>
						Dalam hal terjadi perubahan kendali atau perubahan kepemilikan atas semua atau sebagian dari bisnis Full Of Stars atau perusahaan, termasuk situs, maka Data Pribadi Pengguna mungkin/akan menjadi bagian dari pengalihan aset tersebut.
					</p>
					
				</h4>
				<hr>

				<h3><b>PERSETUJUAN PERUBAHAN KEBIJAKAN PRIVASI</h3>
				<h4 class="lh-1">
					<p>
						Dengan menggunakan Situs atau layanan yang kami sediakan, Anda setuju dengan pengumpulan, penggunaan, penyingkapan dan pengolahan Data Pribadi Anda sebagaimana diatur dalam Kebijakan Privasi ini. Lebih lanjut, jika Anda menggunakan layanan kami, kami berhak mengumpulkan, menggunakan, menyingkap dan memproses Data Pribadi Anda sesuai dengan Kebijakan Privasi ini. Dari waktu ke waktu, Full Of Stars dapat mengubah Kebijakan Privasi ini. Sebagaimana dijelaskan pada awal Kebijakan Privasi ini, kami akan menampilkan perubahan pada Situs ini untuk informasi Anda.
						
					</p>
				</h4>
				<hr>

				<h3><b>URUTAN PRIORITAS</h3>
				<h4 class="lh-1">
					<p>
						Jika Anda telah menyetujui Persyaratan Situs Web kami, dalam hal terdapat perbedaan antara Persyaratan Situs Web tersebut dan Kebijakan Privasi ini, maka Persyaratan Situs Web yang akan berlaku.
						
					</p>
				</h4>
				<hr>

				<h3><b>DISCLAIMER</h3>
				<h4 class="lh-1">
					<p>
						Untuk menghindari keraguan, atas pengecualian yang hukum mengizinkan suatu organisasi seperti Traveloka untuk mengumpulkan, menggunakan atau menyingkap data pribadi Anda tanpa persetujuan Anda, maka izin tersebut yang diberikan oleh hukum akan terus berlaku.
						
					</p>
				</h4>
				<hr>

				<h3><b>PRIVASI KONTAK INFORMASI</h3>
				<h4 class="lh-1">
					<p>
						Apabila Anda memiliki pertanyaan, keluhan, atau komentar tentang Kebijakan Privasi Full Of Stars,Anda dapat menghubungi Care Center kami
						
					</p>
					
				</h4>
				<p>
					<div class="alert alert-info text-center mt25">
						
						<h4>"Semua foto yang ada di www.FullOfStars .co.id merupakan hak cipta Full Of Stars. Jika ada yang ingin mengambil foto dari www.FullOfStars .co.id harap menghubungi dahulu pihak sales Full Of Stars."</h4>
					</div>
				</p>
				<hr>

			</div>

		</div>
	</div>  
</section>
@endsection