@extends('Master.client')

@section('meta')
<script type="text/javascript" src="{{ asset('/js/client/client.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#tgl_lahir').datepicker();
	});
</script>
@endsection

@section('content')


<section class="content-section content-center banner">

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">

					<div class="p25">  
						<div class="row">
							<div class="col-sm-12 text-center">
								<h4>Belum memiliki account FOS ? Silahkan isi data anda disini</h4>
							</div>
						</div>
						<div class="row mb25">
							<div class="col-sm-12 text-center">
								<h6 class="small">Atau sudah punya acoount ? <a href="{{ url('/') }}"><b>Login</b></a></h6>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<form action="{{ url('/simpan') }}" method="post">

									<div class="form-group">
										<div class="row">
											<div class="col-sm-6">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Nama Depan</div>
														<span class="help">e.g. John</span>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" required name="nm_depan" class="form-control">
													</div>
												</div>
											</div>

											<div class="col-sm-6">
												
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Nama Belakang</div>
														<span class="help">e.g. Doe</span>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" required name="nm_belakang" class="form-control">
													</div>
												</div>

											</div>
										</div>

									</div>

									<div class="form-group">
										<div class="row">
											
											<div class="col-sm-6">
												
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Username</div>
														
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="form-control" name="username" onblur="checkUsername()" required>
														<i class="fa fa-spinner" id="loader" style="display:none"></i>
														<span class="text-danger" id="status"></span>
													</div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Email</div>		
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="email" class="form-control" name="email" required>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group">
										<div class="row">
											
											<div class="col-sm-6">
												
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Password</div>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="password" class="form-control" name="password" required>
													</div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Jenis Kelamin:</div>	
													</div>
												</div>

												<div class="row mt5">
													<div class="col-sm-12">
														<label class="radio-inline">
															<input id="male" type="radio" name="gender" value="1" checked="checked">
															<label for="male">Laki-Laki</label>
														</label> 
														<label class="radio-inline">
															<input id="female" type="radio" name="gender" value="2">
															<label for="female">Perempuan</label>
														</label>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group">
										<div class="row">
											
											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">HandPhone</div>	
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="form-control" name="hp">
														<input type="hidden" name="jabatan" value="6" >
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="form-group">
										<div class="row">
											
											<div class="col-sm-6">
												
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Tempat Lahir</div>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="form-control" name="tempat_lahir">
													</div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Tanggal Lahir</div>
													</div>
												</div>

												<div class="">
													<input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" data-provide="datepicker" value="{{date('m/d/Y')}}">
												</div>
											</div>
										</div>

									</div>

									<div class="form-group">
										<div class="row">
											
											<div class="col-sm-6">
												
												<div class="row">
													<div class="col-sm-12">
														<div class="form-label">Alamat</div>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-12">
														<textarea name="alamat" class="form-control"></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="control">
													<button class="btn btn-primary" type="submit">Daftar</button>	
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>

@endsection