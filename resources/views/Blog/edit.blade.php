@extends('Master.Template')

@section('meta')
<link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{ asset('/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }} " type="text/javascript"></script>
<script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		$('[name="content"]').wysihtml5();
	});
</script>
@endsection

@section('title')
Edit Blog
@endsection

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<div class="row">
				<div class="col-sm-9 col-xs-9">
	             	<h4>Data Blog</h4>
	            </div>
	            <div class="col-sm-3 col-xs-3 text-right">
	              <div class="tools text-left">
	                <a href="javascript:;" class="collapse"></a> 
	              </div>
	            </div>	            
	        </div>
		</div>
		<div class="grid-body no-border">
			<form action="{{ url('adminblog\edit') }}" method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{ $item->id }}">
				<div class="row column-seperation">
					<div class="col-md-8 col-sm-8 col-xs-8">
						<div class="form-group">
							<div class="form-label">Judul</div>
							<div class="control">
								<input type="text" required name="judul" class="form-control" value="{{ $item->judul }}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-label">Cover</div>
							<div class="control">
								<input type="file" name="gambar" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<div class="form-label">Content</div>
							<div class="control">
								<!-- <input type="text" class="form-control" name="job_desk"> -->
								<textarea id="some-textarea" name="content" style="width:100%; height:300px;">{{ $item->content }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="control">
								<button type="submit" class="btn btn-primary">Ubah</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection