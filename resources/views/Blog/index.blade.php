@extends('Master.Template')

@section('meta')
<script type="text/javascript" src="{{ asset('/js/blog/blog.js') }}"></script>
<style type="text/css">
	.items:hover td .tbl-opsi{
		display: block !important;
	}
</style>
@endsection

@section('title')
Blog
@endsection

@section('content')

<div class="row">
	<div class="col-sm-9">
		<div class="grid simple">
			<div class="grid-title no-border">
				<div class="row">
					<div class="col-sm-9 col-xs-9">
						<h4>{{ $items->total() }} Blog <span class="semi-bold">ditemukan</span></h4>
					</div>
					<div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		                <a href="javascript:;" class="reload"></a> 
		              </div>
		            </div>
	            </div>
			</div>
			<div class="grid-body no-border">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th><a href="javascript:;">Judul</a></th>
								<th><a href="javascript:;">Petugas</a></th>
								<th><a href="javascript:;">Tgl Buat</a></th>
								<th></th>
							</tr>
						</thead>
						<tbody class="contents-items">
							<?php $no = $items->currentPage() == 1 ? 1 : ($items->perPage() + $items->currentPage()) -1 ; ?>
							@forelse($items as $item)
							<tr class="item_{{ $item->id }} items">
								<td>{{ $no }}</td>
								<td>
									<a href="javascript:;" title="{{ $item->judul }}" data-toggle="tooltip" data-placement="bottom">{{ $item->judul }}</a>
									<div style="display:none;" class="tbl-opsi">
										<small>[
											<a href="{{ url('adminblog/review/'.$item->id) }}">Lihat</a>
											@if(Auth::user()->permission > 2)
											<!-- hanya admin yang memiliki akses Write dan Execute -->
											| <a href="{{ url('adminblog/edit/'.$item->id) }}">Edit</a> 
											@endif
											]</small>
										</div>
									</td>
									<td>{{ $item->nm_depan }} {{ $item->nm_belakang }}</td>
									<td>{{ $item->created_at }}</td>
									<td class="text-right">
										@if(Auth::user()->permission > 2)
										<button type="button" class="close hapus" data-nama="{{ $item->judul }}" data-id="{{ $item->id }}"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
										@endif
									</td>
								</tr>
								<?php $no++; ?>
								@empty

								@endforelse
							</tbody>
					</table>
				</div>

				<div class="text-right pagins">
					{!! $items->render() !!}
				</div>
			</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="grid simple">
				<div class="grid-title no-border">
					<h4></h4>
					
				</div>
				<div class="grid-body no-border">
					<p>
						<a class="btn btn-block btn-primary" href="{{ url('/adminblog/create') }}"><i class="fa fa-plus"></i> Tambah Blog</a>
					</p>
				</div>

			</div>
			<div class="grid simple">
				<div class="grid-title no-border">
					<div class="row">
						<div class="col-sm-9 col-xs-9">
			              <h4>Cari Blog</h4>
			            </div>
			            <div class="col-sm-3 col-xs-3 text-right">
			              <div class="tools text-left">
			                <a href="javascript:;" class="collapse"></a> 
			              </div>
			            </div>
			            
			         </div>
				</div>
				<div class="grid-body no-border">
					<p>
						<div class="form-group">
							<div class="input-group transparent">
								<span class="input-group-addon ">
									<i class="fa fa-search"></i>
								</span>
								<input type="text" class="form-control" placeholder="Cari Judul..." name="src">
							</div>
						</div>

						
						<div class="form-group">
							<label>Urutan</label>
							<select id="source" style="width:100%" name="orderby">
								<option value="asc">A - Z</option>
								<option value="desc">Z - A</option>
							</select>
						</div>
						<div class="form-group">
							<label>Limit / Page</label>
							<select id="source" style="width:100%" name="limit">
								<option value="10">10</option>
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="500">500</option>
							</select>
						</div>

					</p>
					<br />
					<button class="btn btn-block btn-primary cari-barang" type="button"><i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
		</div>
	</div>

	@endsection