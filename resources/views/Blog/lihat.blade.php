@extends('Master.Template')

@section('title')
Data Blog
@endsection

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title">
			<div class="row">
				<div class="col-sm-9 col-xs-9">
	             	<h4>Blog #{{ $item->id }} <b></b></h4>
	            </div>
	            <div class="col-sm-3 col-xs-3 text-right">
	              <div class="tools text-left">
	                <a href="javascript:;" class="collapse"></a> 
	              </div>
	            </div>
	            
	        </div>

		</div>
		<div class="grid-body">
			<div class="scroller" data-height="auto">
				<div class="row">
					<div class="col-sm-8">
						<h3>{{ $item->judul }}</h3>
						<p>
							<table class="table table-stripe">
								
								<tr>
									<td>Content</td>
									<td>:</td>
									<td><?php echo htmlspecialchars_decode(stripslashes($item->content)); ?></td>
								</tr>
								<tr>
									<td>Tanggal Buat</td>
									<td>:</td>
									<td>{{ Format::indoDate(date('y-m-d',strtotime($item->created_at))) }}</td>
								</tr>
							</table>
						</p>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection