@extends('Master.Template')


@section('title')
Banner
@endsection

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<div class="row">
				<div class="col-sm-9 col-xs-9">
					<h4>Data Banner</h4>
				</div>
				<div class="col-sm-3 col-xs-3 text-right">
					<div class="tools text-left">
						<a href="javascript:;" class="collapse"></a> 
					</div>
				</div>

			</div>
		</div>
		<div class="grid-body no-border">
			<form action="{{ url('banner/create') }}" method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row column-seperation">
					<div class="col-md-8 col-sm-8 col-xs-8">
						<div class="form-group">
							<div class="form-label">Gambar</div>
							<div class="control">
								<input type="file" name="gambar" required>
							</div>
						</div>
						<div class="form-group">
							<div class="form-label">Status</div>
							<select class="form-control" name="status">
								<option value="1"> Aktif </option>
								<option value="2"> Tidak Aktif </option>
							</select>
						</div>
						<div class="form-group">
							<div class="control">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection