@extends('Master.Template')

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<div class="row">
				<div class="col-sm-9 col-xs-9">
	              <h4>Tambah Warna</h4>
	            </div>
	            <div class="col-sm-3 col-xs-3 text-right">
	              <div class="tools text-left">
	                <a href="javascript:;" class="collapse"></a> 
	              </div>
	            </div>
	            
	          	</div>
		</div>
		<div class="grid-body no-border">
			<div class="row">
				<div class="col-sm-12">
					<form action="{{ url('warna/create') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Warna</label>
									<input type="text" name="nm_warna" class="form-control" required >
								</div>

							</div>
						</div>	
						<center>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Simpan</button>
								<a href="{{ url('warna') }}"><button class="btn btn-info" type="button">Kembali</button></a>
							</div>	
						</center>
						
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection