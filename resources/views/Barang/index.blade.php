@extends('Master.Template')

@section('meta')
<script type="text/javascript" src="{{ asset('/js/barang/barang.js') }}"></script>
<style type="text/css">
	.items:hover td .tbl-opsi{
		display: block !important;
	}
</style>
@endsection

@section('title')
Product
@endsection

@section('content')

<div class="row">
	<div class="col-sm-9">
		<div class="grid simple">
			<div class="grid-title no-border">
				
				<div class="row">
					<div class="col-sm-9 col-xs-9">
		              <h4>{{ $items->total() }} Barang <span class="semi-bold">ditemukan</span></h4>
		            </div>
		            <div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		                <a href="javascript:;" class="reload"></a> 
		              </div>
		            </div>
		            
		          </div>
			</div>
			<div class="grid-body no-border">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th><a href="javascript:;">Kode Barang</a></th>
								<th><a href="javascript:;">Nama Barang</a></th>
								<th><a href="javascript:;">Nama Artis</a></th>
								<th><a href="javascript:;">Type Barang</a></th>
								<th><a href="javascript:;">Stok</a></th>
								<th></th>
							</tr>
						</thead>
						<tbody class="contents-items">
							<?php $no = $items->currentPage() == 1 ? 1 : ($items->perPage() + $items->currentPage()) -1 ; ?>
							@forelse($items as $item)
							<tr class="item_{{ $item->id }} items">
								<td>{{ $no }}</td>
								<td>
								{{ $item->kd_barang }}
								<div style="display:none;" class="tbl-opsi">
										<small>[
											@if(Auth::user()->permission > 2)
											<!-- hanya admin yang memiliki akses Write dan Execute -->
											 <a href="{{ url('barang/edit/'.$item->id) }}">Edit</a> 
											@endif
											]</small>
										</div>
								</td>
								<td>
									<a href="javascript:;" title="{{ $item->nm_barang }}" data-toggle="tooltip" data-placement="bottom">{{ $item->nm_barang }}</a>
									
									</td>
									<td>{{ $item->nm_depan }} {{ $item->nm_belakang }}</td>
									<td>{{ $item->nama_type }}</td>
									<td>{{ $item->stok }}</td>
									<td class="text-right">
										@if(Auth::user()->permission > 2)
										<button type="button" class="close hapus" data-nama="{{ $item->nm_barang }}" data-id="{{ $item->id }}"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
										@endif
									</td>
								</tr>
								<?php $no++; ?>
								@empty

								@endforelse
							</tbody>
					</table>
				</div>
				<div class="text-right pagins">
						{!! $items->render() !!}
					</div>
			</div>
		</div>
		</div>
		<div class="col-sm-3">
			<div class="grid simple">
				<div class="grid-title no-border">
					
					<div class="row">
						
			            <div class="col-sm-12 col-xs-12 text-right">
			              <div class="tools text-left">
			                <a href="javascript:;" class="collapse"></a> 
			              </div>
			            </div>
			            
			          </div>
				</div>
				<div class="grid-body no-border">
					<p>
						<a class="btn btn-block btn-primary" href="{{ url('/barang/create') }}"><i class="fa fa-plus"></i> Tambah Barang</a>
					</p>
				</div>

			</div>
			<div class="grid simple">
				<div class="grid-title no-border">
					<div class="row">
						<div class="col-sm-9 col-xs-9">
			             <h4>Cari <span class="semi-bold">Barang</span></h4>
			            </div>
			            <div class="col-sm-3 col-xs-3 text-right">
			              <div class="tools text-left">
			                <a href="javascript:;" class="collapse"></a> 
			              </div>
			            </div>
			            
			         </div>
				</div>
				<div class="grid-body no-border">
					<p>
						<div class="form-group">
							<div class="input-group transparent">
								<span class="input-group-addon ">
									<i class="fa fa-search"></i>
								</span>
								<input type="text" class="form-control" placeholder="Cari Barang..." name="src">
							</div>
						</div>

					</p>
					<br />
					<button class="btn btn-block btn-primary cari-barang" type="button"><i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
		</div>
	</div>

	@endsection