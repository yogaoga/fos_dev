@extends('Master.Template')

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<h4>Data Barang</h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a> 
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body no-border">
			<div class="row">
				<div class="col-sm-12">
					<form action="{{ url('barang/edit') }}" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $data->id }}">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Kode Barang</label>
									<input type="text" name="kd_barang" class="form-control" required value="{{ $data->kd_barang }}">
								</div>
								<div class="form-group">
									<label>Nama Barang</label>
									<input type="text" name="nm_barang" class="form-control" required value="{{ $data->nm_barang }}">
								</div>
								<div class="form-group">
									<label>Artist</label>
									<select name="id_artist" class="form-control">
										<option value="">-Pilih Artis-</option>
										@foreach($artist as $row)
										<option value="{{ $row->id_karyawan }}" {{ $row->id_karyawan == $data->id_artist ? 'selected' : '' }}> {{ $row->nm_depan }} {{ $row->nm_belakang }} </option>
											
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label>Distributor</label>
									<input type="text" name="distributor" readonly="" class="form-control" value="{{ Me::data()->nm_depan }} {{ Me::data()->nm_belakang }}" >
									<input type="hidden" name="id_distributor" value="{{ Me::data()->id_karyawan }}">
								</div>
								<div class="form-group">
									<label>Type Barang</label>
									<select name="type_barang" class="form-control">
										<option value="">-Pilih Type-</option>
										@foreach($type as $tipe)
										<option value="{{ $tipe->id }}" {{ $tipe->id == $data->type_barang ? 'selected' : '' }}> {{ $tipe->nama_type }}</option>
										
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label>Warna</label>
									<select name="id_warna" class="form-control">
										<option value="">-Pilih Warna-</option>
										@foreach($warna as $dwarna)
										<option value="{{ $dwarna->id }}" {{ $dwarna->id == $data->id_warna ? 'selected' : '' }}> {{ $dwarna->nm_warna }}</option>
										
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<label>Size</label>
									<input type="text" name="size" class="form-control" placeholder="Size: 40,41,42" value="{{ $data->size }}">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Stok</label>
									<input type="text" name="stok" class="form-control" required value="{{ $data->stok }}">
								</div>
								<div class="form-group">
									<label>Harga</label>
									<input type="text" name="harga" class="form-control" value="{{ $data->harga }}">
								</div>
								<div class="form-group">
									<label>Diskon</label>
									<input type="text" name="discount" class="form-control" value="{{ $data->discount }}">
								</div>
								<div class="form-group">
									<label>Gambar</label>
									<input type="file" name="gambar">
								</div>
								
								<div class="form-group">
									<label>Deskripsi</label>
									<textarea name="deskripsi" class="form-control">{{ $data->deskripsi }}</textarea>
								</div>

							</div>
						</div>	
						<center>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Simpan</button>
								<a href="{{ url('barang') }}"><button class="btn btn-info" type="button">Kembali</button></a>
							</div>	
						</center>
						
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection