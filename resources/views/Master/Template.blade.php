<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<title>Full of stars</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/apple-touch-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/apple-touch-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/apple-touch-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/apple-touch-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/apple-touch-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/apple-touch-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/apple-touch-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/apple-touch-icon-152x152.png') }}" >
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon-180x180.png') }}" href="/">
	<link rel="icon" type="image/png" href="{{ asset('/favicon-32x32.png') }}"  sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('/favicon-194x194.png') }}"  sizes="194x194">
	<link rel="icon" type="image/png" href="{{ asset('/favicon-96x96.png') }}"   sizes="96x96">
	<link rel="icon" type="image/png" href="{{ asset('/android-chrome-192x192.png') }}"  sizes="192x192">
	<link rel="icon" type="image/png" href="{{ asset('/favicon-16x16.png') }}"   sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#fff">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- Penambahan -->
	@yield('csstop')

	<link href="{{ asset('/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen"/>
	<link href="{{ asset('/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
	<!-- BEGIN CORE CSS FRAMEWORK -->
	<link href="{{ asset('/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
	<!-- END CORE CSS FRAMEWORK -->

	<!-- BEGIN CSS TEMPLATE -->
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/css/update.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}" rel="stylesheet" type="text/css" />
	<!-- END CSS TEMPLATE -->

	<!-- BEGIN CORE JS FRAMEWORK--> 
	<script src="{{ asset('/plugins/jquery-1.8.3.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/breakpoints.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/jquery-block-ui/jqueryblockui.js') }}" type="text/javascript"></script> 
	<!-- END CORE JS FRAMEWORK --> 
	<!-- BEGIN PAGE LEVEL JS --> 	
	<script src="{{ asset('/plugins/jquery-slider/jquery.sidr.min.js') }}" type="text/javascript"></script> 	
	<script src="{{ asset('/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/pace/pace.min.js') }}" type="text/javascript"></script>  
	<script src="{{ asset('/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset ('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset ('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS --> 	

	<!-- BEGIN CORE TEMPLATE JS --> 
	<script src="{{ asset('/js/core.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/js/chat.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/js/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/jam.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/js/update.js') }}" type="text/javascript"></script> 
	<!-- END CORE TEMPLATE JS --> 
	
	<!-- Penambahan -->
	@yield('meta')

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="bg-blue">
	
	<!-- ALERT -->
	@if(Session::get('notif'))
	<div class="panel-alert">
		<div class="alert alert-{{ Session::get('notif')['label'] }}">
			<button type="button" class="close"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>
			<p>{!! Session::get('notif')['err'] !!}</p>
		</div>
	</div>
	@endif

	@if (count($errors) > 0)
	<div class="panel-alert">
		<div class="alert alert-danger">
			<button type="button" class="close"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif
	<!-- END ALERT -->

	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse "> 
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="header-seperation"> 
				<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
					<li class="dropdown"> 
						<a id="main-menu-toggle" href="#main-menu"  class="" > 
							<div class="iconset top-menu-toggle-white"></div> 
						</a> 
					</li>		 
				</ul>
				<!-- BEGIN LOGO -->	
				
				<!-- END LOGO --> 
				<ul class="nav pull-right notifcation-center">	
					<li id="header_task_bar"> 
						<a href="{{ url('/') }}" class="dropdown-toggle active" data-toggle="">
							<div class="glyphicon glyphicon-home"></div>
						</a> 
					</li>
					<li class="dropdown hidden-lg"> 
						<a data-toggle="dropdown" href="#" id="user-options">						
							<div class="glyphicon glyphicon-user"></div> 
						</a>
						<ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
							<li>
								<a href="{{ url('users/account') }}"> My Account</a>
							</li>
		                  	<li class="divider"></li>                
		                  	<li><a href="{{ url('/auth/logout') }}"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
		                </ul>
	          		</li> 

					<!-- <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li> -->
					@if(env('CHAT'))
					<li class="dropdown" id="portrait-chat-toggler" style="display:none"> 
						<a href="#sidr" class="chat-menu-toggle"> 
						<div class="iconset top-chat-white "></div> 
						</a> 
					</li>        
					@endif
				</ul>

			</div>
			<!-- END RESPONSIVE MENU TOGGLER --> 
			<div class="header-quick-nav" > 
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="pull-left"> 
				<ul class="nav quick-section">
					<li class="quicklinks"> <a href="javascript:;" class="" id="layout-condensed-toggle" >
						<div class="iconset top-menu-toggle-dark"></div>
					</a> </li>
				</ul>
				<ul class="nav quick-section">
			
			<li class="quicklinks"> 
          		<a href="" class="tmp-reload" title="Reload">
            		<i class="iconset top-reload"></i>
            	</a>
           	</li>
          	<li class="quicklinks"> <span class="h-seperate"></span></li>
          	<li class="quicklinks">
	          	<a href="javascript:;" class="tmp-fullscreen">
	            	<div class="iconset glyphicon glyphicon-resize-full" title="Full Screen"></div>
	        	</a>
        	</li>
        	@if(env('CHAT'))
			<li class="m-r-10 input-prepend inside search-form no-boarder">
				<span class="add-on"> <span class="iconset fa fa-smile-o"></span></span>
					<input name="mystatus" type="text"  class="no-boarder " placeholder="Katakan sesuatu..." maxlength="50" value="{{ Auth::user()->status_user }}" style="width:300px;">
				</li>
			</ul>
			@endif
		</div>
		<!-- END TOP NAVIGATION MENU -->
		<!-- BEGIN CHAT TOGGLER -->
		<div class="pull-right"> 
			<ul class="nav quick-section ">
				<li class="quicklinks"> 
					<a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">						
						<img src="{{ asset('/img/avatars/md/' . Auth::user()->avatar) }}"  alt="" data-src="{{ asset('/img/avatars/md/' . Auth::user()->avatar) }}" data-src-retina="{{ asset('/img/avatars/sm/' . Auth::user()->avatar) }}" width="25" height="25" /> 	
					</a>
					<ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
						<li>
							<a href="{{ url('users/account') }}"> My Account</a>
						</li>
	                  	<li class="divider"></li>                
	                  	<li><a href="{{ url('/auth/logout') }}"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
	                </ul>
          		</li> 
	      	</ul>
  		</div>
  <!-- END CHAT TOGGLER -->
</div> 
<!-- END TOP NAVIGATION MENU --> 

</div>
<!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar " id="main-menu"> 
		<!-- BEGIN MINI-PROFILE -->
		<div class="user-info-wrapper">	
			<div class="profile-wrapper">
				<a href="{{ url('/users/avatar') }}" title="Perbaharui Avatar">
					<img src="{{ asset('/img/avatars/md/' . Auth::user()->avatar) }}"  alt="" data-src="{{ asset('/img/avatars/md/' . Auth::user()->avatar) }}" data-src-retina="{{ asset('/img/avatars/sm/' . Auth::user()->avatar) }}" width="69" height="69" />
				</a>
			</div>
			<div class="user-info">
				<div class="greeting"><small>Welcome</small></div>
				<div class="username"><small>{{ Auth::user()->name }}</small></div>
				<div class="status">{{ Format::hari(date('Y-m-d')) }}, <span id="jam">Memuat...</span></div>
			</div>
		</div>
		<!-- END MINI-PROFILE -->

		<!-- BEGIN SIDEBAR MENU -->	
		<p class="menu-title">MAIN MENU 
			<span class="pull-right">
				<a href="{{ url('/lockscreen') }}">
					<i class="fa fa-power-off"></i>
				</a>
			</span>
		</p>

		{!! Menu::mainMenu() !!}
		<div class="clearfix"></div>
		<!-- END SIDEBAR MENU --> 
	</div>


	<!-- END SIDEBAR --> 
	<!-- BEGIN PAGE CONTAINER-->
	<div class="page-content"> 
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div id="portlet-config" class="modal hide">
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button"></button>
				<h3>Widget Settings</h3>
			</div>
			<div class="modal-body"> Widget settings form goes here </div>
		</div>
		<div class="clearfix"></div>
		<div class="content">  
			<div class="page-title">	
				<h3>@yield('title')</h3>		
			</div>

			@yield('content')

		</div>
	</div>

	<footer>
		@yield('footer')
	</footer>


</div>
<!-- END CONTAINER --> 
<!-- BEGIN CHAT --> 
@if(env('CHAT'))
@include('Chat.Master')
@endif
<!-- END CHAT --> 
<!-- END CONTAINER -->
<script type="text/javascript">
	var _base_url = '{{ url() }}';
</script>

@if(env('CHAT'))
<script type="text/javascript" src="{{ asset('/js/socket.io.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/io.js') }}"></script>
<script type="text/javascript">var __SOCKET_HOST = "{{ env('SOCKET_HOST') }}";</script>
@endif
</body>
</html>