<!doctype html>
<html class="home no-js" lang="">

<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta name="description" content="fullofstars.co.id,Full Of Stars, E-Commers, Jual, Baju, Celana, Sepatu, Aksesoris">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:url"           content="http://fullofstars.co.id/" />
  <meta property="og:type"          content="website" />  
  <meta property="og:title"         content="Full Of Stars" />
  <meta property="og:description"   content="Full Of Stars adalah situs interaktif non ritel berfokus pada pengembangan offline ritel saluran distribusi untuk produk selebriti di Indonesia." />
  <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />
  <!-- /meta -->

  <title>Full Of Stars  @yield('title')</title>

  <!-- page level plugin styles -->


  <link rel="stylesheet" href="{{ asset('/assets/plugins/owl-carousel/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/plugins/owl-carousel/owl.theme.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/plugins/owl-carousel/owl.transitions.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/plugins/magnific-popup/magnific-popup.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/plugins/bxslider/jquery.bxslider.css') }}">
  <link href="{{ asset('/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css" />

  <!-- /page level plugin styles -->

  <!-- core styles -->

  <link rel="stylesheet" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/themify-icons.css') }}">
  <link href="{{ asset('/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
  <!-- /core styles -->

  <!-- template styles -->
  <link rel="stylesheet" href="{{ asset('/assets/skin/blue.css') }}" id="skin">
  <link rel="stylesheet" href="{{ asset('/assets/main.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/app.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/fonts/opensans.css') }}" id="font">
  <script src="{{ asset('/assets/plugins/jquery-1.11.1.min.js') }}"></script>
  
  <script src="{{ asset ('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
  @yield('meta')
  <!-- template styles -->


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->

          <!-- load modernizer -->
          <script src="{{ asset('/assets/plugins/modernizr.js') }}"></script>
        </head>

        <!-- body -->

        <body>


          <div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>

          <!-- menu navigation -->
          <header class="header fixed-sticky">
           <div class="container">
             <nav class="heading-font">

               <!-- branding -->
               <div class="branding" >
                <!-- toggle menu  -->
                <button type="button" class="mobile-toggle">
                  <span class="ti-more-alt"></span>
                </button>
                <!-- /toggle menu -->

                <div class="logo-r">
                  <a href="{{ url('/') }}" class="logo transition" >
                    <img src="{{ asset('/assets/img/fos.png') }}" width="120px">
                  </a>
                </div>
              </div>
              <!-- /branding -->

              <!-- links -->
              <div class="navigation spy">
               <ul class="nav">
                 <li>
                   <a href="{{ url('/index') }}" class="transition">Home</a>
                 </li>
                 <li>
                   <a href="{{ url('/artist') }}" class="transition">Artist</a>
                 </li>
                 <li>
                   <a href="{{ url('/agen') }}" class="transition">Mitra</a>
                 </li>
                 <li>
                   <a href="{{ url('/product') }}" class="transition">Product</a>
                 </li>

                 <li>
                   <a href="{{ url('/about') }}" class="transition">About</a>
                 </li>
                 @if(Auth::check())

                 <li class="dropdown show-on-hover ">
                   <a href="javascript:;" class="ignore" data-toggle="dropdown"><span class="color-acount"> <i class="fa fa-user"> {{ Auth::user()->username }}</i></span></a>
                   <ul class="dropdown-menu">
                     @if(Me::data()->jabatan != 6)

                     <li>
                       <a href="{{ url('/home') }}" ><i class="fa fa-user mr5"></i> Profil Saya</a>
                     </li>
                     <li>
                       <a href="{{ url('/home') }}" class="transition"><i class="fa fa-cog mr5"></i>Admin</a>
                     </li>
                     @else

                     @endif
                      <li>
                       <a href="{{ url('/auth/logout') }}" class="transition"><i class="fa fa-sign-out  mr5"></i>Keluar</a>
                     </li>
                   </ul>
                 </li>

                 @else
                 <li>
                   <a href="{{ url('/auth/login') }}" class=""><span class="color-acount"> Masuk</span></a>
                 </li>
                 @endif

                 <li>
                   <a href="{{ url('/keranjangList') }}" class=""><span class="color-acount"><i class="fa fa-shopping-cart mr5"></i> cart </span></a>
                 </li>

               </ul>


             </div>
             <!-- /links -->

           </nav>
         </div>

       </header>
       <!-- /menu navigation -->
       @if(Session::get('notif'))
       <div class="panel-alert">
        <div class="alert alert-{{ Session::get('notif')['label'] }}">
          <button type="button" class="close"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>
          <p>{!! Session::get('notif')['err'] !!}</p>
        </div>
      </div>
      @endif


      @yield('content')

      <!-- footer -->
      <footer class="content-section  banner  bg-color">

        <div class="container">

          <div class="row text-center mt25">

            <div class="col-sm-12">
              <h3 class="text-white mb25"><strong>STAY CONNECT WITH US</strong></h3>

              <div class="row text-center">

                <div class="col-sm-12 mb25">

                  <a class="btn btn-social-icon btn-white btn-rounded btn-outline   btn-md ml5 mr5" href="https://www.facebook.com/fullofstars.co.id/?ref=hl" style="font-size:14px" target="_blank">
                    <i class="fa fa-facebook"></i>
                  </a>

                  <a class="btn btn-social-icon btn-white btn-rounded btn-outline  btn-md ml5 mr5" href="https://twitter.com/fullofstars_ID" style="font-size:14px" target="_balnk">
                    <i class="fa fa-twitter"></i>
                  </a>

                  <a class="btn btn-social-icon btn-white btn-rounded btn-outline   btn-md ml5 mr5" href="https://www.instagram.com/fullofstars_ID/" style="font-size:14px" target="_blank">
                    <i class="fa fa-instagram"></i>
                  </a>

                  <a class="btn btn-social-icon btn-white btn-rounded btn-outline   btn-md ml5 mr5" href="https://www.youtube.com/channel/UCGnmMrQHRit8vjYw9wGsztQ" style="font-size:14px" target="_blank">
                    <i class="fa fa-youtube"></i>
                  </a>

                </div>
              </div> 

              <div class="row">
                <div class="col-sm-12 ">
                  <div class=" text-white mb10">
                    <a href="{{ url('/partnership') }}" class="ml15 mr15">MITRA</a>
                    <a href="{{ url('/reseller') }}"  class="mr15">RESELLER</a>
                    <a href="{{ url('/press') }}"  class="mr15">  PRESS</a>
                    <a href="{{ url('/blog') }}" class="mr15">NEWS</a>
                    <a href="{{ url('/promo') }}"  class="mr15">  PROMO</a>
                    <a href="{{ url('/privasi') }}"  class="mr15">KEBIJAKAN PRIVASI</a>
                    <a href="{{ url('/kontak') }}"  class="mr15">  KONTAK KAMI </a>
                    <a href="{{ url('/syarat') }}"  class="mr15">SYARAT & KETENTUAN</a>
                  </div>
                </div>
              </div>

              
              <div class="row">
                <div class="col-sm-12 mb25 ">

                  <small class="show text-white">&copy;&nbsp;Copyright Full Of Stars - iBackstage <span class="year"></span>.</small>

                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- /footer -->

      <!-- core scripts -->

      <script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script> 
      <script src="{{ asset('/assets/plugins/appear/jquery.appear.js') }}"></script>
      <script src="{{ asset('/assets/plugins/nav/jquery.nav.js') }}"></script>
      <script src="{{ asset('/assets/plugins/jquery.easing.min.js') }}"></script>
      <!-- /core scripts -->

      <!-- page level scripts -->
      <script src="{{ asset('/assets/plugins/jquery.parallax.js') }}"></script>
      <script src="{{ asset('/assets/plugins/isotope/isotope.pkgd.min.js') }}"></script>
      <!--    <script src="{{ asset('/assets/plugins/count-to/jquery.countTo.js') }}"></script> -->
      <script src="{{ asset('/assets/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
      <script src="{{ asset('/assets/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
      <script src="{{ asset('/assets/plugins/bxslider/jquery.bxslider.min.js') }}"></script>
      <script src="{{ asset('/assets/plugins/fitvids/jquery.fitvids.js') }}"></script>
      <!-- /page level scripts -->

      <!-- template scripts -->
      <script src="{{ asset('/assets/js/main.js') }}"></script>
      <script src="{{ asset('/assets/js/app.js') }}"></script>

      <!-- /template scripts -->

      <!-- page script -->
      <script src="{{ asset('/assets/js/blog.js') }}"></script>
      <script src="{{ asset('/assets/js/subscribe.js') }}"></script>
      <script src="{{ asset('/assets/js/demo_home.js') }}"></script>
      <!-- /page script -->

      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      <script type="text/javascript">
        var _base_url = '{{ url() }}';
      </script>

    </body>

    </html>


