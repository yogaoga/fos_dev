@extends('Master.Template')

@section('meta')
	<!-- BEGIN PAGE LEVEL JS -->
	<script src="{{ asset('/plugins/raphael/raphael-min.js') }}"></script>
	<script src="{{ asset('/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/jquery-slider/jquery.sidr.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script> 
	<script src="{{ asset('/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-morris-chart/js/morris.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-easy-pie-chart/js/jquery.easypiechart.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-flot/jquery.flot.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-flot/jquery.flot.time.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-flot/jquery.flot.selection.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-flot/jquery.flot.animator.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-flot/jquery.flot.orderBars.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-sparkline/jquery-sparkline.js') }}"></script>
	<script src="{{ asset('/plugins/jquery-easy-pie-chart/js/jquery.easypiechart.min.js') }}"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<script src="{{ asset('/js/charts.js') }}" type="text/javascript"></script>

	<link href="{{ asset('/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen"/>
	<link href="{{ asset('/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="{{ asset('/plugins/jquery-ricksaw-chart/css/rickshaw.css') }}" type="text/css" media="screen">
	<link rel="stylesheet" href="{{ asset('/plugins/jquery-morris-chart/css/morris.css') }}" type="text/css" media="screen">
@endsection

@section('title')
	Dashboard  
@endsection

@section('content')
	<div class="row bg-white">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <div class="mini-chart-wrapper">
            <div class="chart-details-wrapper">
              <div class="chartname">Produk Interest</div>
              <div class="chart-value"> 17,555 </div>
            </div>
            <div class="mini-chart">
              <div id="mini-chart-orders"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="grid simple">
        <div class="grid-title  no-border">
          <div class="row">
            <div class="col-sm-2 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-7 col-xs-6 text-center">
              <h4>Graphic <span class="semi-bold">Interest</span></h4>
            </div>
            <div class="col-sm-3 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          
          <div id="line-example1"> </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-6">
          <div class="mini-chart-wrapper">
            <div class="chart-details-wrapper">
              <div class="chartname"> Visitor today </div>
              <div class="chart-value"> 17,000K</div>
            </div>
          </div>
        </div>
      </div>
      <div class="grid simple">
        <div class="grid-title no-border">
          <div class="row">
            <div class="col-sm-3 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-6 col-xs-6 text-center">
              <h4>Grafik <span class="semi-bold">Visitor</span></h4>
            </div>
            <div class="col-sm-3 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          <div id="line-example"> </div>
        </div>
      </div> 
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="grid simple">
        <div class="grid-title no-border">
          <div class="row">
            <div class="col-sm-2 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-8 col-xs-6 text-center">
              <h4>Graphic<span class="semi-bold"> Barang Agent</span></h4>
            </div>
            <div class="col-sm-2 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border"> 
            <div id="placeholder-bar-chart" style="height:200px"></div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="grid simple">
        <div class="grid-title no-border">
          <div class="row">
            <div class="col-sm-2 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-8 col-xs-6 text-center">
              <h4>Graphic <span class="semi-bold">Agent Visitor</span></h4>
            </div>
            <div class="col-sm-2 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          <div class="row">
          <div class="col-sm-11 col-sm-offset-1">
          <div id="pie-distributor"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="grid simple">
        <div class="grid-title no-border">
           <div class="row">
            <div class="col-sm-3 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-6 col-xs-6 text-center">
              <h4>Rate <span class="semi-bold"> Produk Agent</span></h4>
            </div>
            <div class="col-sm-3 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          <div class="row">
            <div class="col-sm-12">
              <table class="table table-hover"> 
                <thead> 
                  <tr> 
                    <th></th> 
                    <th>Agent</th> 
                    <th>Status</th> 
                    <th>Total</th> 
                  </tr> 
                </thead> 
                <tbody> 
                  <tr> 
                    <th scope="row">1</th> 
                    <td>Mark</td> 
                    <td>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                          60%
                        </div>
                      </div>
                    </td> 
                    <td>20</td> 
                  </tr> 

                  <tr> 
                    <th scope="row">2</th> 
                    <td>Jacob</td> 
                    <td>
                      <div class="progress">
                        <div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                          40%
                        </div>
                      </div>
                    </td> 
                    <td>19</td> 
                  </tr> 
                  <tr> 
                    <th scope="row">3</th> 
                    <td>Larry</td> 
                    <td>
                      <div class="progress">
                        <div class="progress-bar progress-bar-danger"  role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                          10%
                        </div>
                      </div>
                    </td>  
                    <td>15</td> 
                  </tr> 
                </tbody> 
              </table>
            </div>
          </div>          
        </div>
      </div>
    </div> 
  </div>

  <div class="row">

    <div class="col-md-4">
      <div class="grid simple">
        <div class="grid-title no-border">
          <div class="row">
            <div class="col-sm-3 col-xs-3">
              <div class="tools"> 
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a>
              </div> 
            </div>
            <div class="col-sm-6 col-xs-6 text-center">
              <h4>Graphic <span class="semi-bold"> Rate Produk</span></h4>
            </div>
            <div class="col-sm-3 col-xs-3 text-right">
              <div class="tools" > 
               
                <a href="#grid-config" data-toggle="modal" class="config"></a> 

                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          <div class="row">
            <div class="col-md-12">
              <h4>Grafik Transaksi <span class="semi-bold"> Purchase Order</span></h4>
              <div id="area-example" style="height:200px"> </div>
            </div> 
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="grid simple">
        <div class="grid-title no-border">
           <div class="row">
            <div class="col-sm-3 col-xs-3">
              <div class="tools text-left">
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a> 
              </div>
            </div>
            <div class="col-sm-6 col-xs-6 ">
              <h4>Download &<span class="semi-bold"> Mail</span></h4>
            </div>
            <div class="col-sm-3 col-xs-3 text-right">
              <div class="tools">
                <a href="#grid-config" data-toggle="modal" class="config"></a>
                
                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="grid-body no-border">
          <div id="donut-example" style="height:200px;"> </div>
        </div>          
      </div>
    </div>
    
    
    <!--
    <div class="col-md-4">
      <div class="grid simple">
        <div class="grid-title no-border">
          <div class="row">
            <div class="col-sm-3">
              <div class="tools"> 
                <a href="javascript:;" class="collapse"></a> 
                <a href="javascript:;" class="reload"></a>
              </div> 
            </div>
            <div class="col-sm-6 text-center">
              <h4>Morris <span class="semi-bold">Charts</span></h4>
            </div>
            <div class="col-sm-3 text-right">
              <div class="tools"> 
               
                <a href="#grid-config" data-toggle="modal" class="config"></a> 

                <a href="javascript:;" class="remove"></a> 
              </div>
            </div>
          </div>
        </div>

        <div class="grid-body no-border">
            <h4>Grafik Jenis <span class="semi-bold"> Kelamin Pasien</span></h4>
  			    <div class="pull-left">
  			      <p>Pria</p>
              <div id="ram-usage" class="easy-pie-custom" data-percent="85"><span class="easy-pie-percent">85</span></div>
            </div>
            <div class="pull-right">
		          <p>Wanita</p>
              <div id="disk-usage" class="easy-pie-custom" data-percent="73"><span class="easy-pie-percent">73</span></div>
            </div>
		        <div class="clearfix"></div>
        </div>
      </div>
    </div> -->

    <div class="col-md-4 ">
      <div class="tiles white no-margin">
        <div class="tiles-body">
          <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
          <div class="tiles-title"> SERVER LOAD </div>
          <div class="heading text-black "> 250 GB </div>
          <div class="progress  progress-small no-radius progress-success">
            <div class="bar animate-progress-bar" data-percentage="25%" ></div>
          </div>
          <div class="description"> <span class="mini-description"><span class="text-black">250GB</span> of <span class="text-black">1,024GB</span> used</span> </div>
        </div>
      </div>
      <div class="tiles white no-margin">
        <div id="updatingChart"> </div>
      </div>
    </div>

  </div>


@endsection