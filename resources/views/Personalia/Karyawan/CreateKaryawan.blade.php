@extends('Master.Template')

@section('meta')
<script src="{{ asset ('/js/tabs_accordian.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/plugins/bootstrap-select2/select2.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#tgl_bergabung').datepicker();
		$('#tgl_lahir').datepicker();
	});
</script>
@endsection

@section('title')
Tambah Agent	
@endsection
@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title no-border">
			<div class="grid-title no-border">
				<div class="row">
					<div class="col-sm-9 col-xs-9">
		              <h4>Silahkan Isi Data</h4>
		            </div>
		            <div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		                <a href="javascript:;" class="reload"></a> 
		              </div>
		            </div>
		            
		         </div>
			</div>
		</div>
		<div class="grid-body no-border">
			<div class="row">
				<div class="col-sm-12">
					<form action="{{ url('petugas/add') }}" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row column-seperation">
							<div class="col-md-6">
								<div class="form-group">
									<div class="form-label">Nama Depan</div>
									<span class="help">e.g. John</span>
									<div class="control">
										<input type="text" required name="nm_depan" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<div class="form-label">Nama Belakang</div>
									<span class="help">e.g. Doe</span>
									<div class="control">
										<input type="text" required name="nm_belakang" class="form-control">
									</div>
								</div>
								
								<div class="form-group">
									<div class="form-label">Email</div>
									<span class="help">e.g. jhondoe@example.com</span>
									<div class="control">
										<input type="email" class="form-control" name="email">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Username</div>
									<div class="control">
										<input type="text" class="form-control" name="username">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Password</div>
									<div class="control">
										<input type="password" class="form-control" name="password">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Jenis Kelamin</div>
									<div class="radio">
										<input id="male" type="radio" name="gender" value="1" checked="checked">
										<label for="male">Laki-Laki</label>
										<input id="female" type="radio" name="gender" value="2">
										<label for="female">Perempuan</label>
									</div>
								</div>
								<div class="form-group">
									<div class="form-label">Ketegori</div>
									<div class="control">
										<select class="form-control" id="source" name="jabatan">
											<option value="">-Pilih-</option>
											@foreach($ref_jabatan as $row)
											<option value="{{ $row->id }}"> {{$row->nm_jabatan}} </option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="form-label">HandPhone</div>

									<div class="control">
										<input type="text" class="form-control" name="hp">
									</div>
								</div>
								<div class="form-group">
									<div class="form-label">Tempat Lahir</div>
									<span class="help">e.g. Bandung</span>
									<div class="control">
										<input type="text" class="form-control" name="tempat_lahir">
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								
								<div class="form-group">
									<div class="form-label">Tanggal Lahir</div>
									<div class="control">
										<div class="input-append success date col-md-10 col-lg-6 no-padding">
											<input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" data-provide="datepicker" value="{{date('m/d/Y')}}">
											<span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span> 
										</div>
									</div>
								</div>
								<br><br>
								
								<div class="form-group">
									<div class="form-label">Alamat</div>
									<span class="help">e.g. Sesuai KTP</span>
									<div class="control">
										<textarea name="alamat" class="form-control"></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Photo</div>

									<div class="control">
										<input type="file" required name="foto">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Deskripsi</div>

									<div class="control">
										<textarea name="deskripsi" class="form-control"></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Facebook</div>
									<div class="control">
										<input type="text" class="form-control" name="fb">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Twitter</div>
									<div class="control">
										<input type="text" class="form-control" name="twitter">
									</div>
								</div>

								<div class="form-group">
									<div class="form-label">Instagram</div>
									<div class="control">
										<input type="text" class="form-control" name="ig">
									</div>
								</div>
								
								<div class="form-group">
									<div class="control">
										<button class="btn btn-primary" type="submit"> Simpan</button>	
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection