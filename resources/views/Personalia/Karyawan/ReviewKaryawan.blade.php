@extends('Master.Template')

@section('title')
Agent Overview
@endsection

@section('content')
<div class="col-md-12">
	<div class="grid simple">
		<div class="grid-title">
			<div class="grid-title no-border">
				<div class="row">
					<div class="col-sm-9 col-xs-9">
		              <h4>Data Personal <b>{{ $karyawan->nm_depan }}</b></h4>
		            </div>
		            <div class="col-sm-3 col-xs-3 text-right">
		              <div class="tools text-left">
		                <a href="javascript:;" class="collapse"></a> 
		                <a href="javascript:;" class="reload"></a> 
		              </div>
		            </div>
		            
		         </div>
			</div>
		</div>
		<div class="grid-body">
			<div class="scroller" data-height="auto">
				<div class="row">
					<div class="col-sm-3">
					@if($karyawan->foto == null)
						<img src="http://placehold.it/200x200" />
					@else
						<img src="{{ asset('/assets/img/client/' . $karyawan->foto) }}" width="200" height="200" />
					@endif
					</div>
					<div class="col-sm-8">
						<h1>{{ $karyawan->nm_depan }} {{ $karyawan->nm_belakang }}</h1>
						<p> <font size="4"> 
						@if(count($jabatan) > 0)
						{{ $jabatan->nm_jabatan }}
						@else
						<i>Anda belum memasukan <b>Jabatan</b></i>
						@endif
						</font> 
					</p>
					<hr/>
					<h3 class="text-success">Informasi</h3>
					<p>
						{{ $karyawan->alamat }}<br>
						{{ $karyawan->telp }} / {{ $karyawan->hp }} 
					</p>
					<p>
						{{ $karyawan->email }}	
					</p>
					<p>	</p>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection