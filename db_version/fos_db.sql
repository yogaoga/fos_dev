-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 01, 2015 at 11:03 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_aktivitas`
--

CREATE TABLE `data_aktivitas` (
  `id_aktivitas` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `keterangan` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_aktivitas`
--

INSERT INTO `data_aktivitas` (`id_aktivitas`, `id_karyawan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Logout', '2015-11-30 09:24:50', '2015-11-30 09:24:50'),
(2, 1, 'Login', '2015-11-30 09:26:58', '2015-11-30 09:26:58'),
(3, 1, 'Logout', '2015-11-30 09:27:07', '2015-11-30 09:27:07'),
(4, 1, 'Login', '2015-12-01 09:51:44', '2015-12-01 09:51:44');

-- --------------------------------------------------------

--
-- Table structure for table `data_barang`
--

CREATE TABLE `data_barang` (
  `id` int(11) NOT NULL,
  `nm_barang` varchar(100) NOT NULL,
  `id_artist` int(11) NOT NULL,
  `id_distributor` int(11) NOT NULL,
  `type_barang` int(11) DEFAULT NULL,
  `stok` int(11) NOT NULL,
  `harga` float NOT NULL,
  `discount` float DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_distributor`
--

CREATE TABLE `data_distributor` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_karyawan`
--

CREATE TABLE `data_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `NIK` varchar(100) NOT NULL,
  `nm_depan` varchar(20) DEFAULT NULL,
  `nm_belakang` varchar(20) DEFAULT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `email` varchar(20) NOT NULL,
  `sex` varchar(5) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `jabatan` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `agama` int(11) NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `id_status` int(11) NOT NULL,
  `tgl_bergabung` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_departemen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_karyawan`
--

INSERT INTO `data_karyawan` (`id_karyawan`, `NIK`, `nm_depan`, `nm_belakang`, `telp`, `email`, `sex`, `hp`, `tempat_lahir`, `tgl_lahir`, `foto`, `jabatan`, `alamat`, `agama`, `pendidikan`, `id_status`, `tgl_bergabung`, `created_at`, `updated_at`, `id_departemen`) VALUES
(1, 'RSOS001', 'Jhane', 'Doe', '0842342342', 'jhane@example.com', '1', '0812366565', 'bali', '2015-07-30', '', 1, 'bandung', 1, 'S1', 1, '2015-01-06', '2015-07-09 21:19:42', '2015-09-07 04:37:31', 4),
(2, 'RSOS002', 'Jhone', 'Doe', '0842342342', 'jhone@example.com', '1', '0812366565', 'jawa', '2015-07-30', '', 2, 'lumajang', 1, 'S1', 1, '2000-01-01', '2015-07-09 21:19:42', '2015-10-29 04:46:01', 4);

-- --------------------------------------------------------

--
-- Table structure for table `data_level`
--

CREATE TABLE `data_level` (
  `id_level` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_level_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_level`
--

INSERT INTO `data_level` (`id_level`, `id_user`, `id_level_user`, `created_at`, `updated_at`) VALUES
(12, 1, 1, '2015-07-10 01:27:56', '2015-07-10 01:27:56'),
(18, 7, 2, '2015-07-10 22:13:49', '2015-07-10 22:13:49'),
(21, 9, 1, '2015-07-28 14:11:13', '2015-07-28 14:11:13'),
(22, 9, 2, '2015-07-28 14:11:13', '2015-07-28 14:11:13'),
(24, 11, 3, '2015-10-19 03:16:11', '2015-10-19 03:16:11'),
(25, 12, 2, '2015-10-26 06:09:06', '2015-10-26 06:09:06'),
(28, 13, 1, '2015-11-10 17:46:55', '2015-11-10 17:46:55'),
(29, 10, 1, '2015-11-10 17:47:28', '2015-11-10 17:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `data_level_user`
--

CREATE TABLE `data_level_user` (
  `id_level_user` int(11) NOT NULL,
  `nm_level` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_level_user`
--

INSERT INTO `data_level_user` (`id_level_user`, `nm_level`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 1, '2015-07-09 10:08:12', '2015-07-09 10:08:12'),
(2, 'Admin', 1, '2015-07-09 10:08:12', '2015-07-09 10:08:12'),
(3, 'Operator', 1, '2015-09-08 04:17:57', '2015-09-08 04:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `data_menu`
--

CREATE TABLE `data_menu` (
  `id_menu` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seri` int(255) NOT NULL DEFAULT '0',
  `ket` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_menu`
--

INSERT INTO `data_menu` (`id_menu`, `parent_id`, `title`, `slug`, `class`, `class_id`, `seri`, `ket`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Settings', 'settings', 'fa fa-cogs', '', 1, '', 1, '2015-07-09 06:10:43', '2015-09-30 04:24:21'),
(2, 1, 'Menus', 'menu/add', '', '2', 1, '', 1, '2015-07-09 06:36:48', '2015-09-25 06:04:35'),
(6, 0, 'Dashboard', '', 'fa fa-dashboard', '', 2, '', 1, '2015-07-09 02:36:50', '2015-10-02 04:18:33'),
(7, 1, 'Menu Akses', 'menu/access', '', '', 2, '', 1, '2015-07-09 02:39:01', '2015-09-25 06:04:35'),
(10, 1, 'Users', 'users', '', '', 3, '', 1, '2015-07-09 20:01:33', '2015-08-25 07:51:28'),
(15, 13, 'Master Pegawai', 'karyawan', '', '', 1, '', 1, '2015-07-27 18:52:34', '2015-09-06 11:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `data_menu_user`
--

CREATE TABLE `data_menu_user` (
  `id_menu_user` int(10) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_menu_user`
--

INSERT INTO `data_menu_user` (`id_menu_user`, `id_menu`, `id_level`, `created_at`, `updated_at`) VALUES
(6, 1, 1, '2015-11-30 09:24:32', '2015-11-30 09:24:32'),
(7, 2, 1, '2015-11-30 09:24:32', '2015-11-30 09:24:32'),
(8, 7, 1, '2015-11-30 09:24:32', '2015-11-30 09:24:32'),
(9, 10, 1, '2015-11-30 09:24:32', '2015-11-30 09:24:32'),
(10, 6, 1, '2015-11-30 09:24:32', '2015-11-30 09:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `data_reseller`
--

CREATE TABLE `data_reseller` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `id_distriutot` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ref_kab_kota`
--

CREATE TABLE `ref_kab_kota` (
  `id_kab_kota` int(10) NOT NULL,
  `nm_kab_kota` varchar(100) DEFAULT NULL,
  `id_provinsi` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `id_ekspedisi` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_kab_kota`
--

INSERT INTO `ref_kab_kota` (`id_kab_kota`, `nm_kab_kota`, `id_provinsi`, `status`, `id_ekspedisi`) VALUES
(1, 'Aceh Barat', 1, 1, 1),
(2, 'Aceh Barat Daya', 1, 1, 2),
(3, 'Aceh Besar', 1, 1, 3),
(4, 'Aceh Jaya', 1, 1, 4),
(5, 'Aceh Selatan', 1, 1, 5),
(6, 'Aceh Singkil', 1, 1, 6),
(7, 'Aceh Tamiang', 1, 1, 7),
(8, 'Aceh Tengah', 1, 1, 8),
(9, 'Aceh Tenggara', 1, 1, 9),
(10, 'Aceh Timur', 1, 1, 10),
(11, 'Aceh Utara', 1, 1, 11),
(12, 'Bener Meriah', 1, 1, 59),
(13, 'Bireuen', 1, 1, 72),
(14, 'Gayo Lues', 1, 1, 127),
(15, 'Nagan Raya', 1, 1, 300),
(16, 'Pidie', 1, 1, 358),
(17, 'Pidie Jaya', 1, 1, 359),
(18, 'Simeulue', 1, 1, 414),
(19, 'Banda Aceh', 1, 1, 20),
(20, 'Langsa', 1, 1, 230),
(21, 'Lhokseumawe', 1, 1, 235),
(22, 'Sabang', 1, 1, 384),
(23, 'Subulussalam', 1, 1, 429),
(24, 'Asahan', 2, 1, 15),
(25, 'Batu bara', 2, 1, 52),
(26, 'Dairi', 2, 1, 110),
(27, 'Deli Serdang', 2, 1, 112),
(28, 'Humbang Hasundutan', 2, 1, 146),
(29, 'Karo', 2, 1, 173),
(30, 'Labuhan Batu', 2, 1, 217),
(31, 'Labuhan Batu Selatan', 2, 1, 218),
(32, 'Labuhan Batu Utara', 2, 1, 219),
(33, 'Langkat', 2, 1, 229),
(34, 'Mandailing Natal', 2, 1, 268),
(35, 'Nias', 2, 1, 307),
(36, 'Nias Barat', 2, 1, 308),
(37, 'Nias Selatan', 2, 1, 309),
(38, 'Nias Utara', 2, 1, 310),
(39, 'Padang Lawas', 2, 1, 319),
(40, 'Padang Lawas Utara', 2, 1, 320),
(41, 'Pakpak Bharat', 2, 1, 325),
(42, 'Samosir', 2, 1, 389),
(43, 'Serdang Bedagai', 2, 1, 404),
(44, 'Simalungun', 2, 1, 413),
(45, 'Tapanuli Selatan', 2, 1, 463),
(46, 'Tapanuli Tengah', 2, 1, 464),
(47, 'Tapanuli Utara', 2, 1, 465),
(48, 'Toba Samosir', 2, 1, 481),
(49, 'Binjai', 2, 1, 70),
(50, 'Gunungsitoli', 2, 1, 137),
(51, 'Medan', 2, 1, 278),
(52, 'Padang Sidempuan', 2, 1, 323),
(53, 'Pematang Siantar', 2, 1, 353),
(54, 'Sibolga', 2, 1, 407),
(55, 'Tanjung Balai', 2, 1, 459),
(56, 'Tebing Tinggi', 2, 1, 470),
(57, 'Agam', 3, 1, 12),
(58, 'Dharmasraya', 3, 1, 116),
(59, 'Kepulauan Mentawai', 3, 1, 186),
(60, 'Lima Puluh Koto/Kota', 3, 1, 236),
(61, 'Padang Pariaman', 3, 1, 322),
(62, 'Pasaman', 3, 1, 339),
(63, 'Pasaman Barat', 3, 1, 340),
(64, 'Pesisir Selatan', 3, 1, 357),
(65, 'Sijunjung (Sawah Lunto Sijunjung)', 3, 1, 411),
(66, 'Solok', 3, 1, 420),
(67, 'Solok Selatan', 3, 1, 422),
(68, 'Tanah Datar', 3, 1, 453),
(69, 'Bukittinggi', 3, 1, 93),
(70, 'Padang', 3, 1, 318),
(71, 'Padang Panjang', 3, 1, 321),
(72, 'Pariaman', 3, 1, 337),
(73, 'Payakumbuh', 3, 1, 345),
(74, 'Sawah Lunto', 3, 1, 394),
(75, 'Solok', 3, 1, 420),
(76, 'Bengkalis', 4, 1, 60),
(77, 'Indragiri Hilir', 4, 1, 147),
(78, 'Indragiri Hulu', 4, 1, 148),
(79, 'Kampar', 4, 1, 166),
(80, 'Kuantan Singingi', 4, 1, 207),
(81, 'Kab. Meranti', 4, 0, 187),
(82, 'Pelalawan', 4, 1, 351),
(83, 'Rokan Hilir', 4, 1, 381),
(84, 'Rokan Hulu', 4, 1, 382),
(85, 'Siak', 4, 1, 406),
(86, 'Dumai', 4, 1, 120),
(87, 'Pekanbaru', 4, 1, 350),
(88, 'Bintan', 5, 1, 71),
(89, 'Karimun', 5, 1, 172),
(90, 'Kepulauan Anambas', 5, 1, 184),
(91, 'Lingga', 5, 1, 237),
(92, 'Natuna', 5, 1, 302),
(93, 'Batam', 5, 1, 48),
(94, 'Tanjung Pinang', 5, 1, 462),
(95, 'Bangka', 6, 1, 27),
(96, 'Bangka Barat', 6, 1, 28),
(97, 'Bangka Selatan', 6, 1, 29),
(98, 'Bangka Tengah', 6, 1, 30),
(99, 'Belitung', 6, 1, 56),
(100, 'Belitung Timur', 6, 1, 57),
(101, 'Pangkal Pinang', 6, 1, 334),
(102, 'Kerinci', 7, 1, 194),
(103, 'Merangin', 7, 1, 280),
(104, 'Sarolangun', 7, 1, 393),
(105, 'Batang Hari', 7, 1, 50),
(106, 'Muaro Jambi', 7, 1, 293),
(107, 'Tanjung Jabung Timur', 7, 1, 461),
(108, 'Tanjung Jabung Barat', 7, 1, 460),
(109, 'Tebo', 7, 1, 471),
(110, 'Bungo', 7, 1, 97),
(111, 'Jambi', 7, 1, 156),
(112, 'Sungaipenuh', 7, 1, 442),
(113, 'Bengkulu Selatan', 8, 1, 63),
(114, 'Bengkulu Tengah', 8, 1, 64),
(115, 'Bengkulu Utara', 8, 1, 65),
(116, 'Kaur', 8, 1, 175),
(117, 'Kepahiang', 8, 1, 183),
(118, 'Lebong', 8, 1, 233),
(119, 'Muko Muko', 8, 1, 294),
(120, 'Rejang Lebong', 8, 1, 379),
(121, 'Seluma', 8, 1, 397),
(122, 'Bengkulu', 8, 1, 62),
(123, 'Banyuasin', 9, 1, 40),
(124, 'Empat Lawang', 9, 1, 121),
(125, 'Lahat', 9, 1, 220),
(126, 'Muara Enim', 9, 1, 292),
(127, 'Musi Banyuasin', 9, 1, 297),
(128, 'Musi Rawas', 9, 1, 298),
(129, 'Ogan Ilir', 9, 1, 312),
(130, 'Ogan Komering Ilir', 9, 1, 313),
(131, 'Ogan Komering Ulu', 9, 1, 314),
(132, 'Ogan Komering Ulu Selatan', 9, 1, 315),
(133, 'Ogan Komering Ulu Timur', 9, 1, 316),
(134, 'Lubuk Linggau', 9, 1, 242),
(135, 'Pagar Alam', 9, 1, 324),
(136, 'Palembang', 9, 1, 327),
(137, 'Prabumulih', 9, 1, 367),
(138, 'Lampung Barat', 10, 1, 223),
(139, 'Lampung Selatan', 10, 1, 224),
(140, 'Lampung Tengah', 10, 1, 225),
(141, 'Lampung Timur', 10, 1, 226),
(142, 'Lampung Utara', 10, 1, 227),
(143, 'Mesuji', 10, 1, 282),
(144, 'Pesawaran', 10, 1, 355),
(145, 'Pringsewu', 10, 1, 368),
(146, 'Tanggamus', 10, 1, 458),
(147, 'Tulang Bawang', 10, 1, 490),
(148, 'Tulang Bawang Barat', 10, 1, 491),
(149, 'Way Kanan', 10, 1, 496),
(150, 'Bandar Lampung', 10, 1, 21),
(151, 'Metro', 10, 1, 283),
(152, 'Lebak', 11, 1, 232),
(153, 'Pandeglang', 11, 1, 331),
(154, 'Serang', 11, 1, 402),
(155, 'Tangerang', 11, 1, 455),
(156, 'Cilegon', 11, 1, 106),
(157, 'Serang', 11, 1, 402),
(158, 'Tangerang', 11, 1, 455),
(159, 'Tangerang Selatan', 11, 1, 457),
(160, 'Kepulauan Seribu', 12, 1, 189),
(161, 'Jakarta Barat', 12, 1, 151),
(162, 'Jakarta Pusat', 12, 1, 152),
(163, 'Jakarta Selatan', 12, 1, 153),
(164, 'Jakarta Timur', 12, 1, 154),
(165, 'Jakarta Utara', 12, 1, 155),
(166, 'Bandung', 13, 1, 22),
(167, 'Bandung Barat', 13, 1, 24),
(168, 'Bekasi', 13, 1, 54),
(169, 'Bogor', 13, 1, 78),
(170, 'Ciamis', 13, 1, 103),
(171, 'Cianjur', 13, 1, 104),
(172, 'Cirebon', 13, 1, 108),
(173, 'Garut', 13, 1, 126),
(174, 'Indramayu', 13, 1, 149),
(175, 'Karawang', 13, 1, 171),
(176, 'Kuningan', 13, 1, 211),
(177, 'Majalengka', 13, 1, 252),
(178, 'Purwakarta', 13, 1, 376),
(179, 'Subang', 13, 1, 428),
(180, 'Sukabumi', 13, 1, 430),
(181, 'Sumedang', 13, 1, 440),
(182, 'Tasikmalaya', 13, 1, 468),
(183, 'Bandung', 13, 1, 22),
(184, 'Banjar', 13, 1, 33),
(185, 'Bekasi', 13, 1, 54),
(186, 'Bogor', 13, 1, 78),
(187, 'Cimahi', 13, 1, 107),
(188, 'Cirebon', 13, 1, 108),
(189, 'Depok', 13, 1, 115),
(190, 'Sukabumi', 13, 1, 430),
(191, 'Tasikmalaya', 13, 1, 468),
(192, 'Banjarnegara', 14, 1, 37),
(193, 'Banyumas', 14, 1, 41),
(194, 'Batang', 14, 1, 49),
(195, 'Blora', 14, 1, 76),
(196, 'Boyolali', 14, 1, 91),
(197, 'Brebes', 14, 1, 92),
(198, 'Cilacap', 14, 1, 105),
(199, 'Demak', 14, 1, 113),
(200, 'Grobogan', 14, 1, 134),
(201, 'Jepara', 14, 1, 163),
(202, 'Karanganyar', 14, 1, 169),
(203, 'Kebumen', 14, 1, 177),
(204, 'Kendal', 14, 1, 181),
(205, 'Klaten', 14, 1, 196),
(206, 'Tegal', 14, 1, 472),
(207, 'Kudus', 14, 1, 209),
(208, 'Magelang', 14, 1, 249),
(209, 'Pati', 14, 1, 344),
(210, 'Pekalongan', 14, 1, 348),
(211, 'Pemalang', 14, 1, 352),
(212, 'Purbalingga', 14, 1, 375),
(213, 'Purworejo', 14, 1, 377),
(214, 'Rembang', 14, 1, 380),
(215, 'Semarang', 14, 1, 398),
(216, 'Sragen', 14, 1, 427),
(217, 'Sukoharjo', 14, 1, 433),
(218, 'Temanggung', 14, 1, 476),
(219, 'Wonogiri', 14, 1, 497),
(220, 'Wonosobo', 14, 1, 498),
(221, 'Magelang', 14, 1, 249),
(222, 'Pekalongan', 14, 1, 348),
(223, 'Salatiga', 14, 1, 386),
(224, 'Semarang', 14, 1, 398),
(225, 'Surakarta (Solo)', 14, 1, 445),
(226, 'Tegal', 14, 1, 472),
(227, 'Bantul', 15, 1, 39),
(228, 'Gunung Kidul', 15, 1, 135),
(229, 'Kulon Progo', 15, 1, 210),
(230, 'Sleman', 15, 1, 419),
(231, 'Yogyakarta', 15, 1, 501),
(232, 'Bangkalan', 16, 1, 31),
(233, 'Banyuwangi', 16, 1, 42),
(234, 'Blitar', 16, 1, 74),
(235, 'Bojonegoro', 16, 1, 80),
(236, 'Bondowoso', 16, 1, 86),
(237, 'Gresik', 16, 1, 133),
(238, 'Jember', 16, 1, 160),
(239, 'Jombang', 16, 1, 164),
(240, 'Kediri', 16, 1, 178),
(241, 'Lamongan', 16, 1, 222),
(242, 'Lumajang', 16, 1, 243),
(243, 'Madiun', 16, 1, 247),
(244, 'Magetan', 16, 1, 251),
(245, 'Malang', 16, 1, 255),
(246, 'Mojokerto', 16, 1, 289),
(247, 'Nganjuk', 16, 1, 305),
(248, 'Ngawi', 16, 1, 306),
(249, 'Pacitan', 16, 1, 317),
(250, 'Pamekasan', 16, 1, 330),
(251, 'Pasuruan', 16, 1, 342),
(252, 'Ponorogo', 16, 1, 363),
(253, 'Probolinggo', 16, 1, 369),
(254, 'Sampang', 16, 1, 390),
(255, 'Sidoarjo', 16, 1, 409),
(256, 'Situbondo', 16, 1, 418),
(257, 'Sumenep', 16, 1, 441),
(258, 'Trenggalek', 16, 1, 487),
(259, 'Tuban', 16, 1, 489),
(260, 'Tulungagung', 16, 1, 492),
(261, 'Batu', 16, 1, 51),
(262, 'Blitar', 16, 1, 74),
(263, 'Kediri', 16, 1, 178),
(264, 'Madiun', 16, 1, 247),
(265, 'Malang', 16, 1, 255),
(266, 'Mojokerto', 16, 1, 289),
(267, 'Pasuruan', 16, 1, 342),
(268, 'Probolinggo', 16, 1, 369),
(269, 'Surabaya', 16, 1, 444),
(270, 'Badung', 17, 1, 17),
(271, 'Bangli', 17, 1, 32),
(272, 'Buleleng', 17, 1, 94),
(273, 'Gianyar', 17, 1, 128),
(274, 'Jembrana', 17, 1, 161),
(275, 'Karangasem', 17, 1, 170),
(276, 'Klungkung', 17, 1, 197),
(277, 'Tabanan', 17, 1, 447),
(278, 'Denpasar', 17, 1, 114),
(279, 'Bima', 18, 1, 68),
(280, 'Dompu', 18, 1, 118),
(281, 'Lombok Barat', 18, 1, 238),
(282, 'Lombok Tengah', 18, 1, 239),
(283, 'Lombok Timur', 18, 1, 240),
(284, 'Lombok Utara', 18, 1, 241),
(285, 'Sumbawa', 18, 1, 438),
(286, 'Sumbawa Barat', 18, 1, 439),
(287, 'Bima', 18, 1, 68),
(288, 'Mataram', 18, 1, 276),
(289, 'Alor', 19, 1, 13),
(290, 'Belu', 19, 1, 58),
(291, 'Ende', 19, 1, 122),
(292, 'Flores Timur', 19, 1, 125),
(293, 'Kupang', 19, 1, 212),
(294, 'Lembata', 19, 1, 234),
(295, 'Manggarai', 19, 1, 269),
(296, 'Manggarai Barat', 19, 1, 270),
(297, 'Manggarai Timur', 19, 1, 271),
(298, 'Nagekeo', 19, 1, 301),
(299, 'Ngada', 19, 1, 304),
(300, 'Rote Ndao', 19, 1, 383),
(301, 'Sabu Raijua', 19, 1, 385),
(302, 'Sikka', 19, 1, 412),
(303, 'Sumba Barat', 19, 1, 434),
(304, 'Sumba Barat Daya', 19, 1, 435),
(305, 'Sumba Tengah', 19, 1, 436),
(306, 'Sumba Timur', 19, 1, 437),
(307, 'Timor Tengah Selatan', 19, 1, 479),
(308, 'Timor Tengah Utara', 19, 1, 480),
(309, 'Kupang', 19, 1, 212),
(310, 'Bengkayang', 20, 1, 61),
(311, 'Kapuas Hulu', 20, 1, 168),
(312, 'Kayong Utara', 20, 1, 176),
(313, 'Ketapang', 20, 1, 195),
(314, 'Kubu Raya', 20, 1, 208),
(315, 'Landak', 20, 1, 228),
(316, 'Melawi', 20, 1, 279),
(317, 'Pontianak', 20, 1, 364),
(318, 'Sambas', 20, 1, 388),
(319, 'Sanggau', 20, 1, 391),
(320, 'Sekadau', 20, 1, 395),
(321, 'Sintang', 20, 1, 417),
(322, 'Pontianak', 20, 1, 364),
(323, 'Singkawang', 20, 1, 415),
(324, 'Barito Selatan', 21, 1, 44),
(325, 'Barito Timur', 21, 1, 45),
(326, 'Barito Utara', 21, 1, 46),
(327, 'Gunung Mas', 21, 1, 136),
(328, 'Kapuas', 21, 1, 167),
(329, 'Katingan', 21, 1, 174),
(330, 'Kotawaringin Barat', 21, 1, 205),
(331, 'Kotawaringin Timur', 21, 1, 206),
(332, 'Lamandau', 21, 1, 221),
(333, 'Murung Raya', 21, 1, 296),
(334, 'Pulang Pisau', 21, 1, 371),
(335, 'Seruyan', 21, 1, 405),
(336, 'Sukamara', 21, 1, 432),
(337, 'Palangka Raya', 21, 1, 326),
(338, 'Balangan', 22, 1, 18),
(339, 'Banjar', 22, 1, 33),
(340, 'Barito Kuala', 22, 1, 43),
(341, 'Hulu Sungai Selatan', 22, 1, 143),
(342, 'Hulu Sungai Tengah', 22, 1, 144),
(343, 'Hulu Sungai Utara', 22, 1, 145),
(344, 'Kab. Kota Baru', 22, 0, 107),
(345, 'Tabalong', 22, 1, 446),
(346, 'Tanah Bumbu', 22, 1, 452),
(347, 'Tanah Laut', 22, 1, 454),
(348, 'Tapin', 22, 1, 466),
(349, 'BanjarBaru', 22, 1, 35),
(350, 'Banjarmasin', 22, 1, 36),
(351, 'Berau', 23, 1, 66),
(352, 'Kab. Bulongan', 23, 0, 96),
(353, 'Kutai Barat', 23, 1, 214),
(354, 'Kutai Kartanegara', 23, 1, 215),
(355, 'Kutai Timur', 23, 1, 216),
(356, 'Malinau', 23, 1, 257),
(357, 'Nunukan', 23, 1, 311),
(358, 'Paser', 23, 1, 341),
(359, 'Penajam Paser Utara', 23, 1, 354),
(360, 'Tana Tidung', 23, 1, 450),
(361, 'Balikpapan', 23, 1, 19),
(362, 'Bontang', 23, 1, 89),
(363, 'Samarinda', 23, 1, 387),
(364, 'Tarakan', 23, 1, 467),
(365, 'Boalemo', 24, 1, 77),
(366, 'Bone Bolango', 24, 1, 88),
(367, 'Gorontalo', 24, 1, 129),
(368, 'Gorontalo Utara', 24, 1, 131),
(369, 'Pohuwato', 24, 1, 361),
(370, 'Gorontalo', 24, 1, 129),
(371, 'Bantaeng', 25, 1, 38),
(372, 'Barru', 25, 1, 47),
(373, 'Bone', 25, 1, 87),
(374, 'Bulukumba', 25, 1, 95),
(375, 'Enrekang', 25, 1, 123),
(376, 'Gowa', 25, 1, 132),
(377, 'Jeneponto', 25, 1, 162),
(378, 'Luwu', 25, 1, 244),
(379, 'Luwu Timur', 25, 1, 245),
(380, 'Luwu Utara', 25, 1, 246),
(381, 'Maros', 25, 1, 275),
(382, 'Pangkajene Kepulauan', 25, 1, 333),
(383, 'Pinrang', 25, 1, 360),
(384, 'Selayar (Kepulauan Selayar)', 25, 1, 396),
(385, 'Sidenreng Rappang/Rapang', 25, 1, 408),
(386, 'Sinjai', 25, 1, 416),
(387, 'Soppeng', 25, 1, 423),
(388, 'Takalar', 25, 1, 448),
(389, 'Tana Toraja', 25, 1, 451),
(390, 'Toraja Utara', 25, 1, 486),
(391, 'Wajo', 25, 1, 493),
(392, 'Makassar', 25, 1, 254),
(393, 'Palopo', 25, 1, 328),
(394, 'Parepare', 25, 1, 336),
(395, 'Bombana', 26, 1, 85),
(396, 'Buton', 26, 1, 101),
(397, 'Buton Utara', 26, 1, 102),
(398, 'Kolaka', 26, 1, 198),
(399, 'Kolaka Utara', 26, 1, 199),
(400, 'Konawe', 26, 1, 200),
(401, 'Konawe Selatan', 26, 1, 201),
(402, 'Konawe Utara', 26, 1, 202),
(403, 'Muna', 26, 1, 295),
(404, 'Wakatobi', 26, 1, 494),
(405, 'Bau-bau', 26, 1, 53),
(406, 'Kendari', 26, 1, 182),
(407, 'Banggai', 27, 1, 25),
(408, 'Banggai Kepulauan', 27, 1, 26),
(409, 'Buol', 27, 1, 98),
(410, 'Donggala', 27, 1, 119),
(411, 'Morowali', 27, 1, 291),
(412, 'Parigi Moutong', 27, 1, 338),
(413, 'Poso', 27, 1, 366),
(414, 'Sigi', 27, 1, 410),
(415, 'Tojo Una-Una', 27, 1, 482),
(416, 'Toli-Toli', 27, 1, 483),
(417, 'Palu', 27, 1, 329),
(418, 'Bolaang Mongondow (Bolmong)', 28, 1, 81),
(419, 'Bolaang Mongondow Selatan', 28, 1, 82),
(420, 'Bolaang Mongondow Timur', 28, 1, 83),
(421, 'Bolaang Mongondow Utara', 28, 1, 84),
(422, 'Kepulauan Sangihe', 28, 1, 188),
(423, 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 28, 1, 190),
(424, 'Kepulauan Talaud', 28, 1, 192),
(425, 'Minahasa', 28, 1, 285),
(426, 'Minahasa Selatan', 28, 1, 286),
(427, 'Minahasa Tenggara', 28, 1, 287),
(428, 'Minahasa Utara', 28, 1, 288),
(429, 'Bitung', 28, 1, 73),
(430, 'Kotamobagu', 28, 1, 204),
(431, 'Manado', 28, 1, 267),
(432, 'Tomohon', 28, 1, 485),
(433, 'Majene', 29, 1, 253),
(434, 'Mamasa', 29, 1, 262),
(435, 'Mamuju', 29, 1, 265),
(436, 'Mamuju Utara', 29, 1, 266),
(437, 'Polewali Mandar', 29, 1, 362),
(438, 'Buru', 30, 1, 99),
(439, 'Buru Selatan', 30, 1, 100),
(440, 'Kepulauan Aru', 30, 1, 185),
(441, 'Maluku Barat Daya', 30, 1, 258),
(442, 'Maluku Tengah', 30, 1, 259),
(443, 'Maluku Tenggara', 30, 1, 260),
(444, 'Maluku Tenggara Barat', 30, 1, 261),
(445, 'Seram Bagian Barat', 30, 1, 400),
(446, 'Seram Bagian Timur', 30, 1, 401),
(447, 'Ambon', 30, 1, 14),
(448, 'Tual', 30, 1, 488),
(449, 'Halmahera Barat', 31, 1, 138),
(450, 'Halmahera Selatan', 31, 1, 139),
(451, 'Halmahera Tengah', 31, 1, 140),
(452, 'Halmahera Timur', 31, 1, 141),
(453, 'Halmahera Utara', 31, 1, 142),
(454, 'Kepulauan Sula', 31, 1, 191),
(455, 'Pulau Morotai', 31, 1, 372),
(456, 'Ternate', 31, 1, 477),
(457, 'Tidore Kepulauan', 31, 1, 478),
(458, 'Fakfak', 32, 1, 124),
(459, 'Kaimana', 32, 1, 165),
(460, 'Manokwari', 32, 1, 272),
(461, 'Maybrat', 32, 1, 277),
(462, 'Raja Ampat', 32, 1, 378),
(463, 'Sorong', 32, 1, 424),
(464, 'Sorong Selatan', 32, 1, 426),
(465, 'Tambrauw', 32, 1, 449),
(466, 'Teluk Bintuni', 32, 1, 474),
(467, 'Teluk Wondama', 32, 1, 475),
(468, 'Sorong', 32, 1, 424),
(469, 'Merauke', 33, 1, 281),
(470, 'Jayawijaya', 33, 1, 159),
(471, 'Nabire', 33, 1, 299),
(472, 'Kepulauan Yapen (Yapen Waropen)', 33, 1, 193),
(473, 'Biak Numfor', 33, 1, 67),
(474, 'Paniai', 33, 1, 335),
(475, 'Puncak Jaya', 33, 1, 374),
(476, 'Mimika', 33, 1, 284),
(477, 'Boven Digoel', 33, 1, 90),
(478, 'Mappi', 33, 1, 274),
(479, 'Asmat', 33, 1, 16),
(480, 'Yahukimo', 33, 1, 499),
(481, 'Pegunungan Bintang', 33, 1, 347),
(482, 'Tolikara', 33, 1, 484),
(483, 'Sarmi', 33, 1, 392),
(484, 'Keerom', 33, 1, 180),
(485, 'Waropen', 33, 1, 495),
(486, 'Jayapura', 33, 1, 157),
(487, 'Deiyai (Deliyai)', 33, 1, 111),
(488, 'Dogiyai', 33, 1, 117),
(489, 'Intan Jaya', 33, 1, 150),
(490, 'Lanny Jaya', 33, 1, 231),
(491, 'Mamberamo Raya', 33, 1, 263),
(492, 'Mamberamo Tengah', 33, 1, 264),
(493, 'Nduga', 33, 1, 303),
(494, 'Puncak', 33, 1, 373),
(495, 'Supiori', 33, 1, 443),
(496, 'Yalimo', 33, 1, 500),
(497, 'Jayapura', 33, 1, 157),
(498, 'Bulungan (Bulongan)', 34, 1, 96),
(499, 'Kab. Malinau', 34, 0, 257),
(500, 'Kab. Nunukan', 34, 0, 311),
(501, 'Kab. Tana Tidung', 34, 0, 450),
(502, 'Kota Tarakan', 34, 0, 467);

-- --------------------------------------------------------

--
-- Table structure for table `ref_provinsi`
--

CREATE TABLE `ref_provinsi` (
  `id_provinsi` int(10) NOT NULL,
  `nm_provinsi` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=481 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_provinsi`
--

INSERT INTO `ref_provinsi` (`id_provinsi`, `nm_provinsi`) VALUES
(1, 'Nanggroe Aceh Darussalam'),
(2, 'Sumatera Utara'),
(3, 'Sumatera Barat'),
(4, 'Riau'),
(5, 'Kepulauan Riau'),
(6, 'Kepulauan Bangka-Belitung'),
(7, 'Jambi'),
(8, 'Bengkulu'),
(9, 'Sumatera Selatan'),
(10, 'Lampung'),
(11, 'Banten'),
(12, 'DKI Jakarta'),
(13, 'Jawa Barat'),
(14, 'Jawa Tengah'),
(15, 'Daerah Istimewa Yogyakarta  '),
(16, 'Jawa Timur'),
(17, 'Bali'),
(18, 'Nusa Tenggara Barat'),
(19, 'Nusa Tenggara Timur'),
(20, 'Kalimantan Barat'),
(21, 'Kalimantan Tengah'),
(22, 'Kalimantan Selatan'),
(23, 'Kalimantan Timur'),
(24, 'Gorontalo'),
(25, 'Sulawesi Selatan'),
(26, 'Sulawesi Tenggara'),
(27, 'Sulawesi Tengah'),
(28, 'Sulawesi Utara'),
(29, 'Sulawesi Barat'),
(30, 'Maluku'),
(31, 'Maluku Utara'),
(32, 'Papua Barat'),
(33, 'Papua'),
(34, 'Kalimantan Utara');

-- --------------------------------------------------------

--
-- Table structure for table `ref_type_barang`
--

CREATE TABLE `ref_type_barang` (
  `id` int(11) NOT NULL,
  `nama_type` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `time_online` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `status_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `permission` int(11) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'avatar1.png',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_karyawan`, `name`, `username`, `password`, `time_online`, `status_user`, `status`, `permission`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Jhane Doe', 'admin', '$2y$10$iO5xWzBW0JX462nn/wY/7et8akByszoR6GtDmn2DCTOMKlUP7GcFe', '1448964407', '', 1, 3, 'avatar8.png', '9lBeEewQkRGfcDG2vFCDeF8KS9zq1WnZvVJzzYmUNTW3N7nwbFp3BqTVThCN', '2015-07-08 07:13:02', '2015-12-01 09:51:47'),
(7, 2, 'Jhone Doe', 'client', '$2y$10$Qj8F.BHTBaXq/kTsDZfZUuhAfZopATO4wP1psyFpyjgcIVRBGIcIy', '1436602766', '', 1, 3, 'avatar5.png', 'lOg0xzssK0ghFArNMJfVywGGIDSvmQj6VnCka9eEhLSs9JFOB4Cc8S9hJONX', '2015-07-10 22:13:49', '2015-07-11 01:04:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_aktivitas`
--
ALTER TABLE `data_aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`);

--
-- Indexes for table `data_barang`
--
ALTER TABLE `data_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_distributor`
--
ALTER TABLE `data_distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `data_level`
--
ALTER TABLE `data_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `data_level_user`
--
ALTER TABLE `data_level_user`
  ADD PRIMARY KEY (`id_level_user`);

--
-- Indexes for table `data_menu`
--
ALTER TABLE `data_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `data_menu_user`
--
ALTER TABLE `data_menu_user`
  ADD PRIMARY KEY (`id_menu_user`);

--
-- Indexes for table `data_reseller`
--
ALTER TABLE `data_reseller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `ref_kab_kota`
--
ALTER TABLE `ref_kab_kota`
  ADD PRIMARY KEY (`id_kab_kota`),
  ADD KEY `pro_kota` (`id_provinsi`);

--
-- Indexes for table `ref_provinsi`
--
ALTER TABLE `ref_provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `ref_type_barang`
--
ALTER TABLE `ref_type_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_aktivitas`
--
ALTER TABLE `data_aktivitas`
  MODIFY `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `data_barang`
--
ALTER TABLE `data_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_distributor`
--
ALTER TABLE `data_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=298;
--
-- AUTO_INCREMENT for table `data_level`
--
ALTER TABLE `data_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `data_level_user`
--
ALTER TABLE `data_level_user`
  MODIFY `id_level_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `data_menu`
--
ALTER TABLE `data_menu`
  MODIFY `id_menu` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `data_menu_user`
--
ALTER TABLE `data_menu_user`
  MODIFY `id_menu_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `data_reseller`
--
ALTER TABLE `data_reseller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ref_kab_kota`
--
ALTER TABLE `ref_kab_kota`
  MODIFY `id_kab_kota` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=503;
--
-- AUTO_INCREMENT for table `ref_provinsi`
--
ALTER TABLE `ref_provinsi`
  MODIFY `id_provinsi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `ref_type_barang`
--
ALTER TABLE `ref_type_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
