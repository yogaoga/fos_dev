<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ref_blog_kategori extends Model
{
	protected $table 		= 'ref_blog_kategori';
	protected $primaryKey 	= 'id';
	protected $fillable 		= [
	'nm_kategori'
	];
}
