<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_pesan extends Model
{
    protected $table = 'data_pesan';
	protected $fillable = [
		'email',
		'content',
		'status',
		'id_petugas',
	];
}
