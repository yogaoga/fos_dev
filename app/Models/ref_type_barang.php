<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ref_type_barang extends Model
{
    protected $table 		= 'ref_type_barang';
	protected $fillable 		= [
		'nama_type'
	];
}
