<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_banner extends Model
{
    protected $table = 'data_banner';
	protected $fillable = [
	'gambar',
	'status',
	];
}
