<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_blog extends Model
{
    protected $table = 'data_blog';
	protected $fillable = [
		'judul',
		'id_petugas',
		'content',
		'status',
		'gambar'
	];
}
