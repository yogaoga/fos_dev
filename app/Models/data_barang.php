<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_barang extends Model
{
    protected $table = 'data_barang';
	protected $fillable = [
		'kd_barang',
		'nm_barang',
		'id_artist',
		'id_distributor',
		'type_barang',
		'stok',
		'harga',
		'discount',
		'gambar',
		'id_warna',
		'size',
		'deskripsi'
	];
}
