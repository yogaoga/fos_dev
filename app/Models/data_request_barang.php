<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_request_barang extends Model
{
    protected $table = 'data_request_barang';
	protected $fillable = [
		'kd_barang',
		'qty',
		'id_user'
	];
}
