<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_warna extends Model
{
    protected $table = 'data_warna';
	protected $fillable = [
	'nm_warna'
	];
}
