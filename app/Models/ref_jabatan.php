<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ref_jabatan extends Model
{
    protected $table 		= 'ref_jabatan';
	protected $primaryKey 	= 'id';
	protected $fillable 		= [
		'nm_jabatan'
	];
}
