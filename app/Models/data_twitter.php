<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data_twitter extends Model
{
	protected $table = 'data_twitter';
	protected $fillable = [
	'id_karyawan',
	'username',
	'password',
	'widget_id'
	];
}
