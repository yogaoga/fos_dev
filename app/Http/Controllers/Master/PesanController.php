<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_pesan;
class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author yoga@valdlabs.com
     */
    public function getIndex()
    {
        $items = data_pesan::where('id_petugas',\Auth::user()->id_karyawan)->paginate(10);
        return view('Pesan.index',[
            'items' => $items
            ]);
    }

}
