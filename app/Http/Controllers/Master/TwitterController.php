<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_twitter;

class TwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {

        $items = data_twitter::join('data_karyawan','data_twitter.id_karyawan','=','data_karyawan.id_karyawan')->paginate(10);
        return view('Twitter.index',[
            'items' => $items
            ]);
    }

    public function getEdit($id){
        $item = data_twitter::find($id);
        return view('Twitter.edit',[
            'item' => $item
            ]);
    }

    public function postUpdate(Request $req){
        data_twitter::find($req->id)->update([
            'username' => $req->username,
            'widget_id' => $req->widget_id,
            'password'  => $req->password
            ]);

        return redirect()->back()->withNotif([
        'label' => 'success',
        'err' =>'Barang Berhasil diubah'
        ]);
    }



}
