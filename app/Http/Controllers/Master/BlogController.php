<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_blog;

use Input;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author yoga@valdlabs.com
     */
    public function getIndex()
    {
        return view('Blog.index',[
            'items' => data_blog::join('data_karyawan','data_karyawan.id_karyawan','=','data_blog.id_petugas')
            ->where('status',1)->paginate(10),
            ]);        
    }

    public function getCreate()
    {
        return view('Blog.create');
    }

    public function postCreate(Request $req)
    {

        if($req->hasFile('gambar')){

            $file = Input::file('gambar');
            $date = date('Y-m-d');
            $name = $file->getClientOriginalName();

            $req->file('gambar')->move(public_path() . '/assets/img/blog/', $name);

            data_blog::create([
                'judul' => $req->judul,
                'id_petugas' => \Auth::user()->id_karyawan,
                'content' => $req->content,
                'gambar' => $name,
                'status' => 1
                ]);
        }
        return redirect()->back()->withNotif([
            'label' => 'success',
            'err' =>'Blog Berhasil ditambahkan'
            ]);
    }

    public function getEdit($id)
    {   
        $item = data_blog::find($id);
        return view('Blog.edit',[
            'item' => $item
            ]);
    }

    public function postEdit(Request $req)
    {

        if($req->hasFile('gambar')){

            $file = Input::file('gambar');
            $date = date('Y-m-d');
            $name = $file->getClientOriginalName();

            $req->file('gambar')->move(public_path() . '/assets/img/blog/', $name);

            data_blog::find($req->id)->update([
                'judul' => $req->judul,
                'id_petugas' => \Auth::user()->id_karyawan,
                'content' => $req->content,
                'gambar' => $name,
                'status' => 1
                ]);
        }else{

            data_blog::find($req->id)->update([
                'judul' => $req->judul,
                'id_petugas' => \Auth::user()->id_karyawan,
                'content' => $req->content,
                'status' => 1
                ]);
        }
        return redirect()->back()->withNotif([
            'label' => 'success',
            'err' =>'Blog Berhasil ditambahkan'
            ]);
    }

    public function getReview($id)
    {
        $item = data_blog::join('data_karyawan','data_karyawan.id_karyawan','=','data_blog.id_petugas')
        ->where('id',$id)
        ->first();
        return view('Blog.lihat',[
            'item' => $item
            ]);
    }

    public function postDestroy(Request $req)
    {
        data_blog::find($req->id)->delete();

        return json_encode([
            'result' => true
            ]);
    }

    public function getAllitems(Request $req)
    {
        if($req->ajax()):
            $res = [];

        $items = data_blog::join('data_karyawan','data_karyawan.id_karyawan','=','data_blog.id_petugas')
        ->where('status',1)->paginate(10);


        $out = '';
        if($items->total() > 0){
            $no = $items->currentPage() == 1 ? 1 : ($items->perPage() * $items->currentPage()) - $items->perPage() + 1;
            foreach($items as $item){
                $btn = \Auth::user()->permission > 2 ? '<button type="button" class="close hapus" onclick="hapus(\'' . $item->id . '\', ' . $item->id . ');"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' : '';


                $out .= '
                <tr class="item_' .  $item->id . ' items">
                    <td>' . $no . '</td>
                    <td>
                        <a href="javascript:;" title="' . $item->judul . '" data-toggle="tooltip" data-placement="bottom">' . $item->judul .'</a>
                        <div style="display:none;" class="tbl-opsi">
                            <small>[
                             <a href="' . url('adminblog/edit/'. $item->id). '">Edit</a>

                             ]</small>
                         </div>
                     </td>
                     <td>'. $item->nm_depan .' '. $item->nm_belakang .'</td>           
                     <td>'. $item->created_at .'</td>
                     <td>'. $btn .'</td>

                 </tr>
                 ';
                 $no++;
             }
         }else{
            $out = '
            <tr>
                <td colspan="4">Tidak ditemukan</td>
            </tr>
            ';
        }

        $res['data'] = $out;
        $res['pagin'] = $items->render();

        return json_encode($res);

        endif;
    }



}
