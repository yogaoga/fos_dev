<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_barang;
use App\Models\data_karyawan;
use App\Models\ref_type_barang;
use App\Models\data_warna;

use Input;
class BarangController extends Controller
{

/**
* @author yoga@valdlabs.com
*/

public function __construct(){
	$this->middleware('auth');
}

public function getIndex()
{	
	if(\Auth::user()->id_user == 1)
	{
		$items = data_barang::join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
		->join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
		->select('data_barang.*','data_karyawan.*','ref_type_barang.nama_type')
		->paginate(10);
	}
	else
	{
		$items = data_barang::join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
		->join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
		->where('id_distributor',\Auth::user()->id_karyawan)
		->select('data_barang.*','data_karyawan.*','ref_type_barang.nama_type')
		->paginate(10);
	}
	return view('Barang.index',[
		'items' => $items
		]);
}

public function getCreate()
{

	return view('Barang.create',[
		'artist' => data_karyawan::where('jabatan',2)->get(),
		'type' => ref_type_barang::all(),
		'warna' => data_warna::all(),
		]);
}

public function getEdit($id)
{

	$data = data_barang::where('id',$id)->first();
	
	return view('Barang.edit',[
		'data' => $data,
		'artist' => data_karyawan::where('jabatan',2)->get(),
		'type' => ref_type_barang::all(),
		'warna' => data_warna::all(),
		]);
}

public function postCreate(Request $req)
{	
	if($req->hasFile('gambar')){

		$file = Input::file('gambar');
		$date = date('Y-m-d');
		$name = $file->getClientOriginalName();

		$req->file('gambar')->move(public_path() . '/assets/img/promo/', $name);

		
		data_barang::create([
			'kd_barang' => $req->kd_barang,
			'nm_barang' => $req->nm_barang,
			'id_artist' => $req->id_artist,
			'id_distributor' => $req->id_distributor,
			'type_barang' => $req->type_barang,
			'stok' => $req->stok,
			'harga' => $req->harga,
			'discount' => $req->discount,
			'gambar' => $name,
			'id_warna' => $req->id_warna,
			'size' => $req->size,
			'deskripsi' => $req->deskripsi
			]);
	}
	return redirect()->back()->withNotif([
		'label' => 'success',
		'err' =>'Barang Berhasil ditambahkan'
		]);

}

public function postEdit(Request $req)
{

	if($req->hasFile('gambar')){

		$file = Input::file('gambar');
		$date = date('Y-m-d');
		$name = $file->getClientOriginalName();

		$req->file('gambar')->move(public_path() . '/assets/img/promo/', $name);

		
		data_barang::find($req->id)->update([
			'kd_barang' => $req->kd_barang,
			'nm_barang' => $req->nm_barang,
			'id_artist' => $req->id_artist,
			'id_distributor' => $req->id_distributor,
			'type_barang' => $req->type_barang,
			'stok' => $req->stok,
			'harga' => $req->harga,
			'discount' => $req->discount,
			'gambar' => $name,
			'id_warna' => $req->id_warna,
			'size' => $req->size,
			'deskripsi' => $req->deskripsi
			]);
	}else{
	
		data_barang::find($req->id)->update([
			'kd_barang' => $req->kd_barang,
			'nm_barang' => $req->nm_barang,
			'id_artist' => $req->id_artist,
			'id_distributor' => $req->id_distributor,
			'type_barang' => $req->type_barang,
			'stok' => $req->stok,
			'harga' => $req->harga,
			'discount' => $req->discount,
			'id_warna' => $req->id_warna,
			'size' => $req->size,
			'deskripsi' => $req->deskripsi
			]);
	}
	return redirect()->back()->withNotif([
		'label' => 'success',
		'err' =>'Barang Berhasil diubah'
		]);
}

public function postDestroy(Request $req)
{
	data_barang::find($req->id)->delete();

	return json_encode([
		'result' => true
		]);
}

public function getAllitems(Request $req)
{
	if($req->ajax()):
		$res = [];

	if(\Auth::user()->id_user == 1)
	{
		$items = data_barang::join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
		->join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
		->where('nm_barang','like','%'.$req->src.'%')
		->select('data_barang.*','data_karyawan.*','ref_type_barang.nama_type')
		->paginate(10);
	}
	else
	{
		$items = data_barang::join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
		->join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
		->where('id_distributor',\Auth::user()->id_karyawan)
		->where('kd_barang',$req->src)
		->select('data_barang.*','data_karyawan.*','ref_type_barang.nama_type')
		->paginate(10);
	}

	$out = '';
	if($items->total() > 0){
		$no = $items->currentPage() == 1 ? 1 : ($items->perPage() * $items->currentPage()) - $items->perPage() + 1;
		foreach($items as $item){
			$btn = \Auth::user()->permission > 2 ? '<button type="button" class="close hapus" onclick="hapus(\'' . $item->id . '\', ' . $item->id . ');"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' : '';


			$out .= '
			<tr class="item_' .  $item->id . ' items">
				<td>' . $no . '</td>
				<td>
					<a href="javascript:;" title="' . $item->kd_barang . '" data-toggle="tooltip" data-placement="bottom">' . $item->kd_barang .'</a>
					<div style="display:none;" class="tbl-opsi">
						<small>[
							 <a href="' . url('barang/edit/'. $item->id). '">Edit</a>

							]</small>
						</div>
					</td>
					<td>'. $item->nm_barang .'</td>           
					<td>
						<div>
							' . $item->nm_depan . ' ' . $item->nm_belakang . '
						</div>
					</td>
					<td>'. $item->nama_type .'</td>
					<td>'. $item->stok .'</td>
					<td>'. $btn .'</td>

				</tr>
				';
				$no++;
			}
		}else{
			$out = '
			<tr>
				<td colspan="8">Tidak ditemukan</td>
			</tr>
			';
		}

		$res['data'] = $out;
		$res['pagin'] = $items->render();

		return json_encode($res);

		endif;
	}
}
