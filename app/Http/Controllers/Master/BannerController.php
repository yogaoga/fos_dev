<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_banner;

use Input;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author yoga@valdlabs.com
     */
    public function getIndex()
    {
        $items = data_banner::paginate(10);

        return view('Banner.index',[
            'items' => $items
            ]);
    }

    public function getCreate()
    {
        return view('Banner.create');
    }

    public function postCreate(Request $req)
    {
        if($req->hasFile('gambar')){

        $file = Input::file('gambar');
        $date = date('Y-m-d');
        $name = $file->getClientOriginalName();

        $names = str_replace(' ', '_', $name);

        $req->file('gambar')->move(public_path() . '/assets/img/banner/', str_replace(' ', '_', $names));

        
        data_banner::create([
            'gambar' => $names,
            'status' => $req->status
            ]);
    }
    return redirect()->back()->withNotif([
        'label' => 'success',
        'err' =>'Barang Berhasil ditambahkan'
        ]);
    }

    public function getEdit()
    {
        return view('Banner.edit');
    }

    public function postEdit(Request $req)
    {

    }

    public function getDestroy($id)
    {
        data_banner::find($id)->delete();

        return redirect()->back();
    }
}
