<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\data_warna;

class WarnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author yoga@valdlabs.com
     */
    public function getIndex()
    {
        $items = data_warna::paginate(10);

        return view('Warna.index',[
            'items' => $items
            ]);
    }

    public function getCreate()
    {
        return view('Warna.create');
    }

    public function postCreate(Request $req)
    {
        data_warna::create([
            'nm_warna' => $req->nm_warna
            ]);

        return redirect()->back()->withNotif([
           'label' => 'success',
           'err' => 'Warna berhasil dibuat'
           ]);
    }

    public function getEdit($id)
    {
        $data = data_warna::find($id);

        return view('Warna.edit',[
            'data' => $data
            ]);
    }

    public function postEdit(Request $req)
    {
        data_warna::find($req->id)->update([
            'nm_warna' => $req->nm_warna
            ]);
        return redirect()->back()->withNotif([
           'label' => 'success',
           'err' => 'Warna berhasil diubah'
           ]);

    }

    public function getAllitems(Request $req)
    {
        
    }
}
