<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jobs\Personalia\KaryawanJob as InsertKaryawan;

use App\Events\Users\UploadAvatarEvent;

use App\Models\data_karyawan;
use App\Models\ref_jabatan;
use App\User;
use App\Models\data_level;

use Input;

class KaryawanController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

	/**
	* Daftar Master Karyawan
	* @access protected
	* @author yoga@valdlabs.com
	*/

	public function getIndex(){
		
		$items = data_karyawan::join('ref_jabatan','ref_jabatan.id','=','data_karyawan.jabatan')->paginate(10);
		
		return view('Personalia.Karyawan.MasterKaryawan',[
			'items' => $items,
			]);

	}

	public function getAdd(){

		$ref_jabatan 	= ref_jabatan::all();

		return view('Personalia.Karyawan.CreateKaryawan',[
			'ref_jabatan' => $ref_jabatan
			]);
	}

	public function postAdd(Request $req){

		if($req->hasFile('foto')){
			
			$file = Input::file('foto');
			$date = date('Y-m-d');
			$name = $file->getClientOriginalName();

			$req->file('foto')->move(public_path() . '/assets/img/client/', $name);


			$karyawan = data_karyawan::create([
				'nm_depan' => $req->nm_depan,
				'nm_belakang' => $req->nm_belakang,
				'email' => $req->email,
				'sex' => $req->gender,
				'hp' => $req->hp,
				'tempat_lahir' => $req->tempat_lahir,
				'tgl_lahir' => date('Y-m-d',strtotime($req->tgl_lahir)),
				'jabatan' => $req->jabatan,
				'alamat' => $req->alamat,
				'id_status' => 1,
				'foto' => $name,
				'deskripsi' => $req->deskripsi,
				'tgl_bergabung' => date('Y-m-d'),
				'facebook' => $req->fb,
				'twitter' => $req->twitter,
				'instagram' => $req->ig
				]);

			$user = User::create([
				'name' => $req->nm_depan .' '. $req->nm_belakang,
				'username' => $req->username,
				'password' => bcrypt($req->password),
				'id_karyawan' => $karyawan->id_karyawan,
				'permission' => 3
				]);

			$level = data_level::firstOrCreate([
				'id_user' => $user->id_user,
				'id_level_user' => $req->jabatan
				]);
		}

		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => $req->nm_depan . ' berhasil tersimpan di Database'
			]);

	}


	public function getUpdate($id){

		$karyawan = data_karyawan::find($id);
		
		return view('Personalia.Karyawan.UpdateKaryawan',[
			'karyawan' => $karyawan,
			'ref_jabatan' => ref_jabatan::all()
			]);
		
	}

	public function postUpdate(Request $req){

		if($req->hasFile('foto')){
			
			$file = Input::file('foto');
			$date = date('Y-m-d');
			$name = $file->getClientOriginalName();

			$req->file('foto')->move(public_path() . '/assets/img/client/', $name);

		
		data_karyawan::find($req->id)
        ->update([
            'nm_depan' => $req->nm_depan,
                'nm_belakang' => $req->nm_belakang,
                'email' => $req->email,
                'sex' => $req->gender,
                'hp' => $req->hp,
                'tempat_lahir' => $req->tempat_lahir,
                'tgl_lahir' => date('Y-m-d',strtotime($req->tgl_lahir)),
                'jabatan' => $req->jabatan,
                'alamat' => $req->alamat,
                'id_status' => 1,
                'foto' => $name,
                'deskripsi' => $req->deskripsi,
                'facebook' => $req->fb,
				'twitter' => $req->twitter,
				'instagram' => $req->ig
            ]);
    }else{
    	data_karyawan::find($req->id)
        ->update([
            'nm_depan' => $req->nm_depan,
                'nm_belakang' => $req->nm_belakang,
                'email' => $req->email,
                'sex' => $req->gender,
                'hp' => $req->hp,
                'tempat_lahir' => $req->tempat_lahir,
                'tgl_lahir' => date('Y-m-d',strtotime($req->tgl_lahir)),
                'jabatan' => $req->jabatan,
                'alamat' => $req->alamat,
                'id_status' => 1,
                'deskripsi' => $req->deskripsi,
                'deskripsi' => $req->deskripsi,
                'facebook' => $req->fb,
				'twitter' => $req->twitter,
				'instagram' => $req->ig
                ]);
    }

		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => $req->nm_depan . ' berhasil terupdate di Database'
			]);
	}

	public function getReview($id){

		$karyawan = data_karyawan::find($id);
		$jabatan = ref_jabatan::where('id',$karyawan->jabatan)->first();

		return view('Personalia.Karyawan.ReviewKaryawan',[
			'karyawan' => $karyawan,
			'jabatan' => $jabatan
			]);

	}

	public function postDestroy(Request $req){

		data_karyawan::find($req->id)->delete();

		return json_encode([
			'result' => true
			]);
	}

	
	public function getPhoto($id){
		return view('Personalia.Karyawan.Photo',[
			'id_karyawan' => $id,
			]);
	}

	public function postPhoto(UploadAvatarEvent $file, Request $req){
		$protect = [
		'avatar1.png',
		'avatar2.png',
		'avatar3.png',
		'avatar4.png',
		'avatar5.png',
		'avatar6.png',
		'avatar7.png',
		'avatar8.png',
		'avatar9.png',
		'avatar10.png',
		'avatar11.png',
		'avatar12.png',
		];

		$avatar = $req->avatar == null ? $protect[0] : $req->avatar;
		
		if(!empty($_FILES['image']['tmp_name'])){
			$avatar = $file->save($_FILES['image']['tmp_name'], [
				'x' => $req->x,
				'y' => $req->y,
				'w' => $req->w,
				'h' => $req->h,
				'r' => $req->r
				]);
		}
		if(!in_array(\Auth::user()->avatar, $protect)){
			$file->rm(\Auth::user()->avatar);
		}

		$user = data_karyawan::find($req->id)->update([
			'foto' => $avatar
			]);

		return redirect()
		->back()
		->withNotif([
			'label' => 'success',
			'err' => 'Avatar berhasil diperbaharui'
			]);
	}

	function getPrint($id){
		$karyawan = data_karyawan::find($id);
		$keluarga = data_karyawan_klrg::where('id_karyawan',$karyawan->id_karyawan)->get();

		$status_karyawan = data_personalia::join('ref_status_karyawan','ref_status_karyawan.id','=','data_personalia.id_status')
		->where('tipe_status',1)
		->where('id_karyawan',$karyawan->id_karyawan)
		->get();


		$catatan = data_personalia::join('ref_status_karyawan','ref_status_karyawan.id','=','data_personalia.id_status')
		->where('tipe_status',3)
		->orWhere('tipe_status',4)
		->orWhere('tipe_status',5)
		->where('id_karyawan',$karyawan->id_karyawan)
		->get();

		$jabatan = ref_jabatan::where('id',$karyawan->jabatan)->first();
		$agama   = ref_agama::where('id',$karyawan->agama)->first();

		return view('Print.Personalia.karyawan',[
			'karyawan' => $karyawan,
			'keluarga' => $keluarga,
			'status_karyawan' => $status_karyawan,
			'jabatan' => $jabatan,
			'agama' => $agama,
			'catatan' => $catatan
			]); 
	}

	public function getAllitems(Request $req){

		if($req->ajax()):
			$res = [];

		$items = data_karyawan::join('ref_jabatan','ref_jabatan.id','=','data_karyawan.jabatan')
		->where('nm_depan','like',$req->src."%")
		->paginate(10);

		$out = '';
		if($items->total() > 0){
			$no = $items->currentPage() == 1 ? 1 : ($items->perPage() * $items->currentPage()) - $items->perPage() + 1;
			foreach($items as $item){
				$btn = \Auth::user()->permission > 2 ? '<button type="button" class="close hapus" onclick="hapus(\'' . $item->id_karyawan . '\', ' . $item->id_karyawan . ');"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' : '';

				$out .= '
				<tr class="item_' .  $item->id_karyawan . ' items">
					<td>' . $no . '</td>
					<td>
						<a href="javascript:;" title="' . $item->nm_depan . '" data-toggle="tooltip" data-placement="bottom">' . $item->nm_depan . ' '. $item->nm_belakang .'</a>
						<div style="display:none;" class="tbl-opsi">
							<small>[
								<a href="'. url('petugas/review/'. $item->id_karyawan) .'">Lihat</a>
								| <a href="' . url('petugas/update/'. $item->id_karyawan). '">Edit</a>

								]</small>
							</div>
						</td>
						<td>'. $item->nm_jabatan .'</td>           
						<td>
							<div>
								' . \Format::indoDate($item->created_at) . '
							</div>
							<small class="text-muted">' . \Format::hari($item->created_at) . ', ' . \Format::jam($item->created_at) . '</small>
						</td>
						<td>'. $btn .'</td>
					</tr>
					';
					$no++;
				}
			}else{
				$out = '
				<tr>
					<td colspan="4">Tidak ditemukan</td>
				</tr>
				';
			}

			$res['data'] = $out;
			$res['pagin'] = $items->render();

			return json_encode($res);

			endif;
		}

	}
