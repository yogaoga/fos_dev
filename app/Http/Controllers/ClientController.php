<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jobs\Personalia\KaryawanJob as InsertKaryawan;

use App\Models\ref_jabatan;

use App\Models\data_barang;
use App\Models\data_karyawan;
use App\Models\data_blog;
use App\Models\data_pesan;
use App\Models\data_request_barang;
use App\Models\ref_blog_kategori;
use App\Models\data_banner;
use App\Models\data_warna;
use App\Models\data_twitter;
use App\User;


use Session;
use Auth;
use Mail;

class ClientController extends Controller
{
    /**
	 * 	@author yoga@valdlabs.com
     */

    public function comingsoon()
    {
      return view('client.coming');
    }

    public function index()
    {
    	$barangs = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
      ->join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->limit(12)
      ->orderBy(\DB::raw('RAND()'))
      ->get();
      $artists = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->where('jabatan',2)
      ->orderBy(\DB::raw('RAND()'))
      ->get();

      $blogs = data_blog::limit(5)
      ->orderBy(\DB::raw('RAND()'))
      ->get();

      $banners = data_banner::where('status',1)->limit(5)->get();

      return view("client.index",[
        'barangs' => $barangs,
        'artists' => $artists,
        'blogs' => $blogs,
        'banners' => $banners

        ]);
    }

    public function artist()
    {

    	$artist = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->where('jabatan',2)->get();

      return view("client.artist",[
        'artist' => $artist
        ]);
    }

    public function agen()
    {

      $artist = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->where('jabatan',3)->get();
      

      return view("client.agen",[
        'artist' => $artist
        ]);
    }

    public function profilpage($id)
    {

    	$artist = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->where('username',$id)
      ->first();

      $barangs = data_barang::where('id_artist',$artist->id_karyawan)
      ->orWhere('id_distributor',$artist->id_karyawan)
      ->paginate(12);

      $items = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
      ->join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->orderBy(\DB::raw('RAND()'))
      ->paginate(12);
      
      $widget = data_twitter::where('id_karyawan',$artist->id_karyawan)->first();


      return view("client.pageprofil",[
        'artist' => $artist,
        'barangs' => $barangs,
        'items' => $items,
        'widget' => $widget
        ]);
    }

    public function profilartist($id)
    {
      $artist = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
      ->where('username',$id)
      ->first();
      
      return view("client.profilartist",[
        'artist' => $artist,
        ]);

    }

    public function product()
    {
    	$barangs = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
    	->get();

    	return view("client.product",[
    		'barangs' => $barangs
    		]);
    }

    public function blog()
    {
    	$foo = session('barang');

    	$items = data_blog::join('users','users.id_karyawan','=','data_blog.id_petugas')
      ->orderBy('data_blog.created_at','DESC')
      ->paginate(5);


      $kategoris = ref_blog_kategori::limit(5)->get();
      $recents = data_blog::limit(5)->orderBy('created_at','DESC')->get();

      return view("client.blog",[
        'items' => $items,
        'kategoris' => $kategoris,
        'recents' => $recents
        ]);
    }
    public function blogreview($id)
    {
      $data = data_blog::join('data_karyawan','data_karyawan.id_karyawan','=','data_blog.id_petugas')
      ->where('id',$id)
      ->first();

      $kategoris = ref_blog_kategori::limit(5)->get();
      $recents = data_blog::limit(5)->get();

      return view('client.blog_review',[
        'data' => $data,
        'kategoris' => $kategoris,
        'recents' => $recents
        ]);
    }

    public function about()
    {
    	$foo = session('barang');

    	$count = count($foo);

    	return view("client.about",[
    		'count' => $count
    		]);
    }
    public function detailproduk($id)
    {	
    	$barang = data_barang::join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
     ->where('data_barang.id',$id)
     ->select('data_barang.*','ref_type_barang.nama_type')
     ->first();

     $artist = data_karyawan::join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
     ->where('data_karyawan.id_karyawan',$barang->id_artist)->first();

     $barangs = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
     ->join('users','users.id_karyawan','=','data_karyawan.id_karyawan')
     ->where('type_barang', $barang->type_barang)
     ->limit(4)
     ->orderBy(\DB::raw('RAND()'))
     ->get();


     return view("client.detail",[
      'barang'  => $barang,
      'artist'  => $artist,
      'barangs' => $barangs,
      'warnas'   => data_warna::all()
      ]);
   }


   public function keranjang(Request $req)
   {

    \Session::put('id',base64_decode($req->id_barang));


    $id_barang = session('id');

    $aa = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')->where('id',$id_barang)->first();

    
    \Session::push('barang.items',$aa->toArray());
    
    $barangs = \Session::get('barang.items');

    return redirect('/keranjangList');
  }

  public function hapuskeranjang($nm_barang)
  {
    \Session::forget('barang.items');

    return redirect('/keranjangList');

  }

  public function keranjangList()
  {
    $barangs = \Session::get('barang.items');


    return view('client.keranjang',[
      'barangs' => $barangs,
      ]);
  }

  public function distributrolist(Request $req)
  {	
    if(Auth::check()){
      $barangs = \Session::get('barang.items');


      foreach ($req->kd_barang as $i => $kd_barang) {

       data_request_barang::create([
        'kd_barang' => $req->kd_barang[$i],
        'qty' => $req->qty[$i],
        'id_user' => 2
        ]);
     }

     $user = data_karyawan::find(\Auth::user()->id_karyawan);

     \Mail::send('client.email', ["items" => $req->all(), "user" => $user ], function($email) use ($req){

      $email->to("fullofstars.indonesia@gmail.com")
      ->subject("message form www.fullofstars.co.id");
    });

     $items = data_karyawan::where('jabatan',3)
     ->orWhere('jabatan',4)
     ->get();

     return redirect('/distributorList');
   }else{
    return view('client.register',[
      'ref_jabatan' => ref_jabatan::where('id', '>', 2)->get(),
      ]);
  }  
}


public function distributor(Request $req)
{
  if($req->all() && $req->cari != "")
  {
   $barangs = \Session::get('barang.items');
   $items = data_karyawan::where('nm_depan','like', $req->cari.'%')
   ->orWhere('nm_belakang','like', $req->cari.'%')
   ->where('jabatan',3)
   ->get();
 }else{
  $barangs = \Session::get('barang.items');
  $items = data_karyawan::where('jabatan',3)
  ->orWhere('jabatan',4)
  ->get();
}
$req->session()->forget('barang.items');
return view('client.distributor',[
  'items' => $items
  ]);
}


public function pesan($id)
{
 return view('client.pesan',[
  'id' => $id
  ]);    	
}

public function kirimPesan(Request $req)
{
  data_pesan::create([
    'email' => $req->email,
    'content' => $req->content,
    'id_petugas' => $req->id,
    'status' => 1
    ]);
  return redirect()->back()->withNotif([
   'label' => 'success',
   'err' => 'Pesan berhasil terkirim'
   ]);
}

public function register()
{

 return view('client.register',[
  'ref_jabatan' => ref_jabatan::where('id', '>', 2)->get(),

  ]);

}

public function simpanRegister(Request $req)
{

 $data = $this->dispatch(new InsertKaryawan($req->all()));

 $credential = array(
  'username' => $req->username,
  'password' => $req->password
  );

 Auth::attempt($credential);

 return redirect('/keranjangList');
}

public function review(Request $req)
{
 $result = [];

 if($req->ajax()):
  $result['result'] 	= true;
$barang = session('barang');
$datas = data_barang::where('id',$barang)->get();
$head = data_karyawan::find($req->id_karyawan);



$html = '
<div class="col-sm-12">
  <table class="table table-hover">
   <thead> 
    <tr>  
     <th>Nama Barang</th> 
     <th>Artis</th> 
     <th>Harga</th> 
     <th>Diskon</th>
     <th>Total</th>
   </tr> 
 </thead> 
 <tbody>';

  foreach($datas as $barang){
   '<tr> 

   <td>'. $barang->nm_barang .'</td> 
   <td>'. $barang->nm_depan .' '. $barang->nm_belakang .'</td> 
   <td>RP. '.number_format($barang->harga).'</td> 
   <td class="text-right">'. $barang->discount .'</td>
   <td class="text-right"> '.$barang->harga + ($barang->harga * $barang->discount).'</td>
 </tr>'; 


}
'</tbody>
</table>';

$result['content'] = $html;

else:
 $result['result'] 	= false;
$result['error'] 	= 'restrict';
endif;
return json_encode($result);

}

public function cari(Request $req)
{
  $barangs = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
  ->where('nm_barang','like', $req->cari.'%')
  ->get();

  return view('client.product',[
    'barangs' => $barangs
    ]);
}

public function search(Request $req)
{
  $datas = data_barang::join('data_karyawan','data_karyawan.id_karyawan','=','data_barang.id_artist')
  ->join('ref_type_barang','ref_type_barang.id','=','data_barang.type_barang')
  ->where('nm_barang','like', $req->cari.'%')
  ->orWhere('alamat','like', $req->cari.'%')
  ->select('data_barang.*','ref_type_barang.nama_type','data_karyawan.nm_depan','data_karyawan.nm_belakang')
  ->get();


  return view('client.search',[
    'datas' => $datas
    ]);
}

      //Footer

public function partnership()
{
  return view('client.partnership');
}

public function reseller()
{
  return view('client.reseller');
}

public function syarat()
{
  return view('client.syarat');
}

public function privasi()
{
  return view('client.privasi');
}

public function press()
{
  return view('client.press');
}

public function promo()
{
  return view('client.promo');
}

public function kontak()
{
  return view('client.kontak');
}

public function checkuser(Request $req)
{
  if($req->ajax()):
    $res = [];

  $cek = User::where('username', $req->username)->first();
  $data = '';
  if(count($cek) > 0) {
    $data = 'Username Sudah digunakan';
  }else{
    $data = 'Username Tersedia';
  }

  $res['data'] = $data;

  return json_encode($res); 

  endif;
}


}
