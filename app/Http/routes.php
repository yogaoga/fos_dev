<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/test', function(){
	return bcrypt('admin');
});


/* 
*	@author : yoga@valdlabs.com
*	
*/

/* Front End */
Route::get('/index','ClientController@index');
Route::get('/artist','ClientController@artist');
Route::get('/product','ClientController@product');
Route::get('/blog','ClientController@blog');
Route::get('/about','ClientController@about');
Route::get('/detail-produk/{id}', 'ClientController@detailproduk');
Route::get('/profil-page/{id}','ClientController@profilpage');
Route::post('/distributor','ClientController@distributrolist');
Route::get('/keranjang','ClientController@keranjang');
Route::get('/keranjangList','ClientController@keranjangList');	
Route::get('/review','ClientController@review');
Route::get('/register','ClientController@register');
Route::post('/simpan','ClientController@simpanRegister');
Route::get('/pencarian','ClientController@cari');
Route::get('/message/{id}','ClientController@pesan');
Route::post('/message','ClientController@kirimPesan');
Route::get('/blogreview/{id}','ClientController@blogreview');
Route::get('/profilartist/{id}','ClientController@profilartist');
Route::get('/hapuskeranjang/{nm_barang}','ClientController@hapuskeranjang');
Route::get('/kontak','ClientController@kontak');
Route::get('/search','ClientController@search');
Route::get('/agen','ClientController@agen');
Route::get('/checkuser','ClientController@checkuser');
Route::get('/distributorList','ClientController@distributor');


//Footer Client
Route::get('/partnership','ClientController@partnership');
Route::get('/reseller','ClientController@reseller');
Route::get('/syarat','ClientController@syarat');
Route::get('/privasi','ClientController@privasi');
Route::get('/press','ClientController@press');
//Route::get('/promo','ClientController@promo');
Route::get('/promo','ClientController@comingsoon');

/* End Front End */
Route::get('/home', 'HomeController@index');
Route::get('/','ClientController@index');
Route::get('/lock', 'HomeController@lock');



Route::controllers([
	'auth' 			=> 'Auth\AuthController',
	'password' 		=> 'Auth\PasswordController',
	'ajax' 			=> 'AjaxController'
]);
/*
*	@access : Auth
*/
Route::group(['middleware' 	=> 'auth'], function () {
	Route::controllers([
		'lockscreen' 		=> 'Auth\LockScreenController',
		'menu' 				=> 'Menus\MenusController',
		'users' 			=> 'Users\UsersController',
		'loguser'			=> 'Users\LogUserController',
		'barang' 			=> 'Master\BarangController',
		'adminblog'			=> 'Master\BlogController',
		'pesan'				=> 'Master\PesanController',
	]);
});
/* @end yoga@valdlabs.com */

/* yoga@valdlabs.com */
Route::controllers([
	'petugas' => 'Personalia\KaryawanController',
	'banner'  => 'Master\BannerController',
	'twitter' => 'Master\TwitterController',
	'warna'   => 'Master\WarnaController',
]);