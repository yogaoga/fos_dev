<?php

namespace App\Jobs\Personalia;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

use App\Models\data_karyawan;
use App\User;
use App\Models\data_level;

class KaryawanJob extends Job implements SelfHandling
{
    public $req;

    /**
     * Create a new job instance.
     *
     * @return void
     * @author yoga@valdlabs.com
     */
    

    public function __construct(array $req){
        $this->req = $req;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    
     $karyawan = data_karyawan::create([
        'nm_depan' => $this->req['nm_depan'],
        'nm_belakang' => $this->req['nm_belakang'],
        'email' => $this->req['email'],
        'sex' => $this->req['gender'],
        'hp' => $this->req['hp'],
        'tempat_lahir' => $this->req['tempat_lahir'],
        'tgl_lahir' => date('Y-m-d',strtotime($this->req['tgl_lahir'])),
        'jabatan' => $this->req['jabatan'],
        'alamat' => $this->req['alamat'],
        'id_status' => 1,
        'tgl_bergabung' => date('Y-m-d'),
        ]);

     $user = User::create([
        'name' => $this->req['nm_depan'] .' '. $this->req['nm_belakang'],
        'username' => $this->req['username'],
        'password' => bcrypt($this->req['password']),
        'id_karyawan' => $karyawan->id_karyawan,
        'permission' => 3
        ]);

    $level = data_level::firstOrCreate([
                'id_user' => $user->id_user,
                'id_level_user' => $this->req['jabatan']
            ]);

     return $karyawan;

 }
}
